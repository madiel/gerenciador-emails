<?php

namespace App\Http\Controllers;

use App\Contato;
use App\TrabalheConosco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Email;
use Alert;
use Mail;
use DateTime;
use App\Http\Requests\RecaptchaRequest;
use Illuminate\Support\Facades\Storage;



class ControllerDados extends Controller
{
    private $product;
    private $totalPage = 5;

    public function email(Request $request)
    {
        $emails = new Email();

        $emails->email = $request->input('email');
        $emails->save();

        alert()->success('Tudo certo', 'Email adicionado com sucesso');

        return back();
    }

    public function contatos(Request $request)
    {
        $nome = $request->old('nome');
        $sobrenome = $request->old('sobrenome');
        $email = $request->old('email');
        $telefone = $request->old('telefone');
        $celular = $request->old('celular');
        $segmento = $request->old('segmento');
        $cargo = $request->old('cargo');
        $faturamento = $request->old('faturamento');
        $numFuncionario = $request->old('funcionarios');

        $validateFields = array(
            'g-recaptcha-response' => 'required|recaptcha',
            'nome'  => 'required',
            'sobrenome' => 'required',
            'email' => 'required',
            'telefone' => 'required',
            'celular' => 'required',
            'segmento' => 'required',
            'cargo' => 'required',
            'faturamentoAnual' => 'required',
            'numFuncionario' => 'required',

        );
        
        $this->validate($request, $validateFields);

        $contatos = new Contato();
        $contatos->nome = $request->input('nome');
        $contatos->sobrenome = $request->input('sobrenome');
        $contatos->email = $request->input('email');
        $contatos->telefone = $request->input('telefone');
        $contatos->celular = $request->input('celular');
        $contatos->segmento = $request->input('segmento');
        $contatos->cargo = $request->input('cargo');
        $contatos->faturamento = $request->input('faturamentoAnual');
        $contatos->funcionarios = $request->input('numFuncionario');       
        $contatos->consultoria = $request->input('consultoria');
        $contatos->seguros = $request->input('seguros');
        $contatos->treinamento = $request->input('treinamento');
        $contatos->outros = $request->input('outros');
        $contatos->mensagem = $request->input('mensagem');

        $contatos->save();

        $emailContato = Contato::orderBy('id', 'desc')->first();

        Mail::send('mail.mail-contato', ['contato' => $emailContato ], function($m){
            $emailContato = Contato::orderBy('id', 'desc')->first();
            $m->from('enllevo@shapeness.com.br', 'Enlevo-site');
            $m->to('enllevo@enllevo.com.br')->subject("$emailContato->nome " . $emailContato->sobrenome);
            
        });

           alert()->success('Tudo certo', 'Contato adicionado com sucesso');

        return back();
    }


    public function trabalhe(Request $request)
    {     
        
        $nome = $request->old('nome');
        $sobrenome = $request->old('sobrenome');
        $email = $request->old('email');
        $linkedin = $request->old('linkedin');
        $areaInteresse = $request->old('areaInteresse');
        $cargo = $request->old('cargo');
        $curriculo = $request->old('curriculo');
        $celular = $request->old('celular');
        $mensagem = $request->old('mensagem');
        
        $validateFields = array(
            'g-recaptcha-response' => 'required|recaptcha',
            'nome'  => 'required',
            'sobrenome'  => 'required',
            'email'  => 'required',
            'areaInteresse'  => 'required',
            'cargo'  => 'required',
            'curriculo'  => 'required',
            'celular'  => 'required',
        );
        
        $this->validate($request, $validateFields);

        $curriculo = Input::file('curriculo');
        $size =  filesize($curriculo);
        $extensao = $curriculo->getClientOriginalExtension();
        $filenamewithextension = $request->file('curriculo')->getClientOriginalName();

        if(($extensao == 'pdf' || $extensao == 'doc' || $extensao == 'docx') && (filesize($curriculo) <= 1000000))
        {

            $post = new TrabalheConosco();

            $post->nome = Input::get('nome');
            $post->sobrenome = Input::get('sobrenome');
            $post->email = Input::get('email');
            $post->celular = Input::get('celular');
            $post->linkedin = Input::get('linkedin');
            $post->area = Input::get('areaInteresse');
            $post->cargo = Input::get('cargo');
            $post->mensagem = Input::get('mensagem');
            $post->curriculo =  $post->curriculo = $post->nome.'.'.$extensao;
            $post->save();

            $curriculo2 = TrabalheConosco::orderBy('id', 'desc')->first();

            Mail::send('mail.email', ['curriculo' => $curriculo2 ], function($m){
                $curriculo2 = TrabalheConosco::orderBy('id', 'desc')->first();
                $m->from('enllevo@shapeness.com.br', 'Enlevo-site');
                $m->to('enllevo@enllevo.com.br')->subject("$curriculo2->nome " . $curriculo2->sobrenome);;
        });
            
            $request->file('curriculo')->storeAs(
                'public/curriculo', "$post->nome." . $extensao,'local'
            );
            $post->save();

            alert()->success('Tudo certo', 'Dados enviados com sucesso');
            return back();
            }else{
                dd('Suba um arquivo pdf ou word');
            }

    }

    public function contatoInterno(){

        $copntatoInterno = Contato::paginate($this->totalPage);

        $data['contato'] = $copntatoInterno;

        return view('mail.contato-interno', $data);
    }

    public function contatoTotal()

    {

        $copntatoInterno = Contato::first();


        $data['contato'] = $copntatoInterno;

        return view('mail.contato-total', $data);
    }


    public function trabalheInterno(){

        $trabalheInterno = TrabalheConosco::paginate($this->totalPage);

        $data['trabalhe'] = $trabalheInterno;

        return view('mail.trabalhe-interno', $data);
    }

    public function trabalheTotal()

    {

        $trabalheInterno = TrabalheConosco::first();

        $data['curriculo'] = $trabalheInterno;

        return view('mail.trabalhe-total', $data);
    }

}
