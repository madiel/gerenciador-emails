<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Controle de Cobrança Digital - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaBannerControle">

					<div class="bannerControle">

					</div>

					<section class="bannerSolucaoControle">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-controle-de-cobranca') }}" class="destaqueAzul">controle de cobrança digital</a>
							</span>
						</div>
							<span class="iconeSegCobrancaDigital icone05"></span>
							<h1 class="tituloControle">Solução Controle de Cobrança Digital</h1>
							<h2 class="textIntroControle">Solução certa para todos os tipos de fornecimento de peças...</h2>
							<h3 class="contTextIntroControle">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
					</div>

					<div>
						<img class="imgControle" src="images/solucoes/Controle-de-Cobranca-Digital/controle-de-cobranca-digital.png" alt="Solução Controle de Cobrança Digital" title="Enllevo Solução Controle de Cobrança Digital">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoControleCobrançaDigital-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaControle">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesControle">CONHEÇA NOSSAS SOLUÇÕES PARA CONTRATOS DE SERVIÇOS</p>
						<hr>
					</aside>

					<div class="divSolucoesControle">
						<ul>
							<li><a href="#portalDigital"><h1>portal de soluções digitais</h1></a></li>
							<li><a href="#automacaoCarga"><h1>automação de cargas</h1></a></li>
							<li><a href="#reportCenter"><h1>report center</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosControle segmentoControle" id="portalDigital">
						<div class="introControle">
							<h1>portal de soluções digitais |</h1><h1 class="segControle">PSD</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						<hr class="hrControle">
						
						<div class="descricaoControle">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoControle">
						<ul>
							<div class="gestaoControle">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegControle" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosControle conteinerBeneficiosControle1">
							<div class="beneficiosControle">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Gestão e acompanhamento do envio dos mailings </li>
									<li>Acompanhamento e estudo do call-back (retorno das campanhas enviadas)</li>
									<li>Definição da estratégia de alcance da campanha de acordo com a quantidade de profissionais disponíveis para atender
										à demanda de retornos a cada momento do dia (soltar mensagens em quantidades que sejam possíveis atender) </li>
									<li>Análise do retorno das pessoas impactadas pela campanha - análise dos resultados de cada campanha</li>
									<li>Previsões diárias das campanhas que serão realizadas </li>
									<li>Interface com qualquer API de comunicação de envio de msgs com operadoras e provedores de e-mail </li>
								</ul>
							</div>
							<div class="caracteristicasControle">
								<p>Características do Sistema</p>

								<ul class="caracteristicasControle1">
									<li class="liNeutra">Signal mailing: overview (prévia) de como vai ser a campanha no dia
										 por meio de uma notificação por sms para o gestor </li>
									<li>Worflow de aprovação dos templates que serão enviados</li>
									<li>Fluxo de aprovação de peças de campanha. </li>
									<li>Envios: gestão e acompanhamento da campanha para deixar no formato correto de envio pelas
										operadoras de telefonia (sms ou whatsapp) ou pelos provedores de e-mail </li>
									<li>Templates de mensagens diferentes para plataformas diferentes </li>
									<li>Call-back: retorno da operadora ou provedor de e-mail sobre o recebimento das campanhas </li>
									<li>Interface com qualquer API de comunicação de envio de msgs com operadoras e provedores de e-mail </li>
									<li>Gestão das campanhas com parametrização flexível para cada necessidade e demanda </li>
									<li>Área pra criar backlists pra se ter controle de pessoas que estão com bloqueio de chamas ativo</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->
					<section class="segmentosControle segmentoControleOposto" id="automacaoCarga">
						<div class="introControleOposto">
							<h1 class="h1IntroControle">automação de cargas |</h1><h1 class="segControleOposto">AC</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrControle hrOposto">
						
						<div class="descricaoControle descricaoOposto">
							<p>A Solução da Enllevo TMS (Sistema de Gestão de Transportes) é um software responsável pelo planejamento,
							monitoramento e contabilidade do transporte, em qualquer tipo de estrutura de distribuição.</p>

							<p>Aperfeiçoamento do planejamento de rotas, combinação de transportes,
							transbordos e muitas outras funções fazem deste módulo uma ferramenta perfeita para todas
							as empresas com operações de transporte complexas.</p>

						</div>

						<div class="secaoControle secaoOposto">
							<ul>
								<div class="gestaoControle gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-01.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-02.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-03.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-04.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-05.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-06.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-07.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-08.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-09.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-10.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-11.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-12.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-13.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/tms-14.png" alt="Enllevo - Gestão de custos de transportadoras" title="Gestão de custos de transportadoras">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosControle conteinerBeneficiosOposto">
							<div class="beneficiosControle beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Transporte de dados de forma automatizada </li>
									<li>Automação na troca de dados com agendamento, periodicidade e sequência de carga específica</li>
									<li>Redução de área de “cópias de dados”: automação da transição de dados entre ferramentas </li>
									<li>‘schedular’ a sequência da carga e o tempo de execução e a sequência dos arquivos </li>
									<li>Gerenciamento de retorno das pessoas impactadas pela campanha </li>
								</ul>
							</div>
							<div class="caracteristicasControle caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasControle2">
									<li class="liNeutra">Ferramenta para gestão de dados ou arquivos </li>
									<li>Conecta-se diretamente como FTP, enfileira o arquivo e conversa com outras ferramentas para enviar ou receber</li>
									<li>Manipulação dos arquivos e cargas</li>
									<li>Automação de cargas </li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->
					<section class="segmentosControle segmentoControle" id="reportCenter">
						<div class="introControle">
							<h1>report center |</h1><h1 class="segControle">RC</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrControle">

						<div class="descricaoControle">
							<p>A Solução da Enllevo YMS (Sistema de Gerenciamento de Pátios) é um software especializado que permite controlar
							totalmente o tráfego de veículos e pedestres nos pátios e instalações da fábrica.</p>

							<p>Além disso, melhora o planejamento das operações de carga e descarga, otimizando o trabalho
							e minimizando o tempo de inatividade.</p>
						</div>


						<div class="secaoControle">



							<ul>
								<div class="gestaoControle">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegControle" src="images/solucoes/Logistica/yms-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosControle conteinerBeneficiosControle3">
							<div class="beneficiosControle beneficiosControle3">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Formata e configura o envio de relatórios gerencias ou operacionais de qualquer tipo</li>
									<li>Diminuição do custo de, no mínimo, uma ferramenta de geração de relatório </li>
									<li>Envio dos relatórios por e-mail ou SMS já direto do sistema </li>
									<li>Automatização do processo de geração de relatórios diários</li>
									<li>Diminuição do custo de uma área específica para geração de relatórios,
										pois não necessita de muitos funcionários para configurar o que foi montado pelo sistema </li>
								</ul>
							</div>


							<div class="caracteristicasControle caracControle3">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolControle3">
									<li>Ferramenta de geração de relatórios </li>
									<li>Geração de relatórios a partir de qualquer outro software</li>
									<li>Envio de relatórios para qualquer outro sistema</li>
									<li>Possibilidade de exportar em excel</li>
									<li>Macros dentro do excel para melhor configuração do relatório</li>
									<li>Prévia da tabela: sistema salva uma imagem do relatório (print) e envia no corpo do e-mail </li>
									<li>Relatórios gerados a partir de dados internos extraídos pela própria empresa </li>
									<li>Escolha de templates para os relatórios </li>
									<li>Agendamento de envios</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--SoluçãoControleCobrançaDigital-->


			<!--Backoffice-->
				<aside class="backofficeControle">
					<div class="textoBackofficeControle">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesControle">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
