<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Distribuição - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaDistribuicao">

					<div class="bannerDistribuicao">

					</div>

					<section class="bannerSolucaoDistribuicao">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-distribuicao') }}" class="destaqueAzul">distribuição</a>
							</span>
						</div>
							<span class="iconeSegDistribuicao icone09"></span>
							<h1 class="tituloDistribuicao">Solução Distribuição</h1>
							<h2 class="textIntroDistribuicao">A Melhor Solução para sua indústria.</h2>
							<h3 class="contTextIntroDistribuicao">Conheça o ERP desenvolvido pela da Enllevo, estruture os projeção de consumo, requisições de compras, pedidos, importação, ordens de produção, custos, rastreabilidade, carteira de entrega, controle de qualidade e formação de preços entre outros.</h3>
					</section>
					</div>

					<div>
						<img class="imgDistribuicao" src="images/solucoes/Distribuicao/distribuicao-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoDistribuição-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaDistribuicao">
								<h2 class="h2PropSolucao">ficou interessado na solução?</h2>
								<h2 class="h2Proposta">peça uma proposta sem compromisso.</h2>
							</div>
						</a>
						<p class="solucoesDistribuicao">conheça nossas soluções para contrato de serviços</p>
						<hr>
					</aside>

					<div class="divSolucoesDistribuicao">
						<ul>
							<li><a href="#distribuicao"><h1>Distribuição</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosDistribuicao segmentoDistribuicao" id="distribuicao">
						<div class="introDistribuicao">
							<h1>distribuição |</h1><h1 class="segDistribuicao">Módulo</h1>

							<h2>Enllevo Módulo de Gestão - <strong>Sistema de Distribuição</strong></h2>
						</div>
						<hr class="hrDistribuicao">
						

						<div class="descricaoDistribuicao">
							<p>Garanta mais eficiência, otimize processos e evolua a gestão de seus negócios a um outro nível.</p>

							<p>Conheça os recursos e as funcionalidades que fazem do ERP Enllevo, a solução ideal para ganhar mais tempo no dia a dia e extrair mais inteligência para a condução do seu negócio.</p>

							</div>

						<div class="secaoDistribuicao">
						<ul>
							<div class="gestaoDistribuicao">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Identificaçao automatica</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Relatorios com a fabrica</p>
								</li>

								<li>
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-03.png" alt="Pickings" title="Pickings">
									<p>Comunicação e notificação</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-05.png" alt="Inventários" title="Inventários">
									<p>Politica comercial</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>E-commerce</p>
								</li>

								<li>
									<img class="imgSegDistribuicao" src="images/solucoes/Distribuicao/distribuicao-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Rastreamento do produto</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDistribuicao" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								
							</ul>
						</div>
						<aside class="conteinerBeneficiosDistribuicao conteinerDistribuicao">
							<div class="beneficiosDistribuicao">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Automatização de orçamentos para clientes e condições especiais</li>
									<li>Maior controle e segurança para o gestor</li>
									<li>Fácil identificação das peças por meio das fotos</li>
									<li>Menor chance de erros nas compras e vendas</li>
									<li>Menor chance de troca de peças nas vendas </li>
									<li>Rastreabilidade permite maior organização e controle dos produtos </li>
									<li>Checagem de compras e vendas feita automaticamente pelo software</li>
									<li>Atende pequenas, médias e grandes empresas </li>
									<li>Política comercial e de preços realizada automaticamente pelo software </li>
									<li>Controle e organização definidos de acordo com as necessidades de cada empresa</li>
									<li>Integração com e-commerce</li>
									<li>Troca de informações com a fábrica de forma automatizada </li>
									<li>Maior controle no prazo de validade dos produtos e organização das vendas </li>
								</ul>
							</div>
							<div class="caracteristicasDistribuicao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaDistribuicao">
									<li class="liNeutra">Possibilidade de bloqueio de peças antes da aprovação de orçamento </li>
									<li>Sistema determina a política comercial, analisando a quantidade de desconto permitida para cada forma de pagamento </li>
									<li>Cliente fidelidade: registro de desconto diferenciado para cliente especial </li>
									<li>Cliente contrato: peças com preços especiais determinados em contrato já ficam registradas no sistema com o valor acordado </li>
									<li>Autorização de venda: orçamentos com valor mais baixo do que o registrado no sistema, o gestor é notificado e precisa autorizar para que a nota seja emitida</li>
									<li>Inserção de fotos dos produtos no sistema </li>
									<li>Inspeção de vendas: checagem das peças recolhidas pelo cliente e das que o vendedor inseriu no pedido por meio de código de barras e com imagens</li>
									<li>Código de barras original ou interno da empresa</li>
									<li>Exigência de uma assinatura para o pedido de compra da empresa </li>
									<li>Inspeção: confere se o que foi comprado de acordo com o pedido de compra e só libera se o valor for menor </li>
									<li>Metodologia mark up </li>
									<li>Calcula automaticamente a precificação de acordo com a margem de lucro determinada pela empresa </li>
									<li>Integração com e-commerce: vai direto para o orçamento </li>
									<li>Rastreabilidade do produto: identificação de quem comprou e para quem vendeu </li>
									<li>Análise gerencial: quanto pagou, por quanto vendeu e quantidade de impostos</li>
									<li>Controle de prazo de validade</li>
									<li>Interface com as fábricas: sistema atualiza automaticamente a lista de preço da fábrica com base no mark up na política de descontos </li>
								</ul>
							</div>
						</aside>
					</section>
					
			<!--SoluçãoDistribuição-->


			<!--Backoffice-->
				<aside class="backofficeDistribuicao">
					<div class="textoBackofficeDistribuicao">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesDistribuicao">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
