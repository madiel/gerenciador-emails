<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Contábil Fiscal - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaBannerContabil">

					<div class="bannerContabil">

					</div>

					<section class="bannerSolucaoContabil">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-contabil-fiscal') }}" class="destaqueAzul">contábil e fiscal</a>
							</span>
						</div>
							<span class="iconeSegContabilFiscal icone07"></span>
							<h1 class="tituloContabil">Solução Contábil e Fiscal</h1>
							<h2 class="textIntroContabil">Solução certa para todos os tipos de...</h2>
							<h3 class="contTextIntroContabil">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
					</div>

					<div>
						<img class="imgContabil" src="images/solucoes/Contabil-e-Fiscal/contabil-e-fiscal-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaContabil">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesContabil">CONHEÇA NOSSAS SOLUÇÕES PARA CONTRATOS DE SERVIÇOS</p>
						<hr>
					</aside>

					<div class="divSolucoesContabil">
						<ul>
							<li><a href="#contabil"><h1>contábil e fiscal</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosContabil segmentoContabil" id="contabil">
						<div class="introContabil">
							<h1>contábil e fiscal |</h1><h1 class="segContabil">módulo</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						<hr class="hrContabil">
						
						<div class="descricaoContabil">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoContabil">
						<ul>
							<div class="gestaoContabil">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegContabil" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContabil" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegContabil" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegContabil" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosContabil conteinerBeneficiosContabil1">
							<div class="beneficiosContabil">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Operador apenas insere o produto e o sistema calcula com base nas informações pré-cadastradas </li>
									<li>Não permite que a nota fiscal seja emitida com erro </li>
									<li>Impede que seja necessário alterar o livro fiscal</li>
									<li>Usuário não precisa alterar regras para cada cliente </li>
									<li>Usuário não precisa registrar manualmente a alíquota de cada produto</li>
									<li>Não é necessário inserir manualmente as informações dos produtos para cálculo de frete</li>
									<li>Pouco suscetível a erro </li>
									<li>Sistema se comunica diretamente com o TMS</li>
									<li>TMS organiza os produtos de cada caminhão a partir do peso total inserido na nota fiscal </li>
									<li>Sistema atua diretamente na raiz de eventuais problemas </li>
									<li>Conciliação entre o contábil e o fiscal </li>
									<li>Checagem de dados das operadoras de máquinas de cartão de crédito</li>
									<li>Impede fraudes das máquinas de cartões e outras empresas </li>
								</ul>
							</div>
							<div class="caracteristicasContabil">
								<p>Características do Sistema</p>

								<ul class="caracteristicasContabil1">
									<li class="liNeutra">Principais informações (cliente, peso, imposto, etc) *pré-cadastradas* no sistema</li>
									<li>Regras cadastradas e associadas a cada cliente </li>
									<li>Impostos registrados e contabilizados automaticamente pelo sistema</li>
									<li>Informa CNPJ inválido</li>
									<li>Em caso de erro na emissão de nota, informa de maneira clara e objetiva </li>
									<li>Sistema trabalha a partir de uma tabela NCM já cadastrada </li>
									<li>Impostos de cada produto registrados no sistema</li>
									<li>Cálculo automático da alíquota de cada produto a partir da base cadastrada no sistema</li>
									<li>Tributações do seguro e frete inseridas no sistema </li>
									<li>Peso e valor de cada produto registrados no sistema para cálculo do frete e seguro</li>
									<li>Sistema calcula o valor do frete e do seguro automaticamente </li>
									<li>Envia notas e boletos emitidos por e-mail</li>
									<li>Lista a movimentação do contas a receber </li>
									<li>Cartão de crédito: sistema realiza associações com as informações das operadoras de máquinas </li>
									<li>Checagem das informações enviadas pelas operadoras</li>
									<li>Calcula a movimentação de cada cartão de crédito a partir das notas e confere com os dados das operadoras </li>
									<li>Retira as notas pendentes de um cartão após confirmação de pagamento da operadora </li>
									<li>Taxa de desconto da operadora registrada no sistema</li>
									<li>Checagem do valor da venda e da taxa de desconto da operadora para impedir fraudes </li>
								</ul>
							</div>
						</aside>
					</section>

			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeContabil">
					<div class="textoBackofficeContabil">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesContabil">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
