<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Contato - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
        <!-- Scripts JS -->

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	</head>

    <body>

		<div class="col-md-offset-2 col-md-8">
                @include('sweet::alert')
            </div>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- BannerContato -->
			<div class="conteudoBanner">
				<div class="imagemBannerInstitucional">
					<img class="imgInstitucional" src="images/banner-topo-enllevo.jpg" title="Enllevo - Soluções que Geram Satisfação" alt="Institucional">
				</div>
				<div class="textoBannerContato">
					<h1 class="contatoBanner">Contato</h1>
				</div>
			</div>
			<!-- BannerContato -->

			<!--IntroducaoContato-->
			<div class="backgroundContato">
			<span class="caminhoSite">
				<a href="index.php">home</a> /
				<a href="{{ route('institucional') }}" class="destaqueAzul">contato</a>
			</span>
					<section class="introducaoContato">
						<h4>Estamos prontos para atender você.</h4>
						<p>Se você quer contratar ou simplesmente saber mais sobre as nossas soluções,
						envie seus dados e entraremos em contato.</p>
					</section>
			<!--IntroduçãoContato-->

			<!--Formulario-->
					<div class="formularioContato" id="ancora">
						<form action="{{ url('/dados/contatos') }}" method="POST" class="formContato">
							{{ csrf_field() }}

							@if ($errors->has('nome'))
							<input type="text" name="nome"  style="border: 1px solid red;"  class="nomeFormContato" placeholder="Campo nome obrigatório" size="">
							@else
							<input type="text" name="nome" class="nomeFormContato" placeholder="Nome" size="" value="{{ old('nome') }}">
							@endif

							@if ($errors->has('sobrenome'))
							<input type="text" name="sobrenome" style="border: 1px solid red;"   class="sobrenomeFormContato" placeholder="Campo  Sobrenome obrigatório" size="">
							@else
							<input type="text" name="sobrenome" class="sobrenomeFormContato" placeholder=" Sobre Nome" size="" value="{{ old('sobrenome') }}">
							@endif

							@if ($errors->has('email'))
							<input type="email" name="email" style="border: 1px solid red;" class="emailFormContato" placeholder="Campo E-mail obrigatório" size="">
							@else
							<input type="email" name="email" class="emailFormContato" placeholder="E-mail" size="" value="{{ old('email') }}">
							@endif

							@if ($errors->has('telefone'))
							<input type="tel" name="telefone" style="border: 1px solid red;"  class="telFormContato" placeholder="Campo Telefone obrigatório" size="">
							@else
							<input type="tel" name="telefone"  class="telFormContato" placeholder="Telefone" size="" value="{{ old('telefone') }}">
							@endif
							
							@if ($errors->has('celular'))
							<input type="tel" name="celular" style="border: 1px solid red;" class="celFormContato" placeholder="Campo Celular/ Whatsapp obrigatório" size="">
							@else
							<input type="tel" name="celular" class="celFormContato" placeholder="Celular/ Whatsapp" size="" value="{{ old('celular') }}">
							@endif

							@if ($errors->has('segmento'))
							<div class="divInputSegmento">
								<p class="paragrafoSegFormContato">Qual seu segmento?</p>
								<input type="text" name="segmento" style="border: 1px solid red;" class="SegFormContato" placeholder="Escolha o segmento..." size="">
							</div>
							@else
							<div class="divInputSegmento">
									<p class="paragrafoSegFormContato">Qual seu segmento?</p>
									<input type="text" name="segmento" class="SegFormContato" placeholder="Escolha o segmento..." size="" value="{{ old('segmento') }}">
								</div>
							@endif

							@if ($errors->has('cargo'))
							<div class="divInputCargo">
									<p class="paragrafoCargoFormContato">Qual seu cargo?</p>
									<input type="text" name="cargo" style="border: 1px solid red;" class="cargoFormContato" placeholder="Escolha o segmento..." size="">
								</div>
							@else
							<div class="divInputCargo">
									<p class="paragrafoCargoFormContato">Qual seu cargo?</p>
									<input type="text" name="cargo" class="cargoFormContato" placeholder="Escolha o segmento..." size="" value="{{ old('cargo') }}">
								</div>
							@endif							


							@if ($errors->has('faturamentoAnual'))
							<div class="divInputFaturamento">
									<p class="paragrafoFormContato">Qual faturamento anual a sua empresa se encaixa?</p>
									<div class="grupoRadioButton">
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento4-5" type="radio" name="faturamentoAnual" class="radioButtonQuadrado radioButtonQuadradoValidacao" value="Até 4,5" />
											<label class="labelRadioButtonQuadrado" for="faturamento4-5">Até 4,5</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id= "faturamento5-10" type="radio" name="faturamentoAnual" class="radioButtonQuadrado radioButtonQuadradoValidacao" value="4,5 à 10" />
											<label class="labelRadioButtonQuadrado" for="faturamento5-10">4,5 à 10</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento10-100" type="radio" name="faturamentoAnual" class="radioButtonQuadrado radioButtonQuadradoValidacao" value="10 à 100" />
											<label class="labelRadioButtonQuadrado" for="faturamento10-100">10 à 100</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento100-200" type="radio" name="faturamentoAnual" class="radioButtonQuadrado radioButtonQuadradoValidacao" value="100 à 200" />
											<label class="labelRadioButtonQuadrado" for="faturamento100-200">100 à 200</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento200+" type="radio" name="faturamentoAnual" class="radioButtonQuadrado radioButtonQuadradoValidacao" value="200 +" />
											<label class="labelRadioButtonQuadrado" for="faturamento200+">200 +</label>
										</div>
									</div>
								</div>
							@else
							<div class="divInputFaturamento">
									<p class="paragrafoFormContato">Qual faturamento anual a sua empresa se encaixa?</p>
									<div class="grupoRadioButton">
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento4-5" type="radio" name="faturamentoAnual" class="radioButtonQuadrado" value="Até 4,5" />
											<label class="labelRadioButtonQuadrado" for="faturamento4-5">Até 4,5</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id= "faturamento5-10" type="radio" name="faturamentoAnual" class="radioButtonQuadrado" value="4,5 à 10" />
											<label class="labelRadioButtonQuadrado" for="faturamento5-10">4,5 à 10</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento10-100" type="radio" name="faturamentoAnual" class="radioButtonQuadrado" value="10 à 100" />
											<label class="labelRadioButtonQuadrado" for="faturamento10-100">10 à 100</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento100-200" type="radio" name="faturamentoAnual" class="radioButtonQuadrado" value="100 à 200" />
											<label class="labelRadioButtonQuadrado" for="faturamento100-200">100 à 200</label>
										</div>
										<div class="caixaRadioButtonQuadrado">
											<input id="faturamento200+" type="radio" name="faturamentoAnual" class="radioButtonQuadrado" value="200 +" />
											<label class="labelRadioButtonQuadrado" for="faturamento200+">200 +</label>
										</div>
									</div>
								</div>
							@endif		

							<div class="divInputFuncEmpresa">
									<p class="paragrafoFuncFormContato" >Quantos funcionários tem sua empresa?</p>
									<input type="text" name="numFuncionario" class="funcFormContato" required placeholder="Escreva quantos funcionários..." size="" value="{{ old('numFuncionario') }}">
								</div>

	
								<div class="grupoRadioButton caixaEmpresa">
									<div class="caixaRadioButton">
										<input type="checkbox" name="software" class="radioButton" value="Software / Sistema" />
										<label class="labelRadioButton">Software / Sistema</label>
									</div>
									<div class="caixaRadioButton">
										<input type="checkbox" name="consultoria" class="radioButton" value="consultoria" />
										<label class="labelRadioButton">Consultoria</label>
									</div>
									<div class="caixaRadioButton" id="ancoraRobo">
										<input type="checkbox" name="seguros" class="radioButton" value="seguros" />
										<label class="labelRadioButton">Seguros</label>
									</div>
									<div class="caixaRadioButton">
										<input type="checkbox" name="treinamento" class="radioButton" value="treinamento" />
										<label class="labelRadioButton">Treinamento</label>
									</div>
									<div class="caixaRadioButton">
										<input type="checkbox" name="outros" class="radioButton" value="outros" />
										<label class="labelRadioButton">Outros</label>
									</div>
								</div>


							
							<div class="caixaTextarea">
								<!--input type="textarea" name="mensagem" class="mensagemFormContato" placeholder="Escreva sua mensagem..."-->
								<textarea rows="100" name="mensagem" class="mensagemFormContato" placeholder="Escreva sua mensagem..."></textarea>
								<input type="submit" name="submit" value="Enviar" class="submitFormContato">
							</div>

							<div class="divRecaptcha"> {!! Recaptcha::render() !!}  </div>
							
						</form>

						@if ($errors->has('g-recaptcha-response'))
							<script>
								window.location.href='#ancora';
							</script>
						@endif

						@if ($errors->has('g-recaptcha-response') && count($errors) == 1)
						<script>
								window.location.href='#ancoraRobo';
							</script>
							<div class="center-robo alert-recap">
									<p>Você é um robo ?</p>
							</div>
						@endif
					</div>
			<!--Formulario-->

			<!--Contatos-->
					<aside class="contatos">

						<div class="contatoEmail">
							<span class="iconeEmail iconeEmail2"></span>
							<p class="contatoEmailP">E-mail</p><p class="email">enllevo@enllevo.com.br</p>
						</div>

						<!--div class="contatoAtendimento">
							<span class="iconePhone iconePhone2"></span>
							<p class="contatoAtendimentoP">Atendimento</p><p class="telefoneAtendimento">11 2714 - 2175</p>
						</div-->

						<div class="contatoVendas">
							<span class="iconeCarrinhoVendas iconeCarrinhoVendas2"></span>
							<p class="contatoVendasP">Vendas</p><p class="telefoneVendas">11 2714 - 2175</p>
						</div>

						<!--div class="contatoSuporte">
							<span class="iconeSuporteSite iconeSuporteSite2"></span>
							<p class="contatoSuporteP">Suporte</p><p class="telefoneSuporte">4003 - 0258</p>
						</div-->

						<div class="suporteTexto suporteSpanFooter contatoSuporte">
							<a href="{{ url('https://shapeness.freshdesk.com/support/login') }}" class="suporte suporteFooter">
								<span class="iconeSuporteSite"></span>
								Suporte
							</a>
						</div>

					</aside>
			<!--Contatos-->

			<!--Mapa-->
				<div class="mapaGoogle">
				<span class="iconeLocalizacao"></span>
				<p class="enderecoMapaEnllevo">Rua Carneiro da Cunha, 303, Saúde - São Paulo - CEP 04144-000</p>
				<iframe class="mapaGoogleEnllevo" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.679108583637!2d-46.638821784869926!3d-23.615838669491737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a5201ba9eb1%3A0x8336241cd3072df4!2sR.+Carneiro+da+Cunha%2C+303+-+Vila+da+Sa%C3%BAde%2C+S%C3%A3o+Paulo+-+SP%2C+04144-000!5e0!3m2!1spt-BR!2sbr!4v1541585342364"
				 allowfullscreen></iframe>
				</div>
			<!--Mapa-->

			<!--RedesSociais-->
				<aside class="textoRedesSociaisContato">
					<p>Fique por dentro das novidades e mais conteúdos em nossas redes sociais</p>

					<div class="redesSociais">
						<ul>
							<li class="redeSocialFacebook">
								<a href="{{ url('https://www.facebook.com/enllevo/') }}" title="Facebook - Enllevo" target = "blank">
									<span class="iconeFacebook">
									</span>
								</a>
							</li>

							<li class="redeSocialInstagram">
								<a href="{{ url('https://www.instagram.com/enllevo_/') }}" title="Instagram - Enllevo" target = "blank">
									<span class="iconeInstagran">
									</span>
								</a>
							</li>
							<li class="redeSocialLinkedin">
								<a href="{{ url('https://www.linkedin.com/company/enllevo') }}" title="LinkedIn - Enllevo" target = "blank">
									<span class="iconeLinkedin">
									</span>
								</a>
							</li>
						</ul>
					</div>
				</aside>
			</div>
			<!--RedesSociais-->

			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
	</body>
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
	async defer>
</html>
