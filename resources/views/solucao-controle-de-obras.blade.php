<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Controle de Obras - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaObras">

					<div class="bannerObras">

					</div>

					<section class="bannerSolucaoObras">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-controle-de-obras') }}" class="destaqueAzul">controle de obras</a>
							</span>
						</div>
							<span class="iconeSegControleObras icone12"></span>
							<h1 class="tituloObras">Solução Controle de Obras</h1>
							<h2 class="textIntroObras">Solução em gestão integrada, agregando ganhos de eficiência e padronização de processos, reduzindo
							custos e riscos operacionais</h2>
							<h3 class="contTextIntroObras">Conheça o ERP modelado e desenvolvido pela da Enllevo, exclusivamente para atender os
							segmentos de Engenharia Civil, Construção e Incorporação</h3>
					</section>
					</div>

					<div>
						<img class="imgObras" src="images/solucoes/Controle-de-Obras/controle-de-obras-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaObras">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesObras">CONHEÇA NOSSAS SOLUÇÕES PARA CONTROLE DE OBRAS</p>
						<hr>
					</aside>

					<div class="divSolucoesObras">
						<ul>
							<li><a href="#empreiteiras"><h1>empreiteiras</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosObras segmentoObras" id="empreiteiras">
						<div class="introObras">
							<h1>empreiteiras |</h1><h1 class="segObras">ERP</h1>

							<h2>Enllevo ERP - Enterprise Resource Planning<strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						<hr class="hrObras">
						
						<div class="descricaoObras">
							<p>A solução da Enllevo para Controle de Obras (Sistema ERP de Gestão Obras) é um software moderno e completo
							proporcionando resultado positivo para seus clientes com um projeto bem executado, dentro do cronograma e do orçamento planejados.</p>

							<p>Centralize dados e informações confiáveis para obter mais eficiência no controle de produção e na eliminação de perdas,
							controle e gestão de projetos evitando desperdício de tempo, dinheiro, mão de obra e materiais.</p>

							</div>

						<div class="secaoObras">
						<ul>
							<div class="gestaoObras">
								<span>Gerencie suas obras com mais agilidade e menos custos</span>
							</div>
								<li>
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Apontamento</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Documentos</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Registros</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Vistoria e Entrega</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Inventários</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Integração com equipamentos externos</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de custos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Modos Operacionais</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Customização total</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Gestão de empilhadeiras</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Gestão de produtos perigosos (ADR)</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>


								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosObras conteinerBeneficioObras">
							<div class="beneficiosObras">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Redução de custos nos projetos</li>
									<li>Aumento da capacidade operacional</li>
									<li>Organização de processos e recursos</li>
									<li>Otimização do tempo</li>
									<li>Gerenciamento de orçamentos</li>
									<li>Diminuição dos desperdícios</li>
									<li>Monitoramento de trabalho</li>
									<li>Técnicas modernas de TI</li>
									<li>Melhor atendimento ao cliente</li>
									<li>Integração com sistemas externos</li>
									<li>Indicadores gráficos</li>
								</ul>
							</div>
							<div class="caracteristicasObras">
								<p>Características do Sistema</p>

								<ul class="caracteristicaObras">
									<li class="liNeutra">Gestão de fornecedores</li>
									<li>Gestão de funcionários</li>
									<li>Gestão de equipamentos</li>
									<li>Ordens para fornecedores e recebimentos</li>
									<li>Controle qualitativo e quantitativo da mercadoria</li>
									<li>Identificação automática (AI)</li>
									<li>Precificação de orçamentos</li>
									<li>Organização dos orçamentos</li>
									<li>Pedidos de clientes e envios</li>
									<li>Relatórios contábeis de serviços</li>
									<li>Mão de obra</li>
									<li>Gestão de tarefas</li>
									<li>Inventários</li>
									<li>Materiais</li>
									<li>Gestão de lotes e datas de validade</li>
									<li>Despesas</li>
									<li>Gestão de produtos perigosos</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto>
					<section class="segmentosObras segmentoObrasOposto" id="tratores">
						<div class="introObrasOposto">
							<h1 class="h1IntroObras">GESTÃO DE LOGÍSTICA |</h1><h1 class="segObrasOposto">TMS</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						
						<hr class="hrObras hrOposto">
						
						<div class="descricaoObras descricaoOposto">
							<p>A Solução da Enllevo TMS (Sistema de Gestão de Transportes) é um software responsável pelo planejamento,
							monitoramento e contabilidade do transporte, em qualquer tipo de estrutura de distribuição.</p>

							<p>Aperfeiçoamento do planejamento de rotas, combinação de transportes,
							transbordos e muitas outras funções fazem deste módulo uma ferramenta perfeita para todas
							as empresas com operações de transporte complexas.</p>

						</div>

						<div class="secaoObras secaoOposto">
							<ul>
								<div class="gestaoObras gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-01.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-02.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-03.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-04.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-05.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-06.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-07.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-08.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-09.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-10.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-11.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-12.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-13.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/tms-14.png" alt="Enllevo - Gestão de custos de transportadoras" title="Gestão de custos de transportadoras">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosObras conteinerBeneficiosOposto">
							<div class="beneficiosObras beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Redução de custos logísticos</li>
									<li>Aumento da capacidade operacional</li>
									<li>Otimização de processos de transporte</li>
									<li>Controle preciso de custos</li>
									<li>Suporte ADR</li>
									<li>Rastreamento de lotes e unidades de carga</li>
									<li>Monitoramento de eventos relacionados ao transporte</li>
									<li>Técnicas modernas em TI e logística</li>
									<li>Indicadores gráficos</li>
									<li>Melhor atendimento ao cliente</li>
									<li>Operacional em qualquer tipo de empresas</li>
								</ul>
							</div>
							<div class="caracteristicasObras caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaObras">
									<li class="liNeutra">Gestão de ordens de expedição</li>
									<li>Criação e planejamento de rotas de transporte</li>
									<li>Operação de incidentes de expedição atípicos</li>
									<li>Colaboração com vários tipos de equipamentos móveis</li>
									<li>Gestão de custos de serviços de transporte</li>
									<li>Lidar com contratos relacionados às tarefas de transporte</li>
									<li>Tarifas de serviçõs de transporte definidas pelo usuário</li>
									<li>Algoritmos de apuração definidos pelo usuário</li>
									<li>Lista de preços de transporte definidas pelo usuário</li>
									<li>Estatísticas e análises</li>
									<li>Integração com mapas</li>
								</ul>
							</div>
						</aside>
					</section-->
					<!--Final Bloco Oposto-->
					<!--section class="segmentosObras segmentoObras" id="veiculoPasseio">
						<div class="introObras">
							<h1>GESTÃO DE LOGÍSTICA |</h1><h1 class="segObras">YMS</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrObras">
						
						<div class="descricaoObras">
							<p>A Solução da Enllevo YMS (Sistema de Gerenciamento de Pátios) é um software especializado que permite controlar
							totalmente o tráfego de veículos e pedestres nos pátios e instalações da fábrica.</p>

							<p>Além disso, melhora o planejamento das operações de carga e descarga, otimizando o trabalho
							e minimizando o tempo de inatividade.</p>
						</div>


						<div class="secaoObras">
							<ul>

								<div class="gestaoObras">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegObras" src="images/solucoes/Logistica/yms-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosObras conteinerBeneficioObras">
							<div class="beneficiosObras beneficioObras">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Otimização na utilização dos veículos disponíveis</li>
									<li>Facilidade de planejamento do tráfego de todos veículos dentros da empresa</li>
									<li>Minimização do acúmulo de tarefas no horário de recebimentos e despachos</li>
									<li>Minimização de eventuais furtos</li>
									<li>Possibilidade de comunicação direta com os motoristas</li>
									<li>Coleta de informações valiosas para facilitar o planejamento e tomada de decisões</li>
									<li>Minimização de custos de operação nos estacionamentos e em áreas de manobras</li>
									<li>Melhor atendimento aos fornecedores e clientes.</li>
								</ul>
							</div>


							<div class="caracteristicasObras">
								<p>Características do Sistema</p>

								<ul class="caracteristicaObras">
									<li>Gestão de ordens de serviço</li>
									<li>Gestão do tráfego de pedestres</li>
									<li>Alertas</li>
									<li>Análise de processos</li>
									<li>Atribuição automática de localizações</li>
									<li>Livro de registro de entradas e saídas</li>
									<li>Gestão de vagas de estacionamento</li>
									<li>Controle de pontualidade</li>
									<li>Sistema de controle</li>
									<li>Sistema de notificações automáticas</li>
									<li>Biblioteca de modelos de visitas</li>
									<li>Espera de serviço (Buffering)</li>
									<li>Identificação de recursos</li>
									<li>Definição de regras de serviço</li>
									<li>Alerta aos visitantes</li>
								</ul>
							</div>
						</aside>
					</section-->
			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeObras">
					<div class="textoBackofficeObras">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesObras">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
