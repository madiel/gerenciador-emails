<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Sobre - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-137221678-1');
		</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->
		<!--ScrollReveal -->
		<script src="./plugins/ScrollReveal/scrollreveal.min.js"></script>
		<!--ScrollReveal-->
    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- BannerInstitucional -->
			<div class="conteudoBanner">
				<div class="imagemBannerInstitucional">
					<img class="imgInstitucional" src="images/banner-topo-enllevo.jpg" title="Enllevo - Soluções que Geram Satisfação" alt="Institucional">
				</div>
				<div class="textoBannerInstitucional">
					<h1 class="sobreBanner">Sobre</h1>
					<span class="opcoesBannerInstitucional">
						<a class="destaqueVerde" href="{{ route('institucional') }}">Sobre</a>   |
						<a href="{{ route('linhadotempo') }}">Modo História</a>
					</span>
				</div>
			</div>
			<!-- BannerInstitucional -->

			<!--BannerSecundario-->
			<div class="backgroundInstitucional">
				<span class="caminhoSite">
					<a href="{{ route('index') }}">home</a> /
					<a href="{{ route('institucional') }}" class="destaqueAzul">Institucional</a>
				</span>
				<section>
					<img class="imgBotoes1024" src="images/mobile320/img-institucional.jpg" alt="Banner Secundario" title="Enllevo - Soluções que Geram Satisfação">
				</section>
			<!--BannerSecundario-->

			<!-- HistoriaEnllevo (resolução1024px)-->
			<section class="historiaEnllevo1024">
				<h1 class="tituloInstitucional1024">A enllevo hoje...</h1>
				<div class="caixaConteudoEnllevo">
					<p class="paragrafoHistoriaEnllevo1024">
						Referência nacional no desenvolvimento de soluções, em que os processos de negócios exigem aprofundamento em
						requisitos específicos, utilizando práticas em gestão empresarial, logística, gestão de pessoas, relacionamento
						com clientes, consultorias em tecnologia da informação e segurança, permitindo a total integração de sua gestão
						ao seu modelo estratégico. Nossas soluções são integradas e atendem tanto o core business quanto o back office
						dos nossos clientes.
					</p>
					<p class="paragrafoHistoriaEnllevo1024">
						A Enllevo, que possui o DNA da Shapeness, é uma empresa brasileira com mais de 33 anos no mercado e detentora
						de toda a tecnologia e recursos de gestão necessários ao desenvolvimento e soluções de negócios, oferecendo
						recursos de ponta em software para os mais variados segmentos do mercado. Esse é o legado que a Enllevo deseja
						carregar como herança. Uma empresa que está preocupada em oferecer, na medida certa, as melhores e mais adequadas
						soluções para uma gestão profícua e equilibrada.
					</p>
					<p class="paragrafoHistoriaEnllevo1024">
						Nossas equipes são altamente experientes e qualificadas, nossos colaboradores reúnem o que existe de melhor em
						formação, experiência e desempenho para satisfazer os clientes, solucionando os problemas de maneira participativa
						e impulsionando o crescimento no mercado.
					</p>
					<p class="paragrafoHistoriaEnllevo1024">
						A Enllevo participa do dia-a-dia de seus clientes gerando uma parceria efetiva, visando sempre à melhor solução
						para os mesmos, entregando satisfação.
					</p>

					<div class="botoesConceitoLinhaDoTempo">
						<a class="botaoConceitoMarca" href="#">VEJA O CONCEITO DA MARCA</a>
						<a class="botaoLinhaTempo" href="{{ route('linhadotempo') }}">VEJA NOSSA LINHA DO TEMPO</a>
					</div>
				</div>


			</section>
			<!-- HistoriaEnllevo (resolução1024px)-->

			<!-- HistoriaEnllevo -->

			<section class="historiaEnllevo">
				<h1 class="tituloInstitucional">A enllevo hoje...</h1>
				<div class="caixaConteudo">
					<p class="paragrafoHistoriaEnllevo">
						Referência nacional no desenvolvimento de soluções, em que os processos de negócios exigem aprofundamento em
						requisitos específicos, utilizando práticas em gestão empresarial, logística, gestão de pessoas, relacionamento
						com clientes, consultorias em tecnologia da informação e segurança, permitindo a total integração de sua gestão
						ao seu modelo estratégico. Nossas soluções são integradas e atendem tanto o core business quanto o back office
						dos nossos clientes.
					</p>
					<p class="paragrafoHistoriaEnllevo invisivel">
						A Enllevo, que possui o DNA da Shapeness, é uma empresa brasileira com mais de 33 anos no mercado e detentora
						de toda a tecnologia e recursos de gestão necessários ao desenvolvimento e soluções de negócios, oferecendo
						recursos de ponta em software para os mais variados segmentos do mercado. Esse é o legado que a Enllevo deseja
						carregar como herança. Uma empresa que está preocupada em oferecer, na medida certa, as melhores e mais adequadas
						soluções para uma gestão profícua e equilibrada.
					</p>
					<p class="paragrafoHistoriaEnllevo invisivel">
						Nossas equipes são altamente experientes e qualificadas, nossos colaboradores reúnem o que existe de melhor em
						formação, experiência e desempenho para satisfazer os clientes, solucionando os problemas de maneira participativa
						e impulsionando o crescimento no mercado.
					</p>
					<p class="paragrafoHistoriaEnllevo invisivel">
						A Enllevo participa do dia-a-dia de seus clientes gerando uma parceria efetiva, visando sempre à melhor solução
						para os mesmos, entregando satisfação.
					</p>
				</div>
				<span class="botaoRedondo exibirTexto">mais...</span>
				<span class="botaoRedondo recolherTexto invisivel">menos...</span>
			</section>

			<!--BotoesVejaCenceito/VejaLinhadoTempo-->

						<div class="bannerSecundario">
							<a class="botaoConceitoMarca" href="#" target="_blank">VEJA O CONCEITO DA MARCA</a>
							<a class="botaoLinhaTempo" href="{{ route('linhadotempo') }}">VEJA NOSSA LINHA DO TEMPO</a>
							<img class="imgBotoes" src="images/mobile320/img-institucional.jpg" alt="Banner Secundario" title="Enllevo - Soluções que Geram Satisfação">
						</div>

			<!--BotoesVejaCenceito/VejaLinhadoTempo-->

			<!--OQueOferecemos-->
						<aside class="secaoOferecemos">
							<h2 class="tituloOferecemos">O que oferecemos...</h2>

							<div class="divTextoOferecemos">
								<p class="textoOferecemos">
									A tecnologia só cumpre o seu papel quando de fato é bem empregada, oferecendo recursos em diversas áreas, facilitando processos,
									acelerando as comunicações e gerando resultados rápidos.
								</p>

								<p class="textoOferecemos">
									Na Enllevo, temos como foco pensar em soluções sob medida para a sua empresa,
									ou seja, correta e com um custo justo, para atender as necessidades do cliente.
								</p>

								<p class="ultimoTextoOferecemos">
									A tecnologia Enllevo e o time de profissionais solucionam os problemas da empresa de maneira estratégica,
									fazem com que a tecnologia esteja sempre a favor do cliente, oferecendo todo o suporte necessário para um cliente satisfeito.
									"Inovação não é só tecnologia: é pensar e agir diferente, de forma assertiva"
								</p>
							</div>
						</aside>

			<!--OQueOferecemos-->

			<!--Tecnologias (resolução 1024px)-->

					<div class="caixaTecnologias">


						<div class="secaoIOT">
							<img class="internetDasCoisasImg" src="images/mobile320/internet-das-coisas.png" alt="Internet das Coisas" title="Enllevo - Internet das Coisas">
							<h4>INTERNET DAS COISAS</h4>
							<p>
								Conecte pessoas, coisas e negócios com soluções e estratégias inteligentes.
								Crie novos modelos de negócios, aproveite informações em tempo real e dê suporte a processos adaptáveis de ponta a ponta.
							</p>
						</div>

						<div class="secaoInternetDados">
							<img class="internetDosDadosImg" src="images/mobile320/inteligencia-de-dados.png" alt="Internet de Dados" title="Enllevo - Internet de Dados">
							<h4>INTERNET DE DADOS</h4>
							<p>
								Utilizar os dados de forma inteligente.
								A inteligência de dados conecta e agrega seus dados, tornando-os anônimos, a fim de prepará-los para uso comercial.
								Ajudamos a aplicar algoritmos aos dados e a criar um serviço de insights de dados ou aplicativo baseado em dados
								para sua empresa e clientes.
							</p>
						</div>

						<div class="secaoDesignUIUX">
							<img class="designUIUXImg" src="images/mobile320/design-ui-ux.png" alt="Design UI/UX" title="Enllevo - Design UI/UX">
							<h4>DESIGN UI / UX</h4>
							<p>
								USER INTERFACE "UI", Responsável pela parte visual de um projeto, por onde o usuário interage com a interface.
								USER EXPERIENCE "UX",  parte emocional, o quão amigável a interação pode ser e os sentimentos que proporciona ao realiza-la.
								Engloba toda a estratégia de design, desenvolvimento, usabilidade e arquitetura de informação.
							</p>
						</div>

						<div class="secaoFuncAnaliticas">
							<img class="FuncAnaliticasImg" src="images/mobile320/funcoes-analyticas.png" alt="Funções Analyticas" title="Enllevo - Funções Analyticas">
							<h4>FUNÇÕES ANALÍTICAS</h4>
							<p>
								Baseadas em uma estratégia de uso de insights analíticos para impulsionar ações de negócios,
								nossas plataformas de funções analíticas comportam todas as fases do ciclo de vida da análise,
								dos dados até a descoberta e implantação.
							</p>
						</div>

						<div class="secaoDesignThinking">
							<img class="designThinkingImg" src="images/mobile320/design-finghs.png" alt="Design Thinking" title="Enllevo - Design Thinking">
							<h4>DESIGN THINKING</h4>
							<p>
								Conjunto de métodos e processos para abordar problemas,
								relacionados a futuras aquisições de informações, análise de conhecimento e propostas de soluções.
							</p>
						</div>

						<div class="secaoInterfaceAdaptavel">
							<img class="interfaceAdaptavelImg" src="images/mobile320/interface-adaptavel.png" alt="Interface Adaptável" title="Enllevo - Interface Adaptável">
							<h4>INTERFACE ADAPTAVEL</h4>
							<p>
								Interfaces Adaptativas, conhecidas também como Interfaces Inteligentes, são interfaces capazes de se adaptar
								a diferentes tipos de usuários fazendo com que os mesmos possam reorganizar os módulos apresentados na
								interface da forma que achar mais agradável para seu uso.
							</p>
						</div>

						<div class="secaoInfraSegura">
							<img class="InfraSeguraImg" src="images/mobile320/infraestrutura-rede.png" alt="Infraestrutura de Rede" title="Enllevo - Infraestrutura de Rede">
							<h4>INFRAESTRUTURA SEGURA</h4>
							<p>
								Em cada etapa de sua jornada, os dados provenientes de dispositivos confiáveis autenticados precisam ser protegidos,
								caso contrário ele poderia cair em mãos erradas.
								Através de uma combinação de técnicas como criptografia de dados e proteção de integridade,
								podemos mitigar os riscos de ataques cibernéticos.
							</p>
						</div>

						<div class="secaoComWebService">
							<img class="comWebServiceImg" src="images/mobile320/comunicacao-web-service.png" alt="Comunicação Web Service" title="Enllevo - Comunicação Web Service">
							<h4 class="textoSecao8">COMUNICAÇÃO WEB SERVICE</h4>
							<p>
								Solução utilizada na integração de sistemas e na comunicação entre aplicações diferentes.
								Com esta tecnologia é possível que novas aplicações	possam interagir com aquelas que já existem
								e que sistemas desenvolvidos em plataformas diferentes sejam compatíveis.
							</p>
						</div>

						<div class="secaoCloudPlataform">
							<img class="cloudPlataformImg" src="images/mobile320/cloud-plataforma.png" alt="Cloud Plataform" title="Enllevo - Cloud Plataform">
							<h4>CLOUD PLATFORM</h4>
							<p>
								Permitir que negócios executem as suas aplicações com mais agilidade, escalabilidade e segurança.
								Web apps, por exemplo, podem contar com um back-end mais estruturado e escalável.
								A Enllevo usa de maneira transacional o meio de comunicaçao browser,
								exigindo menos equipamento e banda larga, e ainda assim, acesso ilimitado.
							</p>
						</div>

						<div class="secaoSuporteEficiente">
							<img class="suporteEficienteImg" src="images/mobile320/suporte-eficiente.png" alt="Suporte Eficiente" title="Enllevo - Suporte Eficiente">
							<h4>SUPORTE EFICIENTE</h4>
							<p>
								Equipe de profissionais capacitados, de prontidão a resolver problemas e esclarecer dúvidas.
								É um suporte que prioriza a resolução de todas as solicitações, que abrangem, o mais eficiente do mercado.
								Instalação do certificado,
								Mapeamento e resolução de problemas,
								Esclarecimento de dúvidas sobre aplicabilidade do certificado digital,
								Entre outros.
							</p>
						</div>

					</div>


			<!--Tecnologias (resolução 1024px)-->

			<!--MissaoVisaoValores-->
						<section class="secaoMissaoVisaoValores">
							<div class="missao">
								<img class="imgMissao1024" src="images/mobile320/icone-missao.png" alt="MissaoEnllevo" title="Missão Enllevo">
								<h5>MISSÃO</h5>
								<img class="imgMissao" src="images/mobile320/icone-missao.png" alt="MissaoEnllevo" title="Missão Enllevo">
								<p>
									Ser referência no desenvolvimento de soluções
									personalizadas, atuando sempre de maneira assertiva
									juntamente com tecnologia de ponta para os mais variados
									segmentos do mercado.
								</p>
							</div>
							<div class="visao">
								<img class="imgVisao1024" src="images/mobile320/icone-visao.png" alt="VisaoEnllevo" title="Visão Enllevo">
								<h5>VISÃO</h5>
								<img class="imgVisao" src="images/mobile320/icone-visao.png" alt="VisaoEnllevo" title="Visão Enllevo">
								<p>"Ser a melhor escolha entre as principais empresas do mercado,
								entregando soluções que geram satisfação."</p>
							</div>

							<div class="valores">
								<img class="imgValores1024" src="images/mobile320/icone-valores.png" alt="ValoresEnllevo" title="Valores Enllevo">
								<h5>VALORES</h5>
								<img class="imgValores" src="images/mobile320/icone-valores.png" alt="ValoresEnllevo" title="Valores Enllevo">
								<ul class="listaValores">
									<li>Satisfação do cliente;</li>
									<li>Responsabilidade social;</li>
									<li>Ética;</li>
									<li>Confiabilidade;</li>
									<li>Inovação;</li>
									<li>Maximização dos recursos;</li>
									<li>Paixão pelo que faz, sempre aberto às evoluções;</li>
									<li>Conceito de equipe, sempre pronta a satisfazer o cliente.</li>
								</ul>
							</div>

							<h3 class="textoSatisfacao">
								"Entendemos o seu segmento, conhecemos de tecnologia, entregamos soluções e geramos satisfação."
							</h3>
						</section>

			<!--MissaoVisaoValores-->

			<!--InternetDasCoisas-->

					<div class="internetDasCoisas">


						<div class="secaoIOT1" onclick="AbrirFecharIOT('.IOTDados1', '.secaoIOT1')">
							<img class="internetDasCoisasImg img01" src="images/mobile320/internet-das-coisas.png" alt="Internet das Coisas" title="Enllevo - Internet das Coisas">
							<h4>INTERNET DAS COISAS</h4>
						</div>

						<div class="secaoIOTDados IOTDados1 invisivel">
							<p>
								Conecte pessoas, coisas e negócios com soluções e estratégias inteligentes.
								Crie novos modelos de negócios, aproveite informações em tempo real e dê suporte a processos adaptáveis de ponta a ponta.
							</p>
						</div>

						<div class="secaoIOT2 " onclick="AbrirFecharIOT('.IOTDados2','.secaoIOT2')">
							<img class="internetDasCoisasImg img02" src="images/mobile320/inteligencia-de-dados.png" alt="Internet de Dados" title="Enllevo - Internet de Dados">
							<h4>INTERNET DE DADOS</h4>
						</div>

						<div class="secaoIOTDados IOTDados2 invisivel">
							<p>
								Utilizar os dados de forma inteligente.
								A inteligência de dados conecta e agrega seus dados, tornando-os anônimos, a fim de prepará-los para uso comercial.
								Ajudamos a aplicar algoritmos aos dados e a criar um serviço de insights de dados ou aplicativo baseado em dados
								para sua empresa e clientes.
							</p>
						</div>

						<div class="secaoIOT3" onclick="AbrirFecharIOT('.IOTDados3','.secaoIOT3')">
							<img class="internetDasCoisasImg img03" src="images/mobile320/design-ui-ux.png" alt="Design UI/UX" title="Enllevo - Design UI/UX">
							<h4>DESIGN UI / UX</h4>
						</div>

						<div class="secaoIOTDados IOTDados3 invisivel">
							<p>
								USER INTERFACE "UI", Responsável pela parte visual de um projeto, por onde o usuário interage com a interface.
								USER EXPERIENCE "UX",  parte emocional, o quão amigável a interação pode ser e os sentimentos que proporciona ao realiza-la.
								Engloba toda a estratégia de design, desenvolvimento, usabilidade e arquitetura de informação.
							</p>
						</div>


						<div class="secaoIOT4" onclick="AbrirFecharIOT('.IOTDados4','.secaoIOT4')">
							<img class="internetDasCoisasImg img04" src="images/mobile320/funcoes-analyticas.png" alt="Funções Analyticas" title="Enllevo - Funções Analyticas">
							<h4>FUNÇÕES ANALÍTICAS</h4>
						</div>

						<div class="secaoIOTDados IOTDados4 invisivel">
							<p>
								Baseadas em uma estratégia de uso de insights analíticos para impulsionar ações de negócios,
								nossas plataformas de funções analíticas comportam todas as fases do ciclo de vida da análise,
								dos dados até a descoberta e implantação.
							</p>
						</div>


						<div class="secaoIOT5" onclick="AbrirFecharIOT('.IOTDados5','.secaoIOT5')">
							<img class="internetDasCoisasImg img05" src="images/mobile320/design-finghs.png" alt="Design Thinking" title="Enllevo - Design Thinking">
							<h4>DESIGN THINKING</h4>
						</div>

						<div class="secaoIOTDados IOTDados5 invisivel">
							<p>
								Conjunto de métodos e processos para abordar problemas,
								relacionados a futuras aquisições de informações, análise de conhecimento e propostas de soluções.
							</p>
						</div>


						<div class="secaoIOT6" onclick="AbrirFecharIOT('.IOTDados6','.secaoIOT6')">
							<img class="internetDasCoisasImg img06" src="images/mobile320/interface-adaptavel.png" alt="Interface Adaptável" title="Enllevo - Interface Adaptável">
							<h4>INTERFACE ADAPTAVEL</h4>
						</div>

						<div class="secaoIOTDados IOTDados6 invisivel">
							<p>
								Interfaces Adaptativas, conhecidas também como Interfaces Inteligentes, são interfaces capazes de se adaptar
								a diferentes tipos de usuários fazendo com que os mesmos possam reorganizar os módulos apresentados na
								interface da forma que achar mais agradável para seu uso.
							</p>
						</div>


						<div class="secaoIOT7" onclick="AbrirFecharIOT('.IOTDados7','.secaoIOT7')">
							<img class="internetDasCoisasImg img07" src="images/mobile320/infraestrutura-rede.png" alt="Infraestrutura de Rede" title="Enllevo - Infraestrutura de Rede">
							<h4>INFRAESTRUTURA SEGURA</h4>
						</div>

						<div class="secaoIOTDados IOTDados7 invisivel">
							<p>
								Em cada etapa de sua jornada, os dados provenientes de dispositivos confiáveis autenticados precisam ser protegidos,
								caso contrário ele poderia cair em mãos erradas.
								Através de uma combinação de técnicas como criptografia de dados e proteção de integridade,
								podemos mitigar os riscos de ataques cibernéticos.
							</p>
						</div>

						<div class="secaoIOT8" onclick="AbrirFecharIOT('.IOTDados8','.secaoIOT8')">
							<img class="internetDasCoisasImg img08" src="images/mobile320/comunicacao-web-service.png" alt="Comunicação Web Service" title="Enllevo - Comunicação Web Service">
							<h4 class="textoSecao8">COMUNICAÇÃO WEB SERVICE</h4>
						</div>

						<div class="secaoIOTDados IOTDados8 invisivel">
							<p>
								Solução utilizada na integração de sistemas e na comunicação entre aplicações diferentes.
								Com esta tecnologia é possível que novas aplicações	possam interagir com aquelas que já existem
								e que sistemas desenvolvidos em plataformas diferentes sejam compatíveis.
							</p>
						</div>

						<div class="secaoIOT9" onclick="AbrirFecharIOT('.IOTDados9','.secaoIOT9')">
							<img class="internetDasCoisasImg img09" src="images/mobile320/cloud-plataforma.png" alt="Cloud Plataform" title="Enllevo - Cloud Plataform">
							<h4>CLOUD PLATFORM</h4>
						</div>

						<div class="secaoIOTDados IOTDados9 invisivel">
							<p>
								Permitir que negócios executem as suas aplicações com mais agilidade, escalabilidade e segurança.
								Web apps, por exemplo, podem contar com um back-end mais estruturado e escalável.
								A Enllevo usa de maneira transacional o meio de comunicaçao browser,
								exigindo menos equipamento e banda larga, e ainda assim, acesso ilimitado.
							</p>
						</div>


						<div class="secaoIOT10" onclick="AbrirFecharIOT('.IOTDados10','.secaoIOT10')">
							<img class="internetDasCoisasImg img10" src="images/mobile320/suporte-eficiente.png" alt="Suporte Eficiente" title="Enllevo - Suporte Eficiente">
							<h4>SUPORTE EFICIENTE</h4>
						</div>

						<div class="secaoIOTDados IOTDados10 invisivel">
							<p>
								Equipe de profissionais capacitados, de prontidão a resolver problemas e esclarecer dúvidas.
								É um suporte que prioriza a resolução de todas as solicitações, que abrangem, o mais eficiente do mercado.
								Instalação do certificado,
								Mapeamento e resolução de problemas,
								Esclarecimento de dúvidas sobre aplicabilidade do certificado digital,
								Entre outros.
							</p>
						</div>

						<h3 class="textoInternetDasCoisas">
						"Entendemos o seu segmento, conhecemos de tecnologia, entregamos soluções e geramos satisfação."
						</h3>
					</div>


			<!--InternetDasCoisas-->

			<!--PactoGlobal-->

					<aside class="secaoPactoGlobal">
						<p class="tituloPacto">
							PACTO GLOBAL
						</p>

						<p class="textoPacto">
							Promovemos otimização de rotinas, redução de custos,
							produtividade e análise de dados estratégicos para a tomada de decisão.
						<p class="contTextoPacto">
							Oferecendo uma perspectiva real da performance corporativa para os gestores,
							a Enllevo pode ser uma grande aliada para os seus negócios,
							trazendo a solução adequada, sempre de maneira assertiva.
						</p>
					</aside>
					<div class="equipeEnllevoIcone">
					<p class="equipeEnllevo">A equipe enllevo...</p>
					<span class="iconeSetaMenuSite"></span>
					</div>
				</div>
			<!--PactoGlobal-->

			<!-- EquipeEnllevo (resolução 1024px) -->
			<section class="caixaEquipeEnllevo1024">
				<h3 class="TituloEquipeEnllevo1024">A equipe enllevo</h3>
				<p class="ParagrafoEquipeEnllevo1024">
					Você pode confiar seu projeto a nossa equipe de profissionais experientes e comprometidos.
				</p>
			</div>
				<div class="caixaEquipe1024">
					<div class="conteinerPerfilProfissional1024 conteinerEnllevo">

					<!-- Edison Consolin -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/edison-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador1 invisivel">
								<h3 class="nomeColaborador1024">Edison Consolin</h3>
								<span class="cargoColaborador1024">Diretor Geral</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Sócio Fundador e Diretor da Shapeness em 1986, hoje Enllevo. 
										Graduado em Administração pela Universidade São Marcos, 
										Liderando pessoalmente projetos nos segmentos Automotivo, Fabril, 
										Contábil/Fiscal, ERP, desenvolvendo estratégia aliada ao acompanhamento 
										do mercado e serviços em empresas de variados portes e atuações.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/edison-consolin-05a86484/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
								<div class="cabecalhoModal">
									X
								</div>
								<div class="textoModalInfo">
									Sócio Fundador e Diretor da Shapeness em 1986, hoje Enllevo. 
									Graduado em Administração pela Universidade São Marcos, 
									Liderando pessoalmente projetos nos segmentos Automotivo, Fabril, 
									Contábil/Fiscal, ERP, desenvolvendo estratégia aliada ao acompanhamento 
									do mercado e serviços em empresas de variados portes e atuações.
								</div>
							</div>
						</figure>

					

						<!-- Marco Bittencourt -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/marco-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Marco Bittencourt</h3>
								<span class="cargoColaborador1024">Diretor de Implantação e Análise</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Sócio proprietário há mais de 25 anos da Enllevo by Shapeness. 
										Ampla experiência em diversos ramos corporativos, responsável pelo suporte aos clientes,
										 treinamento,  metodologias ágeis, implantação de sistema. 
										 Competências em produtos destinados à gestão de concessionárias de veículos, 
										 motores elétricos, mantendo nosso sistema em compliance com as regras de negócios.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<!-- Redes Sociais
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
								 Redes Sociais -->
							</figcaption>

							<div class="modalInfoColab invisivel">
								<div class="cabecalhoModal">
									X
								</div>
								<div class="textoModalInfo">
									Sócio proprietário há mais de 25 anos da Enllevo by Shapeness. 
									Ampla experiência em diversos ramos corporativos, responsável pelo suporte aos clientes,
									treinamento,  metodologias ágeis, implantação de sistema. 
									Competências em produtos destinados à gestão de concessionárias de veículos, 
									motores elétricos, mantendo nosso sistema em compliance com as regras de negócios.
								</div>
							</div>
						</figure>

						

						<!-- José Carlos Pereira -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/jose-carlos-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador1 invisivel">
								<h3 class="nomeColaborador1024">José Carlos Pereira</h3>
								<span class="cargoColaborador1024">Diretor de Projetos</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
									Sócio proprietário há mais de 20 anos da Enllevo by Shapeness, 
									MBA em Segurança da Informação e Advogado formado pela Universidade Estácio. 
									Ampla experiência em diversos ramos corporativos, gestor de projetos de software,
									 metodologias ágeis, implantação de sistema e coordenação de SSO. 
									 Competências em produtos destinados à gestão de concessionárias, 
									 mantendo nosso sistema em compliance com as regras de negócios.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/jos%C3%A9-carlos-pereira-a636ba4a/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
								<div class="cabecalhoModal">
									X
								</div>
								<div class="textoModalInfo">
									Sócio proprietário há mais de 20 anos da Enllevo by Shapeness, 
									MBA em Segurança da Informação e Advogado formado pela Universidade Estácio. 
									Ampla experiência em diversos ramos corporativos, gestor de projetos de software,
									 metodologias ágeis, implantação de sistema e coordenação de SSO. 
									 Competências em produtos destinados à gestão de concessionárias, 
									 mantendo nosso sistema em compliance com as regras de negócios.
								</div>
							</div>
						</figure>

						

						<!-- Silvio Cesar Lopes -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/silvio-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Silvio Cesar Lopes</h3>
								<span class="cargoColaborador1024">Diretor Técnico CTO</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
								<div class="cabecalhoModal">
									X
								</div>
								<div class="textoModalInfo">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</div>
							</div>

						</figure>

						
						<!-- Helcio Silva -->
						<figure class="figureColaborador1024 figure1024"><!--margin 0px-->
							<img class="imgColaborador1024" src="images/equipe/helcio-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Helcio Silva</h3>
								<span class="cargoColaborador1024">Diretor de Desenvolvimento de Negócios</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

					

						<!-- Sergio Martins Ferreira -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/sergio-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Sergio Martins Ferreira</h3>
								<span class="cargoColaborador1024">Programador Cobol / Coodernador de Desenvolvimento</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Analista de sistemas com mais de 30 anos de experiência em análise e desenvolvimento
										de sistemas ERP, Automação comercial e Recursos Humanos, utilizando linguagem Cobol, 
										HTML, JavaScript e PHP. Formado em Ciências da Computação e MBA em Gestão de Tecnologia
										e Internet pela Uninove.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/sergio-martins-ferreira-6325022a/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Analista de sistemas com mais de 30 anos de experiência em análise e desenvolvimento
								de sistemas ERP, Automação comercial e Recursos Humanos, utilizando linguagem Cobol, 
								HTML, JavaScript e PHP. Formado em Ciências da Computação e MBA em Gestão de Tecnologia
								e Internet pela Uninove.
							</div>
						</div>
						</figure>

						

						<!-- Eleandro Carlos da Rosa -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/eleandro-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Eleandro Carlos da Rosa</h3>
								<span class="cargoColaborador1024">Analista Tributário</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>


						

						<!-- José Mauro Prestes de Moraes -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/mauro-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">José Mauro Prestes de Moraes</h3>
								<span class="cargoColaborador1024">Gerente de Projetos</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Fabíola Martin -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/fabiola-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Fabíola Martin</h3>
								<span class="cargoColaborador1024">Analista de Negócios</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Jessica Verdu -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/jessica-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Jessica Verdu</h3>
								<span class="cargoColaborador1024">Analista de RH</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Formada em Gestão de Recursos Humanos e Cursando atualmente Ciências Contábeis. 
										Atuo 09 anos na área de Recursos Humanos como generalista com ênfase na Area Fopag
										 (Folha de pagamento). Consultoria em eSocial e Recursos humanos, responsável pela 
										 implantação do eSocial, e conhecimento integrado em DCTFWEB-REINF.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/jessica-aline-verdu-59b006103/') }}'" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Formada em Gestão de Recursos Humanos e Cursando atualmente Ciências Contábeis. 
								Atuo 09 anos na área de Recursos Humanos como generalista com ênfase na Area Fopag
								(Folha de pagamento). Consultoria em eSocial e Recursos humanos, responsável pela 
								implantação do eSocial, e conhecimento integrado em DCTFWEB-REINF.
							</div>
						</div>
						</figure>

						

						<!-- Bruno Rafael -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/bruno-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Bruno Rafael</h3>
								<span class="cargoColaborador1024">Desenvolvedor Front-End Pleno</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Desenvolvedor Front-End Pleno e Programador PHP, 
										Formado em Desenvolvimento de Sistemas, pela Universidade Nove de Julho. 
										Experiencias com HTML, CSS, JavaScript, Responsivo, Mobile, React JS, 
										Banco de Dados, PHP, Angular Js, Lógica de Programação, Git, 
										Pré processadores CSS e Desenvolvimentos de Sistemas e Web Sites.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/bruno-rafael-filgueira-dias/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Desenvolvedor Front-End Pleno e Programador PHP, 
								Formado em Desenvolvimento de Sistemas, pela Universidade Nove de Julho. 
								Experiencias com HTML, CSS, JavaScript, Responsivo, Mobile, React JS, 
								Banco de Dados, PHP, Angular Js, Lógica de Programação, Git, 
								Pré processadores CSS e Desenvolvimentos de Sistemas e Web Sites.
							</div>
						</div>
						</figure>

						

						<!-- Maria Dolores -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/maria-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Maria Dolores</h3>
								<span class="cargoColaborador1024">Desenvolvedora Front-End</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Desenvolvedora Front-End com Formação em Ciência da Computação 
										pela Universidade São Judas Tadeu, Experiencias com HTML, CSS, JavaScript, 
										Responsivo, Mobile, React, Banco de dados e entre outras.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/maria-d-m-babosa-santos/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Desenvolvedora Front-End com Formação em Ciência da Computação 
								pela Universidade São Judas Tadeu, Experiencias com HTML, CSS, JavaScript, 
								Responsivo, Mobile, React, Banco de dados e entre outras.
							</div>
						</div>
						</figure>

						

						<!-- João de Souza -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/joao-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">João de Souza</h3>
								<span class="cargoColaborador1024">Designer UI/UX / Coodernador de Inovação</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Designer UI/UX, Formado pela Faculdade Impacta de Tecnologia, 
										"User Experience, Mockups, Design Thinking, Softwares para prototipagem,
										Testes de UX e usabilidades." Criação de Identidade Visual, 
										Layouts, Prototipos, Designer Digital, Gráfico, SEO, Midias Sociais
										e com experiência na Direção de Arte, Front-End, Plataforma Mac/Windows.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeInstagram">
										<a href="https://www.instagram.com/joaodesouza.com.br/" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/joaodesouza/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Designer UI/UX, Formado pela Faculdade Impacta de Tecnologia, 
								"User Experience, Mockups, Design Thinking, Softwares para prototipagem,
								Testes de UX e usabilidades." Criação de Identidade Visual, 
								Layouts, Prototipos, Designer Digital, Gráfico, SEO, Midias Sociais
								e com experiência na Direção de Arte, Front-End, Plataforma Mac/Windows.
							</div>
						</div>
						</figure>

						


						<!-- Érycson Nóbrega -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/erycson-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Érycson Nóbrega</h3>
								<span class="cargoColaborador1024">Desenvolvedor Backend Senior</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Madiel França -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/madiel-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Madiel França</h3>
								<span class="cargoColaborador1024">Desenvolvedor Full Stack</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Desenvolvedor Full Stack Pleno, Cursando Ciêcia da Computação, 
										na Universidade Paulista. Experiencias com HTML, CSS, JavaScript, 
										Responsivo, Mobile, Laravel, Banco de Dados, PHP, Lógica de Programação,
										Git, Pré processadores CSS, Web Sites e Infra Estrutura.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/madiel-fran%C3%A7a-318609146/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Desenvolvedor Full Stack Pleno, Cursando Ciêcia da Computação, 
								na Universidade Paulista. Experiencias com HTML, CSS, JavaScript, 
								Responsivo, Mobile, Laravel, Banco de Dados, PHP, Lógica de Programação,
								Git, Pré processadores CSS, Web Sites e Infra Estrutura.
							</div>
						</div>

						</figure>

						
						<!-- Alison Torte -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/alisson-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Alison Torte</h3>
								<span class="cargoColaborador1024">Analista de Suporte</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Willian Bressiani -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/enllevo-equipe.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Willian Bressiani</h3>
								<span class="cargoColaborador1024">Programador PHP</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Marta Araujo -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/marta-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Marta Araujo</h3>
								<span class="cargoColaborador1024">Auxiliar de Limpeza</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Encarregada e auxiliar de serviços de limpeza, 
										profissional responsável por auxiliar na limpeza e conservação do local e 
										ambiente de trabalho.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<!--
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
								-->
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Encarregada e auxiliar de serviços de limpeza, 
								profissional responsável por auxiliar na limpeza e conservação do local e 
								ambiente de trabalho.
							</div>
						</div>
						</figure>

						

						<!-- Amanda Ramalho -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/enllevo-equipe.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Amanda Ramalho</h3>
								<span class="cargoColaborador1024">Analista de Negócios</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Analista de Suporte e Desenvolvimento de projetos para ERP 
										com ênfase nos sistemas TMS e WMS. Graduada em Tecnologia em 
										Informática para Negócios e Gestão Empresarial pela Fatec São Bernardo.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/amanda-ramalho-0a770b66/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Analista de Suporte e Desenvolvimento de projetos para ERP 
								com ênfase nos sistemas TMS e WMS. Graduada em Tecnologia em 
								Informática para Negócios e Gestão Empresarial pela Fatec São Bernardo.
							</div>
						</div>
						</figure>

						


						<!-- Marcelo Bergo -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/marcelo-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Marcelo Bergo</h3>
								<span class="cargoColaborador1024">Analista de Suporte</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Analista de Suporte, graduado em Desenvolvimento de 
										Sistemas pela Universidade Estacio de Sá. Instalação e 
										configuração servidores Linux , suporte a usuários do sistema ERP , 
										instalação e manutenção maquinas em VMWare.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/marcelo-bergo-da-costa/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Analista de Suporte, graduado em Desenvolvimento de 
								Sistemas pela Universidade Estacio de Sá. Instalação e 
								configuração servidores Linux , suporte a usuários do sistema ERP , 
								instalação e manutenção maquinas em VMWare.
							</div>
						</div>
						</figure>

						

						<!-- Fernando Braidatto -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/fernando-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Fernando Braidatto</h3>
								<span class="cargoColaborador1024">Consultor em Lógistica</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Leonardo Arrana -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/leonardo-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Leonardo Arrana</h3>
								<span class="cargoColaborador1024">Analista de Suporte</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Analista de Suporte e Infraestrutura. 
										Formado em Analise e Desenvolivmento de Sistema pela Universidade Cruzeiro do Sul.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/leonardo-moreira-arrana-3b7b5636/?originalSubdomain=br') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
								<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Analista de Suporte e Infraestrutura. 
								Formado em Analise e Desenvolivmento de Sistema pela Universidade Cruzeiro do Sul.
							</div>
						</div>
						</figure>


					

						<!-- Ailton Antonio -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/ailton-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Ailton Antonio</h3>
								<span class="cargoColaborador1024">Analista de Suporte</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Sydney Oliveira -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/sidinei-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Sydney Oliveira</h3>
								<span class="cargoColaborador1024">Programador Cobol</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Alexandre Abreu -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/alexandre-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Alexandre Abreu</h3>
								<span class="cargoColaborador1024">Programador Cobol</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Anderson Mendes -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/anderson-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Anderson Mendes</h3>
								<span class="cargoColaborador1024">Analista de Suporte</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Graduado em Sistemas de Informação pela Universidade Fundação Santo André, 
										responsável pelo suporte, treinamento e implantação do sistema ERP, 
										instalações e atualizações de softwares.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">

									<li class="redeLinkedin">
										<a href="{{ url('https://www.linkedin.com/in/anderson-mendes-06/') }}" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Graduado em Sistemas de Informação pela Universidade Fundação Santo André, 
								responsável pelo suporte, treinamento e implantação do sistema ERP, 
								instalações e atualizações de softwares.
							</div>
						</div>
						</figure>

						

						<!-- Roberta -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/roberta-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Roberta</h3>
								<span class="cargoColaborador1024">Corretora</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- José Sabino -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/jose-sabino-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">José Sabino</h3>
								<span class="cargoColaborador1024">Corretor de Seguros</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						


						<!-- Thiago -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/thiago-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Thiago</h3>
								<span class="cargoColaborador1024">Desenvolvedor Full-Stack</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
								<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

					

						<!-- Natalia Consolin -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/natalia-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Natalia Consolin</h3>
								<span class="cargoColaborador1024">Leverage agile frameworks to vide robust</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

					

						<!-- Fernanda -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/fernanda-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Fernanda</h3>
								<span class="cargoColaborador1024">Leverage agile frameworks to vide robust</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>

						</figure>

						
						<!-- Osmar -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/osmar-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Osmar</h3>
								<span class="cargoColaborador1024">Leverage agile frameworks to vide robust</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						



						<!-- Julia -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/julia-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Júlia Maria Batista</h3>
								<span class="cargoColaborador1024">Representante da área comercial da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Graduação na ESPM em Relações Internacionais, com foco em Marketing e Negócios. 
										Conhecimento e experiência na área de vendas e marketing de campanha.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Andre -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/andre-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">André Miliosi</h3>
								<span class="cargoColaborador1024">Analista de Negócios da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de dezessete anos na área de tecnologia. Graduado em Sistema e pós-graduado em Tecnologia da Informação Bancária 
										(arquitetura e engenharia de softwares). Sua experiencia contempla análise e arquitetura de sistemas informacionais,
										gestão de equipes e projetos com metodologias SCRUM e PMI.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de dezessete anos na área de tecnologia. Graduado em Sistema e pós-graduado em Tecnologia da Informação Bancária 
								(arquitetura e engenharia de softwares). Sua experiencia contempla análise e arquitetura de sistemas informacionais,
								gestão de equipes e projetos com metodologias SCRUM e PMI.
							</div>
						</div>
						</figure>

						

						<!-- Ana Paula -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/01-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Ana Paula</h3>
								<span class="cargoColaborador1024">Programadora da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de xx anos na área de tecnologia. Graduada em Análise e Desenvolvimento de Sistemas. 
										Experiência e conhecimento em C, C#, Python, PHP, Javascript, e SQL.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de xx anos na área de tecnologia. Graduada em Análise e Desenvolvimento de Sistemas. 
								Experiência e conhecimento em C, C#, Python, PHP, Javascript, e, SQL.
							</div>
						</div>
						</figure>

						



						<!-- Lucas Sanches -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/02-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Lucas Sanches Paranhos</h3>
								<span class="cargoColaborador1024">Programador da ID-INNOVE</span>

									<div class="paragrafoInfoColaborador1024">
										<p>
											Atua há mais de xx anos na área de tecnologia. Formado em programação de jogos digitais, 
											graduando em análise e desenvolvimento de sistemas, 
											conhecimento em linguagens HTML, CSS e JavaScript, C#, C, SQL Server, Excel, Visual Basic, Action script
										</p>
									</div>
									<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de xx anos na área de tecnologia. Formado em programação de jogos digitais, 
								graduando em análise e desenvolvimento de sistemas, 
								conhecimento em linguagens HTML, CSS e JavaScript, C#, C, SQL Server, Excel, Visual Basic, Action script
							</div>
						</div>
						</figure>

						

						<!-- Guilherme Guimarães -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/03-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Guilherme Guimarães</h3>
								<span class="cargoColaborador1024">Arquiteto de Soluções da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de quatro anos na área de tecnologia. 
										Graduado em análise e desenvolvimento de sistemas, programador desenvolvedor C# .NET. 
										Experiencia em desenvolvimento web com arquitetura MVC, 
										Domínio de javascript, Jquery, HTML5, CSS, Bootstrap, React e também em desenvolvimento mobile com AngularJS, Ionic e cordova.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de quatro anos na área de tecnologia. 
								Graduado em análise e desenvolvimento de sistemas, programador desenvolvedor C# .NET. 
								Experiencia em desenvolvimento web com arquitetura MVC, 
								Domínio de javascript, Jquery, HTML5, CSS, Bootstrap, React e também em desenvolvimento mobile com AngularJS, Ionic e cordova.
							</div>
						</div>
						</figure>

						

						<!-- Dorval Fernandes -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/04-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Dorval Fernandes</h3>
								<span class="cargoColaborador1024">Arquiteto de Soluções da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de dezenove anos na área de tecnologia. Engenheiro Eletrônico com Ênfase em Computação e Técnico em Mecatrônica, 
										possui experiencias em automação industrial e desenvolvimento de sistemas, com vasto conhecimento em tecnologias de comunicação
										e protocolos de rede, IoT, Analise de Dados, Base de dados Relacionais, Base de dados não relacionais, 
										C, C++, C#, JavaScript, Assembler, Python, SQL, PHP, desenvolvimentos em .Net Framework e .Net Core.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de dezenove anos na área de tecnologia. Engenheiro Eletrônico com Ênfase em Computação e Técnico em Mecatrônica, 
								possui experiencias em automação industrial e desenvolvimento de sistemas, com vasto conhecimento em tecnologias de comunicação
								e protocolos de rede, IoT, Analise de Dados, Base de dados Relacionais, Base de dados não relacionais, 
								C, C++, C#, JavaScript, Assembler, Python, SQL, PHP, desenvolvimentos em .Net Framework e .Net Core.
							</div>
						</div>
						</figure>

						


						<!-- Danilo Araujo -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/05-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Danilo Araujo </h3>
								<span class="cargoColaborador1024">Programador da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de xx anos na área de tecnologia. Programador desenvolvedor nas linguagens VB6,
										VB.NET e C#, Banco de dados SQL SERVER e Aplicações WEB. Conhecimentos em PHP e Bootstrap 4.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de xx anos na área de tecnologia. Programador desenvolvedor nas linguagens VB6,
								VB.NET e C#, Banco de dados SQL SERVER e Aplicações WEB. Conhecimentos em PHP e Bootstrap 4.
							</div>
						</div>
						</figure>

						

						<!-- Ciro Peter -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/06-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Ciro Peter </h3>
								<span class="cargoColaborador1024">Cientista de Dados da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de xx anos na área de tecnologia. Formado em Engenharia da Computação e pós-graduando em Análise de Dados (Big-Data/Machine Learning). 
										Desenvolvedor C#, PHP, JAVA, JS/Jquery, SQL, Python. Experiência no desenvolvimento MVC com Asp.Net em PHP. 
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de xx anos na área de tecnologia. Formado em Engenharia da Computação e pós-graduando em Análise de Dados (Big-Data/Machine Learning). 
								Desenvolvedor C#, PHP, JAVA, JS/Jquery, SQL, Python. Experiência no desenvolvimento MVC com Asp.Net em PHP. 
							</div>
						</div>
						</figure>

						

						<!-- Luck luciano -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/08-prolead.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Lucky Luciano</h3>
								<span class="cargoColaborador1024">Programador da ID-INNOVE</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de xx anos na área de tecnologia. Formado em Sistemas de Informação. 
										Programador desenvolvedor C#, C, JAVA, Lua, Python, JS/Jquery, SQL, Python, R. 
										Experiência em tecnologias SmartGrid (implementação de algoritmos de otimização baseados em 
										simulações e modelos de inteligência computacional).
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de xx anos na área de tecnologia. Formado em Sistemas de Informação. 
								Programador desenvolvedor C#, C, JAVA, Lua, Python, JS/Jquery, SQL, Python, R. 
								Experiência em tecnologias SmartGrid (implementação de algoritmos de otimização baseados em 
								simulações e modelos de inteligência computacional).
							</div>
						</div>
						</figure>

						

						<!-- Daniela Consolin -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/enllevo-equipe.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Daniela Consolin</h3>
								<span class="cargoColaborador1024">Leverage agile frameworks to vide robust</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						

						<!-- Beatriz Consolin -->
						<figure class="figureColaborador1024">
							<img class="imgColaborador1024" src="images/equipe/enllevo-equipe.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Beatriz Consolin</h3>
								<span class="cargoColaborador1024">Leverage agile frameworks to vide robust</span>

								<div class="paragrafoInfoColaborador1024">
									<p>
										Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
										Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
										Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
								Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
								Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
							</div>
						</div>
						</figure>

						


						<!-- Fernanda -->
						<figure class="figureColaborador1024 figure1024">
							<img class="imgColaborador1024" src="images/equipe/fernanda-enllevo.jpg" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador1024 equipeNomeColaborador2 invisivel">
								<h3 class="nomeColaborador1024">Fernanda Sanches</h3>
								<span class="cargoColaborador1024">Analista de Negócios da ID-INNOVE</span>
								
								<div class="paragrafoInfoColaborador1024">
									<p>
										Atua há mais de seis meses na área de tecnologia.
										Experiência como analista de negócios, gestão de projetos e equipes, 
										marketing e gestão de relacionamento com clientes, apoio e orientação técnica à usuários e clientes. 
									</p>
								</div>
								<span class="adicaoTexto">+</span>
								<ul class="redesSociaisColaborador1024">
									<li class="redeFacebook">
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li class="redeInstagram">
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li class="redeLinkedin">
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
							<div class="modalInfoColab invisivel">
							<div class="cabecalhoModal">
								X
							</div>
							<div class="textoModalInfo">
								Atua há mais de seis meses na área de tecnologia.
								Experiência como analista de negócios, gestão de projetos e equipes, 
								marketing e gestão de relacionamento com clientes, apoio e orientação técnica à usuários e clientes.
							</div>
						</div>
						</figure>

						

				<span class="botaoRedondo exibirEquipe">mais...</span>
				<span class="botaoRedondo recolherEquipe invisivel">menos...</span>

					</div>
				</div>
			</section>
			<!-- EquipeEnllevo (resolução 1024px) -->

			<!-- EquipeEnllevo >
			<section class="caixaEquipeEnllevo">
				<h3 class="TituloEquipeEnllevo">A equipe enllevo</h3>
				<p class="ParagrafoEquipeEnllevo">
					Você pode confiar seu projeto a nossa equipe de profissionais experientes e comprometidos.
				</p>
				<div class="caixaEquipe">
					<div class="conteinerPerfilProfissional">
						<figure class="figureColaborador">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador1 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
						<figure class="figureColaborador">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador2 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
						<figure class="figureColaborador invisivel">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador1 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
						<figure class="figureColaborador invisivel">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador2 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
						<figure class="figureColaborador invisivel">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador2 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
						<figure class="figureColaborador invisivel">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador2 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
						<figure class="figureColaborador invisivel">
							<img class="imgColaborador" src="images/mobile320/depoimentos.png" alt="Colaborador" title="Enllevo - Colaborador">
							<figcaption class="infoColaborador nomeColaborador2 invisivel">
								<h3 class="nomeColaborador">Nome Colaborador</h3>
								<span class="cargoColaborador">Cargo Colaborador</span>
								<p class="paragrafoInfoColaborador">
									Leverage agile frameworks to vide robust synopsis for high holistic world view of disruptive,
									Mauris sit amet vulputate dolor. Vivamus bibendum nunc erat, vitae gravida leo efficitur id.
									Curabitur posuere ut libero at suscipit. Sed lacinia ut dui at ullamcorper.
								</p>
								<ul class="redesSociaisColaborador">
									<li>
										<a href="#" title="Facebook" target="_blank">
											<span class="iconeFacebook"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Instagram" target="_blank">
											<span class="iconeInstagran"></span>
										</a>
									</li>
									<li>
										<a href="#" title="Linkedin" target="_blank">
											<span class="iconeLinkedin"></span>
										</a>
									</li>
								</ul>
							</figcaption>
						</figure>
					</div>
				
				</div>
			</section -->
		
			<!-- EquipeEnllevo -->

			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
		<script>
			ScrollReveal().reveal('.figureColaborador1024', {
			delay: 400,
			useDelay: 'onload',
			reset: true,
			mobile: false,
		});
		</script>
    </body>
</html>
