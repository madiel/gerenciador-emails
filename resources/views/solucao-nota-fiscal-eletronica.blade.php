<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Nota Fiscal Eletrônica - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaNota">

					<div class="bannerNota">

					</div>

					<section class="bannerSolucaoNota">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-nota-fiscal-eletronica') }}" class="destaqueAzul">nota fiscal eletrônica</a>
							</span>
						</div>
							<span class="iconeSegNotaFiscal icone11"></span>
							<h1 class="tituloNota">Solução Nota Fiscal Eletrônica</h1>
							<h2 class="textIntroNota">Solução certa para todos os tipos de...</h2>
							<h3 class="contTextIntroNota">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
					</div>

					<div>
						<img class="imgNota" src="images/solucoes/Nota-Fiscal-Eletronica/nota-fiscal-eletronica-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaNota">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesNota">CONHEÇA NOSSAS SOLUÇÕES PARA CONTRATOS DE SERVIÇOS</p>
						<hr>
					</aside>

					<div class="divSolucoesNota">
						<ul>
							<li><a href="#notaFiscal"><h1>nota fiscal eletrônica</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosNota segmentoNota" id="notaFiscal">
						<div class="introNota">
							<h1>nota fiscal eletrônica |</h1><h1 class="segNota">módulo</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						<hr class="hrNota">
						
						<div class="descricaoNota">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoNota">
						<ul>
							<div class="gestaoNota">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegNota" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegNota" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegNota" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegNota" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosNota conteinerBeneficioNota">
							<div class="beneficiosNota">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Segurança de informações: prevenção de fraudes</li>
									<li>Automatização dos processos: agilidade e praticidade</li>
									<li>Redução de riscos</li>
									<li>Redução de retrabalho</li>
									<li>Maior controle para o departamento financeiro e controladoria</li>
									<li>Automatização de processos burocráticos</li>
								</ul>
							</div>
							<div class="caracteristicasNota">
								<p>Características do Sistema</p>

								<ul class="caracteristicaNota">
									<li class="liNeutra">Controle operacional</li>
									<li>Gestão orçamentária: Mapas orçamentário, por unidade e corporativo</li>
									<li>Automatização de processos</li>
									<li>Redução de riscos como erro humano e fraudes</li>
									<li>Redução do tempo de processamento e resposta</li>
									<li>Conexão direta com os órgãos responsáveis</li>
									<li>Certificado Digital do tipo A1</li>
									<li>Calculo automatizado de impostos</li>
									<li>Fácil inclusão de descrições e anotações</li>
									<li>Fácil emissão e envio de boletos</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeNota">
					<div class="textoBackofficeNota">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesNota">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
