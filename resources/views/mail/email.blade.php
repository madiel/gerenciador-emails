<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>[SUBJECT]</title>
      <style type="text/css">
        body {
          padding-top: 0 !important;
          padding-bottom: 0 !important;
          padding-top: 0 !important;
          padding-bottom: 0 !important;
          margin:0 !important;
          width: 100% !important;
          -webkit-text-size-adjust: 100% !important;
          -ms-text-size-adjust: 100% !important;
          -webkit-font-smoothing: antialiased !important;
        }
        .tableContent img {
            border: 0 !important;
            display: block !important;
            outline: none !important;
        }
        a{
            color:#382F2E;
        }

        p, h1{
            color:#382F2E;
            margin:0;
        }
        p{
            border: 1px solid #888888 ;
            text-align:left;
            color:#999999;
            font-size:14px;
            font-weight:normal;
            line-height:19px;
            padding: 5px;
        }

        a.link1{
            color:#382F2E;
        }
        a.link2{
            font-size:16px;
            text-decoration:none;
            color:#ffffff;
        }

        h2{
            text-align:left;
            color:#195285;
            font-size:28px;
            font-weight:bold;
        }
        div,p,ul,h1{
            margin:0;
        }

        .bgBody{
            background: #ffffff;
        }
        .bgItem{
            background: #ffffff;
        }

        @media only screen and (max-width:480px)

        {

        table[class="MainContainer"], td[class="cell"]
        {
            width: 100% !important;
            height:auto !important;
        }
        td[class="specbundle"]
        {
          width:100% !important;
          float:left !important;
          font-size:13px !important;
          line-height:17px !important;
          display:block !important;
          padding-bottom:15px !important;
        }

        td[class="spechide"]
        {
          display:none !important;
        }
            img[class="banner"]
        {
                  width: 100% !important;
                  height: auto !important;
        }
          td[class="left_pad"]
        {
            padding-left:15px !important;
            padding-right:15px !important;
        }

        }

        @media only screen and (max-width:540px)

        {

        table[class="MainContainer"], td[class="cell"]
        {
          width: 100% !important;
          height:auto !important;
        }

        td[class="specbundle"]
        {
          width:100% !important;
          float:left !important;
          font-size:13px !important;
          line-height:17px !important;
          display:block !important;
          padding-bottom:15px !important;
        }

        td[class="spechide"]
        {
          display:none !important;
        }
            img[class="banner"]
        {
                  width: 100% !important;
                  height: auto !important;
        }
        .font {
          font-size:18px !important;
          line-height:22px !important;

        }
        .font1 {
        font-size:18px !important;
        line-height:22px !important;
        }

        }

      </style>
    </head>
    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align='left'>
            <div class="contentEditableContainer contentTextEditable">
              <div class="contentEditable" align='center'>
                <h2 >Trabalhe Conosco Site Enllevo</h2>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td align='left'>
            <div class="contentEditableContainer contentTextEditable">
              <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                  <p>
                  Nome :
                  </p>
                  <p style="background-color: #EEEEEE;">
                  {{ $curriculo->nome }}
                  </p>
                  <br>
                </div>

                <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                  Sobre Nome:
                  </p>
                  <p style="background-color: #EEEEEE;">
                  {{ $curriculo->sobrenome }}
                  </p>
                </div>

                  <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                  Celular/ Whatsapp:
                  </p>
                  <p style="background-color: #EEEEEE;">
                  {{ $curriculo->celular }}
                  </p>
                </div>

                <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                  Email:
                  </p>
                  <p style="background-color: #EEEEEE;">
                  {{ $curriculo->email }}
                  </p>
                </div>

                <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                  LinkedIn:
                  </p>
                  <p style="background-color: #EEEEEE;">
                  {{ $curriculo->linkedin }}
                  </p>
                </div>

                  <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                    Area de interesse:
                  </p>
                  <p style="background-color: #EEEEEE;">
                    {{ $curriculo->area }}
                  </p>
                </div>

                <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                    Qual seu cargo:
                  </p>
                  <p style="background-color: #EEEEEE;">
                    {{ $curriculo->cargo }}
                  </p>
                </div>

                  <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                    <p >
                  Mensagem:
                  </p>
                  <p style="background-color: #EEEEEE;  width: 48%;">
                    {{ $curriculo->mensagem }}
                  </p>
                </div>
            </div>
          </td>
        </tr>
      <table>
      <tr>
        <td align='center' bgcolor='#1A54BA' style='background:#195285; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'>
          <div class="contentEditableContainer contentTextEditable">
            <div class="contentEditable" align='center'>
              <a target='_blank' href='http://www.enllevo.com.br/public/curriculo/public/curriculo/{{ $curriculo->curriculo }}' class='link2' style='color:#ffffff;'>Visualizar Curriculo</a>
            </div>
          </div>
        </td>
      </tr>
   </body>
</html>


