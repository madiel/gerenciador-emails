<!DOCTYPE html>
<html>
<head>

    <!-- CSS Reset -->
    <link href="./css/reset.css" rel="stylesheet">
    <!-- CSS Reset -->

    <!-- CSS -->
    <link href="./css/style.css" rel="stylesheet">
    <!-- CSS -->

    <!-- Biblioteca jQuery -->
    <script src="./scripts/jquery-3.3.1.min.js"></script>
    <!-- Biblioteca jQuery -->

    <!-- Scripts JS -->
    <script src="./scripts/scripts.js"></script>
    <!-- Scripts JS -->

    <style>
        html, body {
            height: 100%;
            width: 94%;
            font-size: 100%;
            font-family: 'Roboto', sans-serif;
        }

        .button {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            padding: 1%;
            }
        * {box-sizing: border-box;}

        body {
            margin: 6%;
            font-family: Arial, Helvetica, sans-serif;
        }

        .topnav {
            overflow: hidden;

        }

        .topnav a {
            float: left;
            display: block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        .topnav a.active {
            background-color: #2196F3;
            color: white;
        }

        .topnav input[type=text] {
            float: right;
            padding: 4px;
            margin-top: 8px;
            margin-right: 16px;
            border: 2px solid 888888 !important;
            font-size: 18px;
        }

        @media screen and (max-width: 600px) {
            .topnav a, .topnav input[type=text] {
            float: none;
            display: block;
            text-align: left;
            width: 100%;
            padding: 14px;
            }

            .topnav input[type=text] {
            border: 1px solid #ccc;
            }
        }

        div.container {
            height: 100%;
        }

        div.titulo {
            font-size: 5vw;
            background-color: blue;
            text-align: center;
            padding: 3vw;
            position: relative;
        }

        div.op1 {
            width: 12%;
            display: inline-block;
            font-size: 1vw;
            text-align: right;
        }

        div.op2 {
            width: 79%;
            display: inline-block;
            font-size: 1vw;
            text-align: left
        }
        .mensagem{
            margin-bottom: 17%;
            position: absolute;
            left: 10%;
            top: 64%;
        }

        .btn-curriculo{
            position: absolute;
            left: 6%;
            top: 75%;
        }

        .logo-enllevo{
            width: 4.5em;
            height: 1em;
        }

        .btn-voltar{
           width: 0.4em;
           height: 0.4em;
        }

    </style>
</head>
<body>
    <div style="text-align: right; font-size: 40px;">
        <a href="{{ route('trabalhe-interno') }}"><img class="btn-voltar" src="images/voltar.png" alt="Voltar"></a>
    </div>

    <div style="text-align: center; font-size: 40px;">
        <a href="{{ route('painel') }}"><img class="logo-enllevo" src="images/logo-enllevo-home.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso"></a>
    </div>

    <div style="margin: 1%;">
        <a style="color: #195285; font-weight: bold;" >{{ date('d/m/Y H:i:s', strtotime($curriculo->created_at))  }}</a>
    </div>

    <table id="example" class="btn-curriculo">
    <div style="border-bottom: 2px solid #9ABC48; margin-bottom: 2%;"></div>
        <div class="container">
            <div class="col-md-12">
                <div class="op1">
                    <p style=" margin: 10px; color: #195285;">
                        Nome :
                    </p>
                    <p style=" margin: 10px; color: #195285;">
                        Sobre Nome:
                    </p>
                    <p style=" margin: 10px; color: #195285;">
                        Celular/ Whatsapp:
                    </p>
                    <p style=" margin: 10px; color: #195285;">
                        Email:
                    </p>
                    <p style=" margin: 10px; color: #195285;">
                        LinkedIn:
                    </p>
                    <p style=" margin: 10px; color: #195285;">
                        Area de interesse:
                    </p>
                    <p style=" margin: 10px; color: #195285;" >
                        Qual seu cargo:
                    </p>
                </div>

                <div class="op2">
                    <p style="margin: 10px;">
                        {{ $curriculo->nome }}
                    </p>
                    <p style="margin: 10px;">
                        {{ $curriculo->sobrenome }}
                    </p>
                    <p style="margin: 10px;">
                        {{ $curriculo->celular }}
                    </p>
                    <p style="margin: 10px;">
                        {{ $curriculo->email }}
                    </p>

                    <p style="margin: 10px;">
                        {{ $curriculo->linkedin }}
                    </p>
                    <p style="margin: 10px;">
                        {{ $curriculo->area }}
                    </p>
                    <p style="margin: 10px;">
                        {{ $curriculo->cargo }}
                    </p>
                </div>

                <div class="col-md-12"></div>
                <div class="op1">
                    <p class="mensagem" style="color: #195285;">
                        Mensagem:
                    </p>
                </div>

                <div class="op2">
                    <p style="margin: 10px;">
                        {{ $curriculo->mensagem }}
                    </p>

                    <tr>
                        <td align='center' bgcolor='#1A54BA' style='background:#195285; padding:15px 18px;-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'>
                        <div class="contentEditableContainer contentTextEditable">
                            <div class="contentEditable" align='center'>
                            <a style="text-decoration:none; color: aliceblue;" target='_blank' href='/public/curriculo/public/curriculo/{{ $curriculo->curriculo }}' class='link2' style='color:#ffffff;'>Visualizar Curriculo</a>
                            </div>
                        </div>
                        </td>
                    </tr>
                </div>
            </div>
        </div>
    </table>
</html>
