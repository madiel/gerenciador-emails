<!DOCTYPE html>
<html>
  <head>

    <!-- CSS Reset -->
    <link href="./css/reset.css" rel="stylesheet">
    <!-- CSS Reset -->

    <!-- CSS -->
    <link href="./css/style.css" rel="stylesheet">
    <!-- CSS -->

    <!-- Biblioteca jQuery -->
    <script src="./scripts/jquery-3.3.1.min.js"></script>
    <!-- Biblioteca jQuery -->

    <!-- Scripts JS -->
    <script src="./scripts/scripts.js"></script>
    <!-- Scripts JS -->

    <style>
    html, body {
        height: 100%;
        width: 94%;
        font-size: 100%;
        font-family: 'Roboto', sans-serif;
    }

    .button {
        border: none;
        color: white;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
        margin: 4px 2px;
        cursor: pointer;
        padding: 2%;
      }
    * {box-sizing: border-box;}

    body {
        margin-left: 6%;
      font-family: Arial, Helvetica, sans-serif;
    }

    .topnav {
      overflow: hidden;

    }

    .topnav a {
      float: left;
      display: block;
      color: black;
      text-align: center;
      padding-top: 14px;
      text-decoration: none;
      font-size: 17px;
    }

    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }

    .topnav a.active {
      background-color: #2196F3;
      color: white;
    }

    .topnav input[type=text] {
      float: right;
      padding: 7px;
      margin-bottom: 8px;
      border: 2px solid 888888 !important;
      font-size: 13px;  
    }

    @media screen and (max-width: 600px) {
      .topnav a, .topnav input[type=text] {
        float: none;
        display: block;
        text-align: left;
        width: 100%;
        margin: 0;
        padding: 14px;
      }

      .topnav input[type=text] {
        border: 1px solid #ccc;
      }
    }
    #example {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    .button2 {
        background-color: #008CBA;
        float: right;
    }

    #example td, #example th {

      padding: 2px;
    }

    #example tr:nth-child(even){background-color: #f2f2f2;}


    table#example {
        border-collapse: collapse;
    }
    #example tr {
        background-color: #eee;
        border-top: 1px solid #fff;
    }
    #example tr:hover {
        background-color: #9ABC48;
    }
    #example th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        color: #195285;
    }
    #example th, #example td {
        padding: 3px 3px;
    }
    #example td:hover {
        cursor: pointer;
    }

    .logo-enllevo{
        width: 4.5em;
        height: 1em;
        margin: 5%;
    }

    .page-item{
        color: #195285;
        float: left;
        padding: 6px 12px;
        text-decoration: none;
        transition: background-color .3s;
        border: 1px solid white;
        font-size: 13px;
        font-style: roboto;
        font-family: Roboto-Medium, sans-serif;
        background-color: lightgray;
    }


    .active{

      padding: 8px;

    }

    .pagination{
      display: inline-flex;
      background-color: #9ABC48;
      border: 1px solid #9ABC48;
      color: white;
      width: 110%;
      padding: 1%;

    } 

    .pagination a {
      color: black;
      float: left;
      padding: 8px;
      text-decoration: none;
    }

    .pagination a:hover:not(.active) {
      background-color: #ddd;
    }

    .disabled{
      padding-top: 8px;
      padding-left: 9px;
    }

    .btn-voltar{
      width: 0.4em;
      height: 0.4em;

    }

    
    .limpar{
      background-color: #9ABC48;
      float: right;
    }

    .font-contato {
      color: inherit;
      font-family: Roboto-Regular, sans-serif;
      font-size: 0.9em;
      font-weight: 500;
      text-decoration: none;
    }

    .font-contato-botao {
  
      color: white;
    
    }

    </style>
  </head>
<body>
    <div style="text-align: center; font-size: 40px;">
      <a href="{{ route('painel') }}"><img class="logo-enllevo" src="images/logo-enllevo-home.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso"></a>
    </div>
     
    <div style="text-align: right; font-size: 40px;">
      <a href="{{ route('painel') }}"><img class="btn-voltar" src="images/voltar.png" alt="Voltar"></a>
    </div>

    <div class="topnav">
      <tag style="color: #195285; font-weight: bold;" >TRABALHE CONOSCO - SITE ENLLEVO</tag>
        <form action="{{ route('pesquisarTrabalhe') }}" method="get">
            {{ csrf_field() }}
            <button class="button button2" style="float: right; margin: 0; padding: 0.8em;" type="submit">pesquisar</button>
            <button class="button limpar" style="float: right; margin: 0; padding: 0.8em;" type="submit">limpar</button>
           <input type="text" id="pesquisar" name="pesquisar" placeholder="Pesquisar..">
        </form>
    </div>

    <table id="example">
      <div style="border-bottom: 2px solid #9ABC48; margin-bottom: 2%;"></div>
      <tr>
        <td style="background-color: white;">
          <div style="    display: -webkit-box;">
            <h5 style="width: 20%;">Data</h5>
            <h5></h5>
          </div>
        </td>
        <td style="background-color: white;">
          <div style="    display: -webkit-box;">
            <h5 style="width: 20%;">Nome</h5> 
            <h5></h5>
          </div>
        </td>
        <td style="background-color: white;">
          <div style="    display: -webkit-box;">
            <h5>Sobre Nome</h5>
            <h5></h5>
          </div>
        </td>
      </tr>
      @foreach($trabalhe as $item)
        <tr>
          <td style="vertical-align: middle;     width: 28%;">
                <a class="font-contato" style="display: block;
                height: 100%;" href="{{ route('trabalhe-total', ['id' => $item->id]) }}">{{ date('d/m/Y H:i:s', strtotime($item->getAttribute('created_at')))  }}</a>
          </td>
          <td style="vertical-align: middle;     width: 28%;">
              <a class="font-contato" style="display: block;
              height: 100%;" href="{{ route('trabalhe-total', ['id' => $item->id]) }}">{{ $item->getAttribute('nome') }}</a>
          </td>
          <td style="vertical-align: middle;     width: 15%;">
              <a class="font-contato" style="display: block;
              height: 100%;" class="button button2 font-contato"  href=" {{ route('trabalhe-total', ['id' => $item->id]) }}">{{ $item->getAttribute('sobrenome') }}</a>
          </td>
          <td style="vertical-align: middle;">
              <a target='_blank' style="display: block;
              height: 100%;" class="button button2 font-contato font-contato-botao" href='/public/curriculo/public/curriculo/{{ $item->curriculo }}' >Curriculo</button></a>
          </td>
        </tr>
      @endforeach 
    </table>
  <div style="float: right; margin-top: 1%; margin-right: 10px;">

    {{ $trabalhe->appends(request()->input())->links() }}

  </div>

</html>

