<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>[SUBJECT]</title>
      <style type="text/css">
      body {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
        padding-top: 0 !important;
        padding-bottom: 0 !important;
        margin:0 !important;
        width: 100% !important;
        -webkit-text-size-adjust: 100% !important;
        -ms-text-size-adjust: 100% !important;
        -webkit-font-smoothing: antialiased !important;
      }
      .tableContent img {
        border: 0 !important;
        display: block !important;
        outline: none !important;
      }
      a{
        color:#382F2E;
      }

      p, h1{
        color:#382F2E;
        margin:1px;
      }
      p{
      border: 1px solid #888888 ;
        text-align:left;
        color:#999999;
        font-size:14px;
        font-weight:normal;
        line-height:19px;
        padding: 5px;
        margin:1px;
      }

      a.link1{
        color:#382F2E;
      }
      a.link2{
        font-size:16px;
        text-decoration:none;
        color:#ffffff;
      }

      h2{
        text-align:left;
        color:#195285;
        font-size:28px;
        font-weight:bold;
      }
      div,p,ul,h1{
        margin:1;
      }

      .bgBody{
        background: #ffffff;
      }
      .bgItem{
        background: #ffffff;
      }

      @media only screen and (max-width:480px)

      {

      table[class="MainContainer"], td[class="cell"]
      {
          width: 100% !important;
          height:auto !important;
      }
      td[class="specbundle"]
      {
          width:100% !important;
          float:left !important;
          font-size:13px !important;
          line-height:17px !important;
          display:block !important;
          padding-bottom:15px !important;
      }

      td[class="spechide"]
      {
        display:none !important;
      }
          img[class="banner"]
      {
                width: 100% !important;
                height: auto !important;
      }
        td[class="left_pad"]
      {
          padding-left:15px !important;
          padding-right:15px !important;
      }
      }

      @media only screen and (max-width:540px)

      {

      table[class="MainContainer"], td[class="cell"]
        {
          width: 100% !important;
          height:auto !important;
        }
      td[class="specbundle"]
        {
          width:100% !important;
          float:left !important;
          font-size:13px !important;
          line-height:17px !important;
          display:block !important;
          padding-bottom:15px !important;
        }

      td[class="spechide"]
        {
          display:none !important;
        }
            img[class="banner"]
        {
                  width: 100% !important;
                  height: auto !important;
        }
        .font {
          font-size:18px !important;
          line-height:22px !important;

          }
          .font1 {
          font-size:18px !important;
          line-height:22px !important;

          }
      }

    </style>

    <tr>
      <td align='left'>
          <div class="contentEditableContainer contentTextEditable">
          <div class="contentEditable" align='center'>
              <h2 >Contato Site Enllevo</h2>
          </div>
          </div>
      </td>
    </tr>

    <tr>
      <td align='left'>
        <div class="contentEditableContainer contentTextEditable">
        <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
            <p>
            Nome :
            </p>
            <p style="background-color: #EEEEEE;">
            {{ $contato->nome }}
            </p>
            <br>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
            Sobre Nome:
            </p>
            <p style="background-color: #EEEEEE;">
            {{ $contato->sobrenome }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
            Celular/ Whatsapp:
            </p>
            <p style="background-color: #EEEEEE;">
            {{ $contato->celular }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
            Telefone:
            </p>
            <p style="background-color: #EEEEEE;">
            {{ $contato->telefone }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
            Email:
            </p>
            <p style="background-color: #EEEEEE;">
            {{ $contato->email }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
            segmento:
            </p>
            <p style="background-color: #EEEEEE;">
            {{ $contato->segmento }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
            <p >
                Area atuação:
            </p>
            @if(!empty($contato->software))
            <p style="background-color: #EEEEEE;">
                {{ $contato->software }}
            </p>
            @endif
            @if(!empty($contato->consultoria))
            <p style="background-color: #EEEEEE;">
                    {{ $contato->consultoria }}
                    </p>
            @endif
            @if(!empty($contato->seguros))
                <p style="background-color: #EEEEEE;">
                {{ $contato->seguros }}
                </p>
            @endif

            @if(!empty($contato->treinamento))
                <p style="background-color: #EEEEEE;">
                {{ $contato->treinamento }}
                </p>
            @endif


            @if(!empty($contato->outros))
            <p style="background-color: #EEEEEE;">
                {{ $contato->outros }}
            </p>
            @endif

            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
                Qual seu cargo:
            </p>
            <p style="background-color: #EEEEEE;">
                {{ $contato->cargo }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
                Faturamento anual:
            </p>
            <p style="background-color: #EEEEEE;">
                {{ $contato->faturamento }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
                Quantidades funcionários:
            </p>
            <p style="background-color: #EEEEEE;">
                {{ $contato->funcionarios }}
            </p>
            </div>

            <div class="contentEditable" align='left' style="display:flex;justify-content:left;align-items:left; margin-bottom: 1%;" >
                <p >
            Mensagem:
            </p>
            <p style="background-color: #EEEEEE;  width: 48%;">
                {{ $contato->mensagem }}
            </p>
            </div>
        </div>
      </td>
    </tr>
  </body>
</html>


