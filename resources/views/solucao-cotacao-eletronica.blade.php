<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Cotação Eletrônica - Enllevo - Soluções que Geram Satisfação</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaBannerFornecimento">

					<div class="bannerFornecimento">

					</div>

					<section class="bannerSolucaoFornecimento">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-fornecimento-de-pecas') }}" class="destaqueAzul">cotação eletrônica</a>
							</span>
						</div>
							<span class="iconeCotacaoEletronica icone03"></span>
							<h1 class="tituloFornecimento">Solução Cotação Eletrônica</h1>
							<h2 class="textIntroFornecimento">Conte com a solução certa para todos os tipos de cotações eletrônicas.</h2>
							<h3 class="contTextIntroFornecimento">Conheça o sistema desenvolvido pela da Enllevo, gerencie através da cotações eletronicas.
							</h3>
					</section>
					</div>

					<div>
						<img class="imgFornecimento" src="images/solucoes/Cotacao-Eletronica/cotacao-eletronica-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--Solução Cotação Eletrônica-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaFornecimento">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesFornecimento">CONHEÇA NOSSAS SOLUÇÕES PARA CONTRATOS DE SERVIÇOS</p>
						<hr>
					</aside>

					<div class="divSolucoesFornecimento">
						<ul>
							<li><a href="#cotacao"><h1>cotação eletrônica</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosFornecimento segmentoFornecimento" id="cotacao">
						<div class="introFornecimento">
							<h1>cotação eletrônica |</h1><h1 class="segFornecimento">módulo</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						<hr class="hrFornecimento">
						
						<div class="descricaoFornecimento">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoFornecimento">
						<ul>
							<div class="gestaoFornecimento">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegFornecimento" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegFornecimento" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegFornecimento" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegFornecimento" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosFornecimento conteinerBeneficiosFornecimento1">
							<div class="beneficiosFornecimento">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Redução dos custos</li>
									<li>Supressão das intermediadoras</li>
									<li>Sem intermediadoras, maior possibilidade de negociação do desconto com as seguradoras</li>
									<li>Sistema é utilizado pelas seguradoras</li>
									<li>Garantia da procedência das peças</li>
									<li>Relacionamento direto com os parceiros (fornecedores)</li>
									<li>Rede de fornecedores criada pela seguradora</li>
									<li>Aumento do desconto das peças adquiridas em concessionários</li>
									<li>Agilidade nas soluções das adversidades</li>
									<li>Redução dos custos administrativos</li>
									<li>Menos funcionários para administrar o processo</li>
									<li>Flexibilização da operação com os fornecedores</li>
									<li>Redução do tempo de emissão de nota</li>
								</ul>
							</div>
							<div class="caracteristicasFornecimento">
								<p>Características do Sistema</p>

								<ul class="caracteristicasFornecimento1">
									<li class="liNeutra">Controle de acompanhamento do processo logístico</li>
									<li>Processo de escolha do vencedor da cotação realizado de forma automática</li>
									<li>Confere a nota recebida com a nota do sistema de forma automática</li>
									<li>Back office atua fora da curva</li>
									<li>Parametrização para escolha do vencedor da cotação</li>
									<li>Cálculo da melhor oferta (valor/prazo de entrega) realizado de forma automática</li>
									<li>Automatiza 95% do processo</li>
									<li>Sistema proativo</li>
									<li>Possibilidade de mudança de fluxo em função das necessidades de cada região</li>
									<li>Processo de entrega automatizado</li>
									<li>Pedido de peça automatizado</li>
									<li>Abertura de pedido por geolocalização</li>
									<li>Sistema processa o melhor cenário para a cotação</li>
									<li>Emissão de nota leva menos de uma hora</li>
								</ul>
							</div>
						</aside>
					</section>
			


			<!--Backoffice-->
				<aside class="backofficeFornecimento">
					<div class="textoBackofficeFornecimento">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesFornecimento">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
