<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Trabalhe Conosco - Enllevo - Soluções que Geram Satisfação</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS Slick Carrosel -->
		<link rel="stylesheet" type="text/css" href="./plugins/slick/slick.css">
		<link rel="stylesheet" type="text/css" href="./plugins/slick/slick-theme.css">
		<!-- CSS Slick Carrosel -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

	</head>

    <body>

		<div class="col-md-offset-2 col-md-8">

                @include('sweet::alert')

            </div>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- BannerInstitucional -->
			<div class="conteudoBanner">
				<div class="imagemBannerContato">
					<img class="imgTrabalheConosco" src="images/banner-topo-enllevo.jpg" title="Enllevo - Soluções que Geram Satisfação" alt="Trabalhe Conosco">
				</div>
				<div class="textoBannerTrabalheConosco">
					<h1 class="trabalheConoscoBanner">Trabalhe Conosco</h1>
				</div>
			</div>
			<!-- BannerInstitucional -->

			<!-- mensagemEnllevo -->
			<div class="backgroundTrabalheConosco">
			<span class="caminhoSite">
				<a href="index.php">home</a> /
				<a href="institucional.php" class="destaqueAzul">trabalhe conosco</a>
			</span>
			<section class="msgEnllevoTrabalheConosco">
				<h1 class="tituloTrabalheConosco">Acreditamos na inovação, na colaboração e no conhecimento</h1>
				<p class="paragrafoTrabalheConosco">
					Mais que processos e ferramentas, acreditamos nas pessoas e interações. Responder rapidamente às mudanças e inovar faz
					parte do nosso dia a dia. Saiba mais sobre as nossas iniciativas de tecnologia na página <a href="#" class="linkTranformacaoDigital">Tranformação digital.</a>
				</p>
			</section>
			<!-- mensagemEnllevo -->

			<!-- AreasAtuacaoEnllevo -->
			<section class="caixaAreaAtuacao">
				<h3 class="subTituloH3">Nossas áreas de atuação</h3>
				<div class="areaAtuacao">
					<h4 class="tituloAreaAtuacao">TI Tecnologia da Informação</h4>
					<img class="imgTecDaInformacao" src="images/mobile320/TI-Tecnologia-da-informacao.png" alt="Tecnologia da Informação" title="Enllevo - Tecnologia da Informação">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Voltado para área de Programação "BACK-END" em diversas liguagens como PHP, Cobol, Java, Asp etc...
					</p>
				</div>
				<div class="areaAtuacao">
					<h4 class="tituloAreaAtuacao">Front End</h4>
					<img class="imgFrontEnd" src="images/mobile320/Front-End.png" alt="Front End" title="Enllevo - Front End">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Voltado para área de Sintax e marcação "FRONT-END" em diversas liguagens como HTML, CSS, Java Script, JS etc...
					</p>
				</div>
				<div class="areaAtuacao invisivel">
					<h4 class="tituloAreaAtuacao">Design Digital UI / UX Designer</h4>
					<img class="imgUIUX" src="images/mobile320/UI-UX-Designer.png" alt="Design Digital UI / UX Designer" title="Enllevo - Design Digital UI / UX Designer">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Área de criação e envolvendo toda a parte digital como UI Designer e UX Designer, layouts, projetos, prototipagem,
						testes de usabilidades e apoio nos materiais de marketing.
					</p>
				</div>
				<div class="areaAtuacao invisivel">
					<h4 class="tituloAreaAtuacao">Marketing e Mídias</h4>
					<img class="imgMarketingMidias" src="images/mobile320/Marketing-midias.png" alt="Marketing e Mídias" title="Enllevo - Marketing e Mídias">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Gestão, Mídias e Marketing de todos os campos da empresa.
					</p>
				</div>
				<div class="areaAtuacao invisivel">
					<h4 class="tituloAreaAtuacao">Gestão Administrativa e Financeira</h4>
					<img class="imgGestaoAdmFinc" src="images/mobile320/Gestao.png" alt="Gestão Administrativa e Financeira" title="Enllevo - Gestão Administrativa e Financeira">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit arcu felis, ac auctor elit dictum id.
						Phasellus pellentesque placerat dolor, efficitur dictum ex cursus.
					</p>
				</div>
				<div class="areaAtuacao invisivel">
					<h4 class="tituloAreaAtuacao">Vendas e Relacionamento</h4>
					<img class="imgVendas" src="images/mobile320/Venda.png" alt="Vendas e Relacionamento" title="Enllevo - Vendas e Relacionamento">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Voltado para área de Programação "BACK-END" em diversas liguagens como PHP, Cobol, Java, Asp etc...
					</p>
				</div>
				<div class="areaAtuacao invisivel">
					<h4 class="tituloAreaAtuacao">RH e Contabilidade</h4>
					<img class="imgRHCont" src="images/mobile320/RH-contabilidade.png" alt="RH e Contabilidade" title="Enllevo - RH e Contabilidade">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit arcu felis, ac auctor elit dictum id.
						Phasellus pellentesque placerat dolor, efficitur dictum ex cursus.
					</p>
				</div>
				<div class="areaAtuacao invisivel">
					<h4 class="tituloAreaAtuacao">Atendimento e Relacionamento</h4>
					<img class="imgAtendimento" src="images/mobile320/Atendimento.png" alt="Atendimento e Relacionamento" title="Enllevo - Atendimento e Relacionamento">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit arcu felis, ac auctor elit dictum id.
						Phasellus pellentesque placerat dolor, efficitur dictum ex cursus.
					</p>
				</div>
				<span class="botaoRedondo exibirAreaAtuacao">mais...</span>
				<span class="botaoRedondo recolherAreaAtuacao invisivel">menos...</span>
			</section>
			<!-- AreasAtuacaoEnllevo -->

			<!--AreaAtuacaoEnllevoResolucao1024-->
			<section class="caixaAreaAtuacao1024">
				<h3 class="subTitulo1024">Nossas áreas de atuação</h3>

				<div class="areaAtuacao1024">
					<h4 class="tituloAreaAtuacao1024">TI Tecnologia da Informação</h4>
					<img class="imgTecDaInformacao1024" src="images/mobile320/TI-Tecnologia-da-informacao.png" alt="Tecnologia da Informação" title="Enllevo - Tecnologia da Informação">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Voltado para área de Programação "BACK-END" em diversas liguagens como PHP, Cobol, Java, Asp etc...
					</p>
				</div>
				<div class="areaAtuacao1024">
					<h4 class="tituloAreaAtuacao1024">Front End</h4>
					<img class="imgFrontEnd1024" src="images/mobile320/Front-end.png" alt="Front End" title="Enllevo - Front End">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Voltado para área de Sintax e marcação "FRONT-END" em diversas liguagens como HTML, CSS, Java Script, JS etc...
					</p>
				</div>
				<div class="areaAtuacao1024">
					<h4 class="tituloAreaAtuacao1024">Design Digital UI / UX Designer</h4>
					<img class="imgUIUX1024" src="images/mobile320/UI-UX-Designer.png" alt="Design Digital UI / UX Designer" title="Enllevo - Design Digital UI / UX Designer">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Área de criação e envolvendo toda a parte digital como UI Designer e UX Designer, layouts, projetos, prototipagem,
						testes de usabilidades e apoio nos materiais de marketing.
					</p>
				</div>
				<div class="areaAtuacao1024 areaMarketing">
					<h4 class="tituloAreaAtuacao1024">Marketing e Mídias</h4>
					<img class="imgMarketingMidias1024" src="images/mobile320/Marketing-midias.png" alt="Marketing e Mídias" title="Enllevo - Marketing e Mídias">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Gestão, Mídias e Marketing de todos os campos da empresa.
					</p>
				</div>
				<div class="areaAtuacao1024">
					<h4 class="tituloAreaAtuacao1024">Gestão Administrativa e Financeira</h4>
					<img class="imgGestaoAdmFinc1024" src="images/mobile320/Gestao.png" alt="Gestão Administrativa e Financeira" title="Enllevo - Gestão Administrativa e Financeira">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit arcu felis, ac auctor elit dictum id.
						Phasellus pellentesque placerat dolor, efficitur dictum ex cursus.
					</p>
				</div>
				<div class="areaAtuacao1024">
					<h4 class="tituloAreaAtuacao1024">Vendas e Relacionamento</h4>
					<img class="imgVendas1024" src="images/mobile320/Venda.png" alt="Vendas e Relacionamento" title="Enllevo - Vendas e Relacionamento">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Voltado para área de Programação "BACK-END" em diversas liguagens como PHP, Cobol, Java, Asp etc...
					</p>
				</div>
				<div class="areaAtuacao1024">
					<h4 class="tituloAreaAtuacao1024">RH e Contabilidade</h4>
					<img class="imgRHCont1024" src="images/mobile320/RH-contabilidade.png" alt="RH e Contabilidade" title="Enllevo - RH e Contabilidade">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit arcu felis, ac auctor elit dictum id.
						Phasellus pellentesque placerat dolor, efficitur dictum ex cursus.
					</p>
				</div>
				<div class="areaAtuacao1024 areaAtendimento">
					<h4 class="tituloAreaAtuacao1024">Atendimento e Relacionamento</h4>
					<img class="imgAtendimento1024" src="images/mobile320/Atendimento.png" alt="Atendimento e Relacionamento" title="Enllevo - Atendimento e Relacionamento">
					<!--span class="imgAreaAtuacao"></span-->
					<p class="paragrafoAreaAtuacao1024">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit arcu felis, ac auctor elit dictum id.
						Phasellus pellentesque placerat dolor, efficitur dictum ex cursus.
					</p>
				</div>
			</section>
			<!--AreaAtuacaoEnllevoResolucao1024-->

			<!--FormProcessoSeletivo-->
				<aside class="secaoFormProcesso">
					<p class="tituloFormProcessoSeletivo">Se você se vê trabalhando aqui, queremos ver o seu currículo.</p>
					<p class="conteudoTextFormProcesso">Cadastre aqui o seu currículo. Este é o principal canal para entrarmos em contato com profissionais do mercado.</p>
				</aside>
					<div class="formProcessoSeletivo" id="ancora">
						<form action="{{ url('/dados/trabalhe-conosco') }}" method="POST" class="formProcesso" enctype="multipart/form-data">
							{{ csrf_field() }}

							@if ($errors->has('nome'))
							<script>
								window.location.href='#ancora';
							</script>
							<input type="text" name="nome" style="border: 1px solid red;" class="nomeFormProcesso" placeholder="Campo nome obrigatório" size="">
							@else
							<input type="text" name="nome" class="nomeFormProcesso" placeholder="Nome" size="" value="{{ old('nome') }}">
							@endif

							@if ($errors->has('sobrenome'))
							<script>
								window.location.href='#ancora';
							</script>
							<input type="text" name="sobrenome" class="sobrenomeFormProcesso" style="border: 1px solid red;" placeholder="Campo  Sobrenome obrigatório" size="" >
							@else
							<input type="text" name="sobrenome" class="sobrenomeFormProcesso" placeholder=" Sobre Nome" size="" value="{{ old('sobrenome') }}">
							@endif

							@if ($errors->has('email'))
							<script>
								window.location.href='#ancora';
							</script>
							<input type="email" name="email" class="emailFormProcesso" style="border: 1px solid red;" placeholder="Campo E-mail obrigatório" size="">
							@else
							<input type="email" name="email" class="emailFormProcesso" placeholder="E-mail" size="" value="{{ old('email') }}">
							@endif

							@if ($errors->has('celular'))
							<script>
								window.location.href='#ancora';
							</script>
							<input type="tel" name="celular" class="celFormProcesso" style="border: 1px solid red;" placeholder="Campo  Celular obrigatório" size="" >
							@else
							<input type="tel" name="celular" class="celFormProcesso" placeholder="Celular/ Whatsapp" size="" value="{{ old('celular') }}">
							@endif

							@if ($errors->has('linkedin'))
							<script>
								window.location.href='#ancora';
							</script>
							<input type="text" name="linkedin" class="linkedinFormProcesso" style="border: 1px solid red;" placeholder="Campo LinkedIn obrigatório" size="">
							@else
							<input type="text" name="linkedin" class="linkedinFormProcesso" placeholder="LinkedIn" size="" value="{{ old('linkedin') }}">
							@endif

							@if ($errors->has('areaInteresse'))
							<script>
								window.location.href='#ancora';
							</script>
							<div class="divInputInteresseProcesso">
									<p class="paragrafoIntFormProcesso">Qual a area de interesse?</p>
									<input type="text" name="areaInteresse" class="intFormProcesso" style="border: 1px solid red;" placeholder="Campo  Escolha a area de interesse obrigatório" size="">
								</div>
							@else
							<div class="divInputInteresseProcesso">
									<p class="paragrafoIntFormProcesso">Qual a area de interesse?</p>
									<input type="text" name="areaInteresse" class="intFormProcesso" placeholder="Escolha a area de interesse..." size="" value="{{ old('areaInteresse') }}">
								</div>
							@endif
			
							@if ($errors->has('cargo'))
							<script>
								window.location.href='#ancora';
							</script>
							
							<div class="divInputCargoProcesso">
									<p class="paragrafoCargoFormProcesso">Qual seu cargo?</p>
									<input type="text" name="cargo" class="cargoFormProcesso" style="border: 1px solid red;" placeholder="Campo  Escolha seu cargo obrigatório" size="">
								</div>
							@else
							
							<div class="divInputCargoProcesso">
									<p class="paragrafoCargoFormProcesso">Qual seu cargo?</p>
									<input type="text" name="cargo" class="cargoFormProcesso" placeholder="Escolha seu cargo..." size="" value="{{ old('cargo') }}">
								</div>
							@endif

							@if ($errors->has('curriculo'))
							<script>
								window.location.href='#ancora';
							</script>
							
							<div class="caixaInputFile">
									<p class="uploadPdfForm">Envie seu curriculo em PDF</p>
									<label style="border: 1px solid red;" for="upload" class="labelInputFile">Campo Importar currículo é obrigatório</label>
									<input type="file" name="curriculo" id="upload" class="upload" placeholder="Campo Importar currículo é obrigatório" size="" value="{{ old('curriculo') }}">
								</div>
							@else
							
								<div class="caixaInputFile">
									<p class="uploadPdfForm">Envie seu curriculo em PDF</p>
									<label for="upload" class="labelInputFile">Importar curriculo...</label>
									<input type="file" name="curriculo" id="upload" class="upload" placeholder="Importar currículo..." size="" value="{{ old('curriculo') }}">
								</div>
							@endif	
							
							<span class="enviarMsgProcesso">
								<textarea  rows="100" name="mensagem" class="mensagemFormProcesso" placeholder="Escreva sua mensagem..." value="{{ old('mensagem') }}"></textarea>
								<input type="submit" name="submit" value="Enviar" class="submitFormProcesso">
							</span>
							
							<div class="divRecaptcha recap"> {!! Recaptcha::render() !!}  </div>
							
						</form>

						@if ($errors->has('g-recaptcha-response') && count($errors) == 1)
						<div class="center-robo alert-recap">
								<p>Você é um robo ?</p>
						</div>
						<script>
							window.location.href='#ancora';
						</script>
						@endif
					</div>
					
			<!--FormProcessoSeletivo-->
			<!--ProcessoSeletivo-->
				<aside class="processoSeletivo">
					<img src="images/processo.png" class="logo">
					<p class="tituloProcessoSeletivo">Conheça um pouco do processo seletivo</p>

					<div class="etapasProcessoSeletivo">
						<ul class="listaEtapasProcesso">
							<li>
								<span class="enumeracaoProcessoSeletivo">1</span>
								<p class="tituloSecaoProcessoSeletivo">cadastro</p>
								<div class="caixaIconeEtapaInfoTexto">
									<span class="iconeEtapa01"></span>
									<p class="paragrafoListaProcesso">Realize o cadastro através do nosso formulario acima e evie seu curriculo em pdf.</p>
								</div>
							</li>
							<!--div class="divBorderTracejada1"></div-->
							<li class="liContatoProcesso">
								<span class="enumeracaoProcessoSeletivo">2</span>
								<p class="tituloSecaoProcessoSeletivo">contato</p>
								<div class="caixaIconeEtapaInfoTexto">
									<span class="iconeEtapa01"></span>
									<p class="paragrafoListaProcesso">Se o seu perfil estiver de acordo com a oportunidade, nosso time entrará em contato.
									Caso contrário, seu cadastro segue em nossa base e será avaliado sempre que tiver uma nova oportunidade.</p>
								</div>
							</li>
							<!--div class="divBorderTracejada2Bottom"></div>
							<div class="divBorderTracejada2Right"></div-->
							<li class="liSelecaoProcesso">
								<span class="enumeracaoProcessoSeletivo">3</span>
								<p class="tituloSecaoProcessoSeletivo">seleção</p>
								<div class="caixaIconeEtapaInfoTexto">
									<span class="iconeEtapa03"></span>
									<p class="paragrafoListaProcesso paragrafoListaProcessoSelecao">Chegou a hora de nos conhecermos melhor.
									Algumas oportunidades contam com mais de uma fase, mas você terá todas as informações durante o processo.</p>
								</div>
							</li>
							<!--div class="divBorderTracejada3"></div-->
							<li class="liRetornoProcesso">
								<span class="enumeracaoProcessoSeletivo">4</span>
								<p class="tituloSecaoProcessoSeletivo">retorno</p>
								<div class="caixaIconeEtapaInfoTexto">
									<span class="iconeEtapa04"></span>
									<p class="paragrafoListaProcesso paragrafoListaProcessoRetorno">Fique tranquilo, todos os candidatos selecionados receberão um retorno do nosso time.</p>
								</div>
							</li>
							<!--div class="divBorderTracejada4Bottom"></div-->
							<!--div class="divBorderTracejada4Right"></div-->
							<li class="liAprovadoProcesso">
								<span class="enumeracaoProcessoSeletivo">5</span>
								<p class="tituloSecaoProcessoSeletivo">aprovado</p>
								<div class="caixaIconeEtapaInfoTexto">
									<span class="iconeFinalista"></span>
									<p class="paragrafoListaProcesso paragrafoListaProcessoAprovado">Seja muito bem-vindo. Agora você faz parte de uma excelente empresa para se trabalhar.</p>
								</div>
							</li>
						</ul>
					</div>
				</aside>
				</div>
			<!--ProcessoSeletivo-->

            <!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
		<!-- JS Slick -->
		<script src="./plugins/slick/slick.min.js" type="text/javascript"></script>
        <!-- JS Slick -->
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
    </body>
</html>
