<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Logística - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionabannerRotativoAuto">

					<div class="bannerRotativoAuto">

					</div>

					<section class="bannerSolucaoAutomotivo">
					<div class="divCaminhoSolucoes">
						<span class="caminhoSolucoes">
							<a href="{{ route('index') }}">home</a> /
							<a href="{{ route('index') }}">soluções</a> /
							<a href="{{ route('solucao-automotivo') }}" class="destaqueAzul">automotivo</a>
						</span>
					</div>
						<span class="iconeSegAutomotivo icone02"></span>
						<h1 class="tituloSolAuto">Solução Automotiva</h1>
						<h2 class="textIntroSolAuto">Solução certa que atende às particularidades do segmento, desde o controle de mapas de produção
						até indicadores automatizados de qualidade.</h2>
						<h3 class="contTextIntroSolAuto">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
						envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>

					</section>
					</div>

					<div>
						<img class="imgSolAuto" src="images/solucoes/Automotivo/automotivo-enllevo.png" alt="Solução Automotivo" title="Enllevo Solução Automotivo">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoAutomotivo-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaAuto">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesParaSolAuto">CONHEÇA NOSSAS SOLUÇÕES PARA AUTOMOTIVA</p>
						<hr>
					</aside>

					<div class="divSolucoesAutomotivo">
						<ul>
							<li><a href="#veiculoComercial"><h1>VEÍCULOS COMERCIAIS</h1></a></li>
							<li><a href="#tratores"><h1>TRATORES</h1></a></li>
							<li><a href="#veiculoPasseio"><h1>VEÍCULO DE PASSEIO</h1></a></li>
							<li><a href="#implementoRodoviario"><h1>IMPLEMENTO RODOVIÁRIO</h1></a></li>
							<li><a href="#gestaoRecapagem"><h1>GESTÃO DE RECAPAGEM</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosSolAuto segmentoComercial" id="veiculoComercial">
						<div class="introComercial">
							<h1>VEÍCULOS COMERCIAIS |</h1><h1 class="segComercial">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Automotiva</strong></h2>
						</div>
						<hr class="hrSolAuto">
						
						<div class="descricaoSolAuto">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoSolAuto">

						<div class="gestaoSolAuto">
							<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
						</div>

							<ul>
								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-09.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-10.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-11.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-comercial-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>


								<!--li>
									<img class="imgSegAuto" src="images/solucoes/Logistica/wms-13.png" alt="Modos Operacionais" title="Modos Operacionais">
									<p>Modos operacionais</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Logistica/wms-14.png" alt="Consolidação do armazém" title="Consolidação do armazém">
									<p>Consolidação do armazém</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Logistica/wms-15.png" alt="Relatórios" title="Relatórios">
									<p>Relatórios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Logistica/wms-16.png" alt="Customização Total" title="Customização Total">
									<p>Customização total</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Logistica/wms-17.png" alt="Gestão de empilhadeiras" title="Gestão de empilhadeiras">
									<p class="gestaoEmpilhadeira">Gestão de empilhadeiras</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Logistica/wms-18.png" alt="Gestão de produtos perigosos (ADR)" title="Gestão de produtos perigosos (ADR)">
									<p>Gestão de produtos perigosos (ADR)</p>
								</li-->
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolAuto caracteristicasBeneficiosSistemaComercial">
							<div class="beneficiosSistemaSolAuto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas Automotivas</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>
							<div class="caracteristicasSistemasSolAuto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasComercial">
									<li class="liCaracteristicasSistemas">Veículos Comerciais</li>
									<li>Veículos Oficina</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
					<section class="segmentosSolAuto segmentoTratores" id="tratores">
						<div class="introTratores">
							<h1 class="h1IntroTratores">TRATORES |</h1><h1 class="segTratores">MÒDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Automotiva</strong></h2>
						</div>
						<hr class="hrSolAuto hrSolAutoTratores">
						
						<div class="descricaoSolAuto descricaoSolAutoOposto">
							<p>A Solução da Enllevo para Tratores é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

						</div>

						<div class="secaoSolAuto secaoSolAutoTratores">

						<div class="gestaoSolAuto gestaoSolAutoOposto">
							<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
						</div>

							<ul>
								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-01.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-02.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-03.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-04.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-05.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-06.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-07.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-08.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-09.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-10.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-11.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-12.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/tratores-13.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Controle de Serviços em Campo</p>
								</li>


							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolAuto caracteristicasBeneficiosSistemaAuto">
							<div class="beneficiosSistemaSolAuto beneficiosSistemaTratores">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Tratores</li>
									<li>Adaptação do software às necessidades da empresa</li>
									<li>Vendas de Implementos</li>
									<li>Controle de Serviços em Campo</li>
								</ul>
							</div>
							<div class="caracteristicasSistemasSolAuto caracteristicasSistemasAuto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAuto">
									<li class="liCaracteristicasSistemas">Tratores</li>
									<li>Veículos Oficina</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
					<section class="segmentosSolAuto segmentoPasseio" id="veiculoPasseio">
						<div class="introPasseio">
							<h1>VEÍCULOS DE PASSEIO |</h1><h1 class="segPasseio">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Automotiva</strong></h2>
						</div>
						<hr class="hrSolAuto">
						
						<div class="descricaoSolAuto">
							<p>A Solução da Enllevo para Veículos de Passeio é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>
						</div>


						<div class="secaoSolAuto">

						<div class="gestaoSolAuto">
							<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
						</div>

							<ul>
								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-09.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-10.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-11.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>

							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaAuto sistemaPasseio">
							<div class="beneficiosSistemaSolAuto beneficiosPasseio">
								<p>Benefícios do Sistema</p>

								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas Automotivas</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>


							<div class="caracteristicasSistemasSolAuto caracPasseio">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAutoPasseio">
									<li class="liCaracteristicasSistemas">Veículos de Passeio</li>
									<li>Veículos Oficina</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>

					<section class="segmentosSolAuto segmentoRodoviario" id="implementoRodoviario">
						<div class="introRodoviario">
							<h1>IMPLEMENTOS RODOVIÁRIOS |</h1><h1 class="segRodoviario">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Automotiva</strong></h2>
						</div>
						<hr class="hrSolAuto hrSolAutoRodoviario">

						<div class="descricaoSolAuto descricaoSolAutoOposto">
							<p>A Solução da Enllevo para Implementos Rodoviários é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>
						</div>

						<div class="secaoSolAuto secaoSolAutoRodoviario">

						<div class="gestaoSolAuto gestaoSolAutoOposto">
							<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
						</div>

							<ul>
								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-09.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-10.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-11.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAuto" src="images/solucoes/Automotivo/iplementos-rodoviario-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolAuto sistemaSolAuto">
							<div class="beneficiosSistemaSolAuto beneficiosRodoviario">
								<p>Benefícios do Sistema</p>

								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas Automotivas</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>


							<div class="caracteristicasSistemasSolAuto caracRodoviario">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAutoRodoviario">
									<li class="liCaracteristicasSistemas">Veículos Oficina</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>

					<section class="segmentosSolAuto segmentoRecapagem" id="gestaoRecapagem">
						<div class="introRecapagem">
							<h1>GESTÃO DE RECAPAGEM |</h1><h1 class="segRecapagem">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Automotiva</strong></h2>
						</div>
						<hr class="hrSolAuto">
						
						<div class="descricaoSolAuto">
							<p>A Solução da Enllevo para Gestão de Recapagem é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>
						</div>

						<div class="secaoSolAuto">

						<div class="gestaoSolAuto">
							<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
						</div>

							<ul>
								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Automotivo/g-recapagem-13.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Controle da Autoclavi "Rastreabilidade</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolAuto SolucaoAuto">
							<div class="beneficiosSistemaSolAuto beneficiosRecapagem">
								<p>Benefícios do Sistema</p>

								<ul>
									<li class="liBeneficiosSistema">Integração total com os fabricantes</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Controle da Autoclavi "Rastreabilidade"</li>
								</ul>
							</div>


							<div class="caracteristicasSistemasSolAuto caracRecapagem">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAutoRecapagem">
									<li class="liCaracteristicasSistemas">Processos produtivos em todas as fases "Pneu"</li>
									<li>Estoque de materiais</li>
									<li>Baixa automatica do estoque</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Relatorio em todos os processos</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Elaboração de propostas comerciais</li>
									<li>Controle de comissões</li>
									<li>Processos de compra concessionária x fabricantes</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--SoluçãoAutomotivo-->


			<!--Backoffice-->
				<aside class="backofficeAuto">
					<div class="textoBackofficeAuto">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesAuto">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
