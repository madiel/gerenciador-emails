<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Logística - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionabannerRotativo">

					<div class="bannerRotativo">

					</div>

					<section class="bannerSolucaoLogistica">
					<div class="divCaminhoSolucoes">
						<span class="caminhoSolucoes">
                            <a href="{{ route('index') }}">home</a> /
							<a href="{{ route('index') }}">soluções</a> /
							<a href="{{ route('solucao-logistica') }}" class="destaqueAzul">logistica</a>
						</span>
					</div>
						<span class="iconeSegLogistica icone01"></span>
						<h1 class="tituloLogistica">Soluçao de Logística</h1>
						<h2 class="textIntroLogistica">Conte com soluções de ponta da Enllevo e garanta mais eficiência e tecnologia no processo logístico para entregar resultados.</h2>
						<h3 class="contTextIntroLogistica">Conheça o sistema que oferece controle total sobre cada etapa da operação logística, da entrada
						de matérias-primas até a expedição dos produtos.</h3>

					</section>
						<div>
							<img class="imgLogistica" src="images/solucoes/Logistica/logistica-enllevo.png" alt="Solução Logistica" title="Enllevo Solução Logistica">
						</div>
				</div>
			<!--Cabeçalho-->

			<!--SoluçãoLogistica-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonProposta">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						<a href="{{ route('contato') }}">
						<p class="solucoesParaLogistica">CONHEÇA NOSSAS SOLUÇÕES PARA LOGÍSTICA</p>
						<hr>
					</aside>

					<div class="divSolucoesLogistica">
						<ul>
							<li><a href="#WMS"><h1>WMS</h1></a></li>
							<li><a href="#TMS"><h1>TMS</h1></a></li>
							<li><a href="#YMS"><h1>YMS</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentos segmentoWMS" id="WMS">
						<div class="introWMS">
							<h1>GESTÃO DE LOGÍSTICA |</h1><h1 class="segWMS">WMS</h1>

							<h2>Enllevo WMS - <strong>Warehouse Management System</strong></h2>
						</div>
						<hr class="hrLogistica">
						
						<div class="descricaoSegLogistica">
							<p>A Solução da Enllevo para Logística WMS (Sistema de Gestão de Armazéns)
							é um software moderno e completo para gerenciar um armazém ou um grupo de armazéns.</p>

							<p>Ao longo dos anos dedicados em seu desenvolvimento
							se traduziram em um sistema de extrema amplitude e versatilidade,
							o qual adapta-se aos mais diversos segmentos e suas exigências, otimizando todos os processos logísticos de armazenagem.</p>
						</div>
						<div class="secaoSegLog">
							<ul>
								<div class="gestaoSegLogistica">
									<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
								</div>
								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Entregas ao armazém</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Gestão de locais</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-03.png" alt="Pickings" title="Pickings">
									<p>Pickings</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Expedição do armazém</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-05.png" alt="Inventários" title="Inventários">
									<p>Inventários</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>RF, RFID e Voz</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Identificação automática</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Status de qualidade e datas de validade</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-09.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Embalagens retornáveis</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-10.png" alt="GS1" title="GS1">
									<p>GS1</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-11.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Integração com equipamentos externos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Gestão de custos</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-13.png" alt="Modos Operacionais" title="Modos Operacionais">
									<p>Modos operacionais</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-14.png" alt="Consolidação do armazém" title="Consolidação do armazém">
									<p>Consolidação do armazém</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-15.png" alt="Relatórios" title="Relatórios">
									<p>Relatórios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-16.png" alt="Customização Total" title="Customização Total">
									<p>Customização total</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-17.png" alt="Gestão de empilhadeiras" title="Gestão de empilhadeiras">
									<p class="gestaoEmpilhadeira">Gestão de empilhadeiras</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/wms-18.png" alt="Gestão de produtos perigosos (ADR)" title="Gestão de produtos perigosos (ADR)">
									<p>Gestão de produtos perigosos (ADR)</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistema caracteristicasBeneficiosSistemaWMS">
							<div class="beneficiosSistema">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Redução de custos logísticos</li>
									<li>Aumento da capacidade operacional</li>
									<li>Organização de processos e recursos</li>
									<li>Otimização de processos de distribuição</li>
									<li>Supervisão de elos na cadeia logística</li>
									<li>Rastreamento de lotes, paletes e datas</li>
									<li>Monitoramento de trabalho</li>
									<li>Técnicas modernas de TI e logística</li>
									<li>Melhoria na qualidade do fornecimento</li>
									<li>Melhor atendimento ao cliente</li>
									<li>Operacional em qualquer tipo de armazéns</li>
									<li>Integração com sistemas externos</li>
									<li>Indicadores gráficos</li>
								</ul>
							</div>
							<div class="caracteristicasSistemas">
								<p>Características do Sistema</p>

								<ul class="caracteristicasWMS">
									<li class="liCaracteristicasSistemas">Divisão hierárquica da estrutura de armazéns</li>
									<li>Divisão espacial de locais de armazém</li>
									<li>Definição de parâmetros logísticos de produtos</li>
									<li>Ordens para fornecedores e recebimentos no armazém</li>
									<li>Controle qualitativo e quantitativo da mercadoria em fornecimento</li>
									<li>Identificação automática (AI)</li>
									<li>Atribuição automática ou manual de locais do armazém</li>
									<li>Reposição de armazém</li>
									<li>Pedidos de clientes e envios</li>
									<li>Relatórios contábeis de serviços logísticos</li>
									<li>Crossdocking</li>
									<li>Gestão de tarefas</li>
									<li>Inventários</li>
									<li>Gestão de embalagens reutilizáveis</li>
									<li>Gestão de lotes e datas de validade</li>
									<li>Rastreamento de lotes e veículos de armazenagem</li>
									<li>Gestão de produtos perigosos</li>
									<li>Comunicação de dados sem fio (RF)</li>
								</ul>
							</div>
						</aside>
					</section>
					<section class="segmentos TMS" id="TMS">
						<div class="introTMS">
							<h1 class="h1IntroTMS">GESTÃO DE LOGÍSTICA |</h1><h1 class="segTMS">TMS</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrLogistica hrLogisticaTMS">

						<div class="descricaoSegLogistica descricaoSegLogisticaTMS">
							<p>A Solução da Enllevo TMS (Sistema de Gestão de Transportes) é um software responsável pelo planejamento,
							monitoramento e contabilidade do transporte, em qualquer tipo de estrutura de distribuição.</p>

							<p>Aperfeiçoamento do planejamento de rotas, combinação de transportes,
							transbordos e muitas outras funções fazem deste módulo uma ferramenta perfeita para todas
							as empresas com operações de transporte complexas.</p>

						</div>
						<div class="secaoSegLog secaoSegLogTMS">
							<ul>

							<div class="gestaoSegLogistica gestaoSegLogisticaTMS">
								<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
							</div>
								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-01.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-02.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-03.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-04.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-05.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-06.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-07.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-08.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-09.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-10.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-11.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-12.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-13.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/tms-14.png" alt="Enllevo - Gestão de custos de transportadoras" title="Gestão de custos de transportadoras">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistema caracteristicasBeneficiosSistemaTMS">
							<div class="beneficiosSistema beneficiosSistemaTMS">
								<p>Benefícios do Sistema</p>
								<ul>
									<li>Redução de custos logísticos</li>
									<li>Aumento da capacidade operacional</li>
									<li>Otimização de processos de transporte</li>
									<li>Controle preciso de custos</li>
									<li>Suporte ADR</li>
									<li>Rastreamento de lotes e unidades de carga</li>
									<li>Monitoramento de eventos relacionados ao transporte</li>
									<li>Técnicas modernas em TI e logística</li>
									<li>Indicadores gráficos</li>
									<li>Melhor atendimento ao cliente</li>
									<li>Operacional em qualquer tipo de empresas</li>
								</ul>
							</div>
							<div class="caracteristicasSistemas caracteristicasSistemasTMS">
								<p>Características do Sistema</p>

								<ul class="caracteristicasTMS">
									<li>Gestão de ordens de expedição</li>
									<li>Criação e planejamento de rotas de transporte</li>
									<li>Operação de incidentes de expedição atípicos</li>
									<li>Colaboração com vários tipos de equipamentos móveis</li>
									<li>Gestão de custos de serviços de transporte</li>
									<li>Lidar com contratos relacionados às tarefas de transporte</li>
									<li>Tarifas de serviçõs de transporte definidas pelo usuário</li>
									<li>Algoritmos de apuração definidos pelo usuário</li>
									<li>Lista de preços de transporte definidas pelo usuário</li>
									<li>Estatísticas e análises</li>
									<li>Integração com mapas</li>
								</ul>
							</div>
						</aside>
					</section>
					<section class="segmentos segmentoYMS" id="YMS">
						<div class="introYMS">
							<h1>GESTÃO DE LOGÍSTICA |</h1><h1 class="segYMS">YMS</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrLogistica">
						

						<div class="descricaoSegLogistica">
							<p>A Solução da Enllevo YMS (Sistema de Gerenciamento de Pátios) é um software especializado que permite controlar
							totalmente o tráfego de veículos e pedestres nos pátios e instalações da fábrica.</p>

							<p>Além disso, melhora o planejamento das operações de carga e descarga, otimizando o trabalho
							e minimizando o tempo de inatividade.</p>
						</div>
						<div class="secaoSegLog">
							<ul>

							<div class="gestaoSegLogistica">
								<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
							</div>
								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegLogistica" src="images/solucoes/Logistica/yms-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistema sistemaYMS">
							<div class="beneficiosSistema beneficiosYMS">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Otimização na utilização dos veículos disponíveis</li>
									<li>Facilidade de planejamento do tráfego de todos veículos dentros da empresa</li>
									<li>Minimização do acúmulo de tarefas no horário de recebimentos e despachos</li>
									<li>Minimização de eventuais furtos</li>
									<li>Possibilidade de comunicação direta com os motoristas</li>
									<li>Coleta de informações valiosas para facilitar o planejamento e tomada de decisões</li>
									<li>Minimização de custos de operação nos estacionamentos e em áreas de manobras</li>
									<li>Melhor atendimento aos fornecedores e clientes.</li>
								</ul>
							</div>
							<div class="caracteristicasSistemas caracYMS">
								<p>Características do Sistema</p>

								<ul class="caracteristicasYMS">
									<li>Gestão de ordens de serviço</li>
									<li>Gestão do tráfego de pedestres</li>
									<li>Alertas</li>
									<li>Análise de processos</li>
									<li>Atribuição automática de localizações</li>
									<li>Livro de registro de entradas e saídas</li>
									<li>Gestão de vagas de estacionamento</li>
									<li>Controle de pontualidade</li>
									<li>Sistema de controle</li>
									<li>Sistema de notificações automáticas</li>
									<li>Biblioteca de modelos de visitas</li>
									<li>Espera de serviço (Buffering)</li>
									<li>Identificação de recursos</li>
									<li>Definição de regras de serviço</li>
									<li>Alerta aos visitantes</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--SoluçãoLogistica-->


			<!--Backoffice-->
				<aside class="backoffice">
					<div class="textoBackoffice">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>
					<div class="backofficeOpcoes">
						<ul>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
							<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
						</ul>
					</div>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
