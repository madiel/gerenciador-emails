<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Erro 404 - Enllevo - Soluções que Geram Satisfação</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!--Conteudo-->
			<div class="imgTelaErro">
				<img src="images/mobile320/erro-easter egg-01.png" alt="Erro" title="Enllevo - Soluções que Geram Satisfação">
				<p>Algo deu errado ou esta tela não existe...</p>
			</div>
			<!--Conteudo-->

			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
    </body>
</html>
