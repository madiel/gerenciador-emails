<header class="cabecalho">
    <h1 class="logoEnllevo">
		Enllevo - Soluções que Geram Satisfação - Desenvolvimento de Software - ERP - WMS - YMS - TMS - Logistica - Automotivo - Distribuição - Nota Fiscal - Gestão de RH - Consultoria Empresarial - Enllevo, "Sensação ou Estado de Espírito" - Enllevo by Shapeness
		<a href="{{ route('index') }}" /><img class="logoCabecalho" src="images/mobile320/logo-enllevo.png" title="Marca Enllevo" alt="Marca Enllevo">
	</h1>
	<ul class="listaRedeSociais">
		<li class="Facebook">
			<a href="#" title="Facebook -Enllevo">
				<span class="iconeFacebook"></span>
			</a>
		</li>
		<li class="Instagram">
			<a href="#" title="Instagram - Enllevo">
				<span class="iconeInstagran"></span>
			</a>
		</li>
		<li class="Linkedin">
			<a href="#" title="Linkedin - Enllevo">
				<span class="iconeLinkedin"></span>
			</a>
		</li>
	</ul>
	<div class="menuNavegacao backgroundAzul">
		<div class="menuMobile">
			<span class="menuLinha"></span>
			<span class="menuLinha"></span>
			<span class="menuLinha"></span>
		</div>
		<div class="menuAberto invisivel">
			<div class="menuInicial">
				<nav class="menuBackground">
					<div class="cabecalhoMenuAberto">
						<img class="logoEnllevoBranco" src="images/mobile320/logo-enllevo-002.png" title="Marca Enllevo" alt="Marca Enllevo">
						<span class="fecharMenu iconeFecharMenuSite backgroundAzul"></span>
					</div>
					<ul class="listaMenuNavegacao">
						<li>
							<a href="{{ route('index') }}">Início</a>
						</li>
						<li>
							<a href="{{ route('institucional') }}">Institucional</a>
						</li>
						<li class="solucoes">
							Soluções<span class="iconeSetaMenuSite"></span>
						</li>
						<li class="servicos">
							Serviços<span class="iconeSetaMenuSite"></span>
						</li>
						<li>
							<a href="{{ route('contato') }}">Contato</a>
						</li>
					</ul>
				</nav>
				<div class="pesquisaAlteraIdioma">
					<!-- <div class="pesquisaMenu">
						 <input class="inputPesquisa" type="text" name="pesquisa">
						<span class="iconeLupaSite"></span>
					</div> -->
					<div class="alteraIdioma">
						<img class="idiomaSelecionado" src="images/mobile320/brasil.png" title="idioma brasileiro" alt="idioma brasileiro">
						<ul class="listaIdiomas invisivel">
							<li>
								<img src="images/mobile320/brasil.png" title="idioma brasileiro" alt="idioma brasileiro">
							</li>
							<li>
								<img src="images/mobile320/brasil.png" title="idioma brasileiro2" alt="idioma brasileiro2">
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="menuSolucoes invisivel">
				<nav class="menuBackground">
					<div class="cabecalhoMenuAberto">
						<img class="logoEnllevoBranco" src="images/mobile320/logo-enllevo-002.png" title="Marca Enllevo" alt="Marca Enllevo">
						<span class="fecharMenuSolucoes iconeFecharMenuSite"></span>
					</div>
					<h3 class="subTituloMenu">Soluções</h3>
					<ul class="listaMenuSolucoes">
						<li>
							<a href="{{ route('solucao-automotivo') }}" title="Enllevo Automotivo">
								<span class="iconeSegAutomotivo">
									<span class="labelSegmento">Automotivo</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-motores-e-maquinas') }}" title="Enllevo Motores e Máquinas">
								<span class="iconeSegMotoresMaquinas">
									<span class="labelSegmento">Motores e Máquinas</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-fornecimento-de-pecas') }}" title="Enllevo Fornecimento de Peças">
								<span class="iconeCotacaoEletronica">
									<span class="labelSegmento">Cotação Eletrônica</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-logistica') }}" title="Enllevo Logística">
								<span class="iconeSegLogistica">
									<span class="labelSegmento">Logística</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-manufatura') }}" title="Enllevo Manufatura">
								<span class="iconeSegManufatura">
									<span class="labelSegmento">Manufatura</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-distribuicao') }}" title="Enllevo Distribuição">
								<span class="iconeSegDistribuicao">
									<span class="labelSegmento">Distribuição</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-gestao-de-estacionamento') }}" title="Enllevo Gestão de Estacionamento">
								<span class="iconeSegEstacionamento">
									<span class="labelSegmento">Gestão de Estacionamento</span>
								</span>
							</a>
						</li>
						<li>
							<a href="erro.php" title="Enllevo Nota Fiscal Eletrônica">
								<span class="iconeSegNotaFiscal">
									<span class="labelSegmento">Nota Fiscal Eletrônica</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-controle-de-obras') }}" title="Enllevo Controles de Obras">
								<span class="iconeSegControleObras">
									<span class="labelSegmento">Controles de Custos de Obras</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-contratos-de-servicos') }}" title="Enllevo Contratos de Serviços">
								<span class="iconeSegContratos">
									<span class="labelSegmento">Contratos de Serviços</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-associacoes') }}" title="Enllevo Associações">
								<span class="iconeSegAssociacoes">
									<span class="labelSegmento">Associações</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-controle-de-cobranca') }}" title="Enllevo Controle de Cobrança Digital">
								<span class="iconeSegCobrancaDigital">
									<span class="labelSegmento">Ações de Cobranças Digitais</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-gestao-rh') }}" title="Enllevo Gestão de RH">
								<span class="iconeSegGestaoRh">
									<span class="labelSegmento">Gestão de RH</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-contabil-fiscal') }}" title="Enllevo Contábil e Fiscal">
								<span class="iconeSegContabilFiscal">
									<span class="labelSegmento">Contábil e Fiscal</span>
								</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="menuServicos invisivel">
				<nav class="menuBackground">
					<div class="cabecalhoMenuAberto">
						<img class="logoEnllevoBranco" src="images/mobile320/logo-enllevo-002.png" title="Marca Enllevo" alt="Marca Enllevo">
						<span class="fecharMenuServicos iconeFecharMenuSite"></span>
					</div>
					<h3 class="subTituloMenu">Serviços</h3>
					<ul class="listaMenuServicos">
						<li>
							<a href="{{ route('desevolvimento-de-software') }}" title="Enllevo Desenvolvimento de Software">
								<span class="iconeSevDesenvolvimentoSoftware">
									<span class="labelSegmento">Desenvolvimento de Software</span>
								</span>
							</a>
						</li>
						<li>
							<a href="{{ route('consultoria-empresarial') }}" title="Enllevo Consultoria Empresarial">
								<span class="iconeSevConsultoria">
									<span class="labelSegmento">Consultoria Empresarial</span>
								</span>
							</a>
						</li>
						<!--li>
							<a href="erro.php" title="Enllevo Corretora de Seguros">
								<span class="iconeSevSeguros">
									<span class="labelSegmento">Corretora de Seguros</span>
								</span>
							</a>
						</li-->
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>

<!--Header Resolução 1024px-->
<header class="headerDesk" id="topo">
	<aside class="header1024">
		<div class="vendas1024">
			<span class="iconeCarrinhoVendas"></span>
			<span class="vendasHeader">Vendas</span>
			<span class="telVendasHeader">11 2714 - 2175</span>
		</div> |
		<div class="suporte1024">
			<a href="{{ url('https://shapeness.freshdesk.com/support/devolta-para-o-futuro') }}" class="conjuntoSuporte suporteHeader">
				<span class="iconeSuporteSite"></span>
				Suporte
			</a>
			<!--span class="telSuporteHeader">4003-0258</span-->
		</div>

		<div class="pesquisaIdioma1024">
			<!--div class="barraPesquisaHeader">
			(removido temporariamente)
				 <input class="inputBarraPesquisa" type="text" name="pesquisa">
				<span class="iconeLupaSite iconeAbrirInput"></span>
				<span class="iconeLupaSite iconePesquisar"></span>
			</div-->
			<span class="spanIdioma">Idioma</span>
			<div class="idiomaHeader">
				<img class="selecionaIdiomaHeader" src="images/mobile320/brasil.png" title="idioma brasileiro" alt="idioma brasileiro">
				<ul class="listaIdiomas invisivel">
					<li>
						<img src="images/mobile320/brasil.png" title="idioma brasileiro" alt="idioma brasileiro">
					</li>
					<li>
						<img src="images/mobile320/brasil.png" title="idioma brasileiro2" alt="idioma brasileiro2">
					</li>
				</ul>
			</div>
		</div>
	</aside>

	<div class="navHeader">
		<h1 class="logoHeader">
			Enllevo - Soluções que Geram Satisfação - ERP- Desenvolvimento de Software - ERP - WMS - YMS - TMS - Logistica - Automotivo - Distribuição - Nota Fiscal - Gestão de RH - Consultoria Empresarial - Enllevo, "Sensação ou Estado de Espírito" - Enllevo by Shapeness
			<a href="{{ route('index') }}">
				<img class="logoHeader1024" src="images/logo-enllevo-home.png" title="Marca Enllevo" alt="Marca Enllevo">
			</a>
		</h1>
		<div class="conteudoNavMenu">
			<nav class="navMenu">
				<ul class="listaNavMenu">
					<li>
						<a href="{{ route('index') }}">Início</a>
					</li>
					<li>
						<a href="{{ route('institucional') }}">Institucional</a>
					</li>
					<li class="menuDesktopSolucoes">
						Soluções<span class="iconeSetaMenuSite"></span>
					</li>

					<li class="menuDesktopServicos">
						Serviços<span class="iconeSetaMenuSite"></span>
					</li>

					<li>
					<a href="{{ route('contato') }}">Contato</a>
					</li>
				</ul>
			</nav>
			<ul class="liRedeSocial">
				<li class="redeSocialFacebook">
					<a href="{{ url('https://www.facebook.com/enllevo/') }}" target="_blank" title="Facebook - Enllevo">
						<span class="iconeFacebook"></span>
					</a>
				</li>
				<li class="redeSocialInstagram">
					<a href="{{ url('https://www.instagram.com/enllevo_/') }}" target="_blank" title="Instagran - Enllevo">
						<span class="iconeInstagran"></span>
					</a>
				</li>
				<li class="redeSocialLinkedin">
					<a href="{{ url('https://www.linkedin.com/company/enllevo') }}" target="_blank" title="LinkedIn - Enllevo">
						<span class="iconeLinkedin"></span>
					</a>
				</li>
			</ul>
		</div>
		<div class="abreMenuDesktop invisivel">
			<!--nav class="navAbreMenuSolucoes"-->
				<h4 class="h4Solucoes">Soluções completas de negócios</h4>
					<ul class="solucoesHeader">
						<li>
							<a href="{{ route('solucao-automotivo') }}" title="Enllevo Automotivo">
								<div class="iconeSegAutomotivo divIcones"></div>
								<span class="tituloSolucoes">Automotivo</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-motores-e-maquinas') }}" title="Enllevo Motores e Máquinas">
								<div class="iconeSegMotoresMaquinas divIcones"></div>
								<span class="tituloSolucoes">Motores e Máquinas</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-fornecimento-de-pecas') }}" title="Enllevo Fornecimento de Peças">
								<div class="iconeCotacaoEletronica divIcones"></div>
								<span class="tituloSolucoes">Cotação Eletrônica</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-logistica') }}" title="Enllevo Logística">
								<div class="iconeSegLogistica divIcones"></div>
								<span class="tituloSolucoes">Logística</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-manufatura') }}" title="Enllevo Manufatura">
								<div class="iconeSegManufatura divIcones"></div>
								<span class="tituloSolucoes">Manufatura</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-distribuicao') }}" title="Enllevo Distribuição">
								<div class="iconeSegDistribuicao divIcones"></div>
								<span class="tituloSolucoes">Distribuição</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-gestao-de-estacionamento') }}" title="Enllevo Gestão de Estacionamento">
								<div class="iconeSegEstacionamento divIcones"></div>
								<span class="tituloSolucoes">Gestão de Estacionamento</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-nota-fiscal-eletronica') }}" title="Enllevo Nota Fiscal Eletrônica">
								<div class="iconeSegNotaFiscal divIcones"></div>
								<span class="tituloSolucoes">Nota Fiscal Eletrônica</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-controle-de-obras') }}" title="Enllevo Controles de Obras">
								<div class="iconeSegControleObras divIcones"></div>
								<span class="tituloSolucoes">Controles de Custos de Obras</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-contratos-de-servicos') }}" title="Enllevo Contratos de Serviços">
								<div class="iconeSegContratos divIcones"></div>
								<span class="tituloSolucoes">Contratos de Serviços</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-associacoes') }}" title="Enllevo Associações">
								<div class="iconeSegAssociacoes divIcones"></div>
								<span class="tituloSolucoes">Associações</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-controle-de-cobranca') }}" title="Enllevo Controle de Cobrança Digital">
								<div class="iconeSegCobrancaDigital divIcones"></div>
								<span class="tituloSolucoes">Ações de Cobrança Digital</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-gestao-rh') }}" title="Enllevo Gestão de RH">
								<div class="iconeSegGestaoRh divIcones"></div>
								<span class="tituloSolucoes">Gestão de RH</span>
							</a>
						</li>
						<li>
							<a href="{{ route('solucao-contabil-fiscal') }}" title="Enllevo Contábil e Fiscal">
								<div class="iconeSegContabilFiscal divIcones"></div>
								<span class="tituloSolucoes">Contábil e Fiscal</span>
							</a>
						</li>
					</ul>
			<!--/nav-->
		</div>
			<div class="abreMenuServicos invisivel">
				<h4 class="subtituloServicos">Serviços que associam visão de negócio com integração à tecnologia da informação</h4>
					<ul class="servicosHeader">
						<li>
							<a href="{{ route('desevolvimento-de-software') }}" title="Enllevo Desenvolvimento de Software">
								<div class="iconeSevDesenvolvimentoSoftware divIcones"></div>
								<span class="tituloServicos">Desenvolvimento de Software</span>
							</a>
						</li>
						<li>
							<a href="{{ route('consultoria-empresarial') }}" title="Enllevo Consultoria Empresarial">
								<div class="iconeSevConsultoria divIcones"></div>
								<span class="tituloServicos">Consultoria Empresarial</span>
							</a>
						</li>
						<!--li>
							<a href="erro.php" title="Enllevo Corretora de Seguros">
								<div class="iconeSevSeguros divIcones"></div>
								<span class="tituloServicos">Corretora de Seguros</span>
							</a>
						</li-->
					</ul>
			</div>
	</div>
</header>
<!--Header Resolução 1024px-->
