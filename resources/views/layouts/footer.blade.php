<!--Assinatura do site-->
<form action="{{ url('dados/email') }}" method="POST">
    {{ csrf_field() }}
<div class="assinatura">
	<p class="paragrafoAssinatura">Assine e fique por dentro de conteúdos exclusivos</p>
	<div class="divInputAssinatura assinaturaSite">
            <span class="iconeMktIcon"></span>
		    <input class="inputAssinatura" type="email" name='email' placeholder="Digite seu e-mail">
            {{--  <span class="enviar"><a href="#">Enviar</a></span>  --}}
            <input type="submit" name="submit" value="Enviar" class="enviar">
        </div>
    </div>
</form>

<!-- > /Assinatura do site-->
<aside class="rodape">

	<div class="imgLogoFooter">
		<img src="images/logotipo-enllevo-rodape.png" alt="Logo Enllevo by Shapeness" title="Enllevo - Soluções que Geram Satisfação">
	</div>

	<div class="conjuntoBotoes botoesFooter">
		<ul class="botoes">
			<span class="footerAssesoriaImprensa">
				<li><a href="{{ route('institucional') }}" title="Institucional - Enllevo">Institucional</a></li>
				<li>Transformação Digital<!--a href="{{ route('transformacao-digital') }}" title="Transformação Digital - Enllevo"></a--></li>
				<li>Marca Enllevo<!--a href="{{ route('erro') }}" title="Marca Enllevo - Enllevo"></a--></li>
				<li><a href="{{ route('trabalhe-conosco') }}" title="Trabalhe Conosco - Enllevo">Trabalhe Conosco</a></li>
				<li>Assessoria de Imprensa<!--a href="{{ route('erro') }}" title="Assessoria de Imprensa"></a--></li>
			</span>

			<span class="footerDownload">
				<li><a href="{{ route('contato') }}" title="Contato - Enllevo">Contato</a></li>
				<li><a href="{{ url('https://shapeness.freshdesk.com/support/login') }}" title="Suporte - Enllevo">
					Suporte
				</a></li>
				<li>Parceiros<!--a href="{{ route('erro') }}" title="Parceiros - Enllevo"></a--></li>
				<li>Download<!--a href="{{ route('erro') }}" title="Download - Enllevo"></a--></li>
			</span>
			<span class="footerSiteMap">
				<li>Politica de Privacidade<!--a href="{{ route('erro') }}" title="Politica de Privacidade - Enllevo"></a--></li>
				<li>Termos de Uso<!--a href="{{ route('erro') }}" title="Termos de Uso - Enllevo"></a--></li>
				<li>Site Map<!--a href="{{ route('erro') }}" title="Site Map - Enllevo"></a--></li>
			</span>
		</ul>
	</div>

	<div class="telefones1024">
		<ul class="vendas vendasFooter">
			<li class="vendaTexto vendasSpanFooter">
				<span class="iconeCarrinhoVendas"></span>
				Vendas
			</li>
			<li class="telefoneContato telefoneLiContatoFooter">11 2714 - 2175</li>
		</ul>
		<!--ul class=""-->
			<a href="{{ url('https://shapeness.freshdesk.com/support/login') }}" class="suporte suporteFooter">
				<div class="suporteTexto suporteSpanFooter">

						<span class="iconeSuporteSite"></span>
						Suporte

				</div>
			</a>
			<!--li class="telefoneLiContatoFooter">4003-0258</li-->
		<!--/ul-->
	</div>


	<div class="iconesRedeSocial">
		<ul>
			<li class="redeSocialFacebook">
				<a href="{{ url('https://www.facebook.com/enllevo/') }}" target="_blank" title="Facebook - Enllevo">
					<span class="iconeFacebook"></span>
				</a>
			</li>
			<li class="redeSocialInstagram">
				<a href="{{ url('https://www.instagram.com/enllevo_/') }}" target="_blank" title="Instagram - Enllevo">
					<span class="iconeInstagran"></span>
				</a>
			</li>
			<li class="redeSocialLinkedin">
				<a href="{{ url('https://www.linkedin.com/company/enllevo') }}" target="_blank" title="LinkedIn - Enllevo">
					<span class="iconeLinkedin"></span>
				</a>
			</li>
		</ul>
	</div>
	<div class="divImagemLogo">
		<img src="images/mobile320/logo-shapeness-enllevo.fw.png" alt="Logo Enllevo by Shapeness" title="Enllevo - Soluções que Geram Satisfação">
	</div>
	<div class="telefones telefonesFooter">
		<ul class="vendas vendasFooter">
			<li class="vendaTexto vendasSpanFooter">
				<span class="iconeCarrinhoVendas"></span>
				Vendas
			</li>
			<li class="telefoneContato telefoneLiContatoFooter">11 2714 - 2175</li>
		</ul>

		<div class="suporteTexto suporteSpanFooter">
			<a href="{{ url('https://shapeness.freshdesk.com/support/login') }}" class="suporte suporteFooter">
				<span class="iconeSuporteSite"></span>
				Suporte
			</a>
		</div>

		<!--ul class="suporte suporteFooter">
			<li class="suporteTexto suporteSpanFooter">
				<span class="iconeSuporteSite"></span>
				Suporte
			</li>
			<li class="telefoneLiContatoFooter">4003-0258</li>
		</ul-->
	</div>
</aside>
<footer class="copyright">
	<div class="textoCopy">
		<a href="#topo"><span class="voltaTopo"></span></a>
		<a href="{{ route('index') }}" title="Index - Enllevo">
			<span class="iconeEnllevoMini"></span>
		</a>
		<small class="copy">&copy; Copyright 2018 Enllevo - Todos os direitos reservados.</small>

	</div>

</footer>
