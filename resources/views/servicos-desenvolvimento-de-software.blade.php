<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Desenvolvimento de Software - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaDev">

					<div class="bannerDev">

					</div>

					<section class="bannerSolucaoDev">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
								<a href="index.php">home</a> /
								<a href="index.php">serviços</a> /
								<a href="servicos-desenvolvimento-de-software.blade.php" class="destaqueAzul">desenvolvimento de software</a>
							</span>
						</div>
							<span class="iconeSevDesenvolvimentoSoftware icone15"></span>
							<h1 class="tituloDev">Desenvolvimento de Software</h1>
							<h2 class="textIntroDev">Entrega incremental em todas as versões: os módulos do sistema que são testados e entregues ao cliente a medida que ficarem prontos, para evitar que alguma
							falha contamine o projeto inteiro e ele precise ser refeito por completo. </h2>
							<h3 class="contTextIntroDev">...</h3>
					</section>
					</div>

					<div>
						<img class="imgDev" src="images/solucoes/Desenvolvimento-de-Software/desenvolvimento-de-software.png" alt="Desenvolvimento de Software" title="Enllevo Desenvolvimento de Software">
					</div>
			<!--Cabeçalho-->

			<!--ServiçoDesenvolvimentoDeSoftware-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaDev">
								<h2 class="h2PropSolucao">ficou interessado?</h2>
								<h2 class="h2Proposta">peça sua proposta sem compromisso.</h2>
							</div>
						</a>
						<p class="solucoesDev">conheça nossos serviços de desenvolvimento de software</p>
						<hr>
					</aside>

					<div class="divSolucoesDev">
						<ul>
							<li><a href="#fabricaProjetos"><h1>fabrica de projetos</h1></a></li>
							<li><a href="#devSistemas"><h1>desenvolvimento de sistemas</h1></a></li>
							<li><a href="#gestaoSistemas"><h1>gestão de manutenção de sistemas</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosDev segmentoDev" id="fabricaProjetos">
						<div class="introDev">
							<h1>fábrica de projetos |</h1><h1 class="segDev">projetos</h1>

							<h2>Enllevo MES - <strong>Manufacturing Execution System</strong></h2>
						</div>
						<hr class="hrDev">
						<img class="imgGestaoDev" src="images/solucoes/Automotivo/veiculos-de-passeio-01.png" alt="Desenvolvimento de Software" title="Desenvolvimento de Software">

						<div class="descricaoDev">
							<p>...</p>

							<p>...</p>

							</div>

						<div class="secaoDev">
						<ul>
							<div class="gestaoDev">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="entregasArmazem">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosDev conteinerDev">
							<div class="beneficiosDev">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Otimizaço de tempo</li>
									<li>Otimização de custos</li>
									<li>Estratégia para manutenção do foco da empresa</li>
									<li>Recursos e aproveitamento da fábrica de projetos Enllevo</li>
								</ul>
							</div>
							<div class="caracteristicasDev">
								<p>Características do Serviço</p>
								<ul class="caracteristicaDev">
									<li class="liNeutra">Baseado no modelo Scrum (modelo ágil de gestão)</li>
									<li>Desenvolvimento completo de sistema de informação a partir das demandas específicas de cada empresa</li>
									<li>Processo de concepção</li>
									<li>Desenho</li>
									<li>Construção</li>
									<li>Testes</li>
									<li>Implantação do software</li>
									<li>Recursos de ponta para a elaboração de um software customizado e parametrizado</li>
									<li>Definições efetuadas pela Enllevo</li>
									<li>Protocolo claro de aceite do cliente em todas as fases do desenvolvimento</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->
					<section class="segmentosDev segmentoDevOposto" id="devSistemas">
						<div class="introDevOposto">
							<h1 class="h1IntroDev">desenvolvimento de sistemas |</h1><h1 class="segDevOposto">projetos</h1>

							<h2>Enllevo APS - <strong>Planejamento e Escalonamento Avançado</strong></h2>
						</div>
						<hr class="hrDev hrOposto">
						 <img class="imgGestaoDev imgGestaoOposto" src="images/solucoes/Logistica/tms-a.png" alt="" title="">

						<div class="descricaoDev descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoDev secaoOposto">
							<ul>
								<div class="gestaoDev gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosDev conteinerBeneficiosOposto">
							<div class="beneficiosDev beneficiosOposto">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Sistema customizado</li>
									<li>Atende ás necessidades específicas da empresa e do projeto</li>
									<li>Atividades baseadas em um acordo de nível de serviço</li>
									<li>Definição dos requisitos de negócio a partir de documentação padronizada com critérios de entrega e qualidade definidos</li>
								</ul>
							</div>
							<div class="caracteristicasDev caracteristicasOposto">
								<p>Características do Serviço</p>
								<ul class="caracteristicaDev">
									<li class="liNeutra">Produção padronizada de programas seguindo especificações definidas pelo cliente</li>
									<li>Definição e arquitetura de solução a partir do cliente</li>
									<li>Codificação e testes unitários, integrados com tecnologia Enllevo</li>
									<li>Nível de serviço das atividades pactuado entre as partes</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->

					<section class="segmentosDev segmentoDev" id="gestaoSistemas">
						<div class="introDev">
							<h1>gestão de manutenção de sistemas |</h1><h1 class="segDev">manutenção corretiva</h1>

							<h2>Enllevo MES - <strong>Manufacturing Execution System</strong></h2>
						</div>
						<hr class="hrDev">
						<img class="imgGestaoDev" src="images/solucoes/Automotivo/veiculos-de-passeio-01.png" alt="" title="">

						<div class="descricaoDev">
							<p>...</p>

							<p>...</p>

							</div>

						<div class="secaoDev">
						<ul>
							<div class="gestaoDev">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="entregasArmazem">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosDev conteinerDev">
							<div class="beneficiosDev">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Ajustes específicos de acordo com as funcionalidades de cada software</li>
									<li>Redução de custos</li>
									<li>Permite solucionar problemas simples de forma simples</li>
									<li>Elabora uma versão melhor e mais evoluída de um software já em execução, mas com problemas complexos</li>
								</ul>
							</div>
							<div class="caracteristicasDev">
								<p>Características do Serviço</p>
								<ul class="caracteristicaDev">
									<li class="liNeutra">Correção de erros não identificados na fase de testes</li>
									<li>Ajustes de erros simples que não atrapalham a execução do software</li>
									<li>Reparos temporários de problemas mais complexos no sistema</li>
									<li>Disponibilidade de uma nova versão do software, com a solução do erro na totalidade</li>
								</ul>
							</div>
						</aside>
					</section>

					<!--Bloco Oposto-->
					<section class="segmentosDev segmentoDevOposto">
						<div class="introDevOposto">
							<h1 class="h1IntroDev">gestao de manutenção de sistemas |</h1><h1 class="segDevOposto">manutenção adaptativa</h1>

							<h2>Enllevo APS - <strong>Planejamento e Escalonamento Avançado</strong></h2>
						</div>
						<hr class="hrDev hrOposto">
						 <img class="imgGestaoDev imgGestaoOposto" src="images/solucoes/Logistica/tms-a.png" alt="" title="">

						<div class="descricaoDev descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoDev secaoOposto">
							<ul>
								<div class="gestaoDev gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosDev conteinerBeneficiosOposto">
							<div class="beneficiosDev beneficiosOposto">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Otimização de tempo</li>
									<li>Otimização de serviço "técnico" (sistema realiza as mudanças, não os funcionários)</li>
									<li>Possibilidade de criação de uma nova plataforma hardware</li>
									<li>Nova geração de processadores em que o software aproveita seus recursos</li>
									<li>Nova versão de um sistema operacional</li>
								</ul>
							</div>
							<div class="caracteristicasDev caracteristicasOposto">
								<p>Características do Serviço</p>
								<ul class="caracteristicaManufatura">
									<li class="liNeutra">Adaptação do software para mudanças no ambiente externo</li>
									<li>Parametrização de acordo com alterações de regras, leis, normas, entre outros</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->


					<section class="segmentosDev segmentoDev">
						<div class="introDev">
							<h1>gestão de manutenção de sistemas |</h1><h1 class="segDev">manutenção preventiva</h1>

							<h2>Enllevo MES - <strong>Manufacturing Execution System</strong></h2>
						</div>
						<hr class="hrDev">
						<img class="imgGestaoDev" src="images/solucoes/Automotivo/veiculos-de-passeio-01.png" alt="" title="">

						<div class="descricaoDev">
							<p>...</p>

							<p>...</p>

							</div>

						<div class="secaoDev">
						<ul>
							<div class="gestaoDev">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="entregasArmazem">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosDev conteinerDev">
							<div class="beneficiosDev">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Mantém o software na versão mais atualizada possível</li>
									<li>Sistema já preparado para possíveis atualizações</li>
									<li>Possível erro ou limitação do problema é tratado antes que um problema possa acontecer</li>
								</ul>
							</div>
							<div class="caracteristicasDev">
								<p>Características do Serviço</p>
								<ul class="caracteristicaDev">
									<li class="liNeutra">Alterações no software para melhorar a confiabilidade</li>
									<li>Oferece melhor estrutura para possíveis manutenções</li>
									<li>Gerada a partir da observação dos mantenedores sobre o que pode gerar erro ou limitação</li>
								</ul>
							</div>
						</aside>
					</section>


					<!--Bloco Oposto-->
					<section class="segmentosDev segmentoDevOposto">
						<div class="introDevOposto">
							<h1 class="h1IntroDev">gestao de manutenção de sistemas |</h1><h1 class="segDevOposto">manutenção evolutiva</h1>

							<h2>Enllevo APS - <strong>Planejamento e Escalonamento Avançado</strong></h2>
						</div>
						<hr class="hrDev hrOposto">
						 <img class="imgGestaoDev imgGestaoOposto" src="images/solucoes/Logistica/tms-a.png" alt="" title="">

						<div class="descricaoDev descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoDev secaoOposto">
							<ul>
								<div class="gestaoDev gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegDev" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosDev conteinerBeneficiosOposto">
							<div class="beneficiosDev beneficiosOposto">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Melhor desempenho do software</li>
									<li>Desenvolvimento de novas funcionalidades ao software</li>
									<li>Redução de custos nas operações</li>
									<li>Sistema mais evoluído sem a necessidade de implementar um novo</li>
								</ul>
							</div>
							<div class="caracteristicasDev caracteristicasOposto">
								<p>Características do Serviço</p>
								<ul class="caracteristicaDev">
									<li class="liNeutra">Manutenções não previstas no documento original</li>
									<li>Melhorar a qualidade do software</li>
									<li>Modificação de código-fonte para melhor legibilidade</li>
									<li>Adequação a alguns paradigmas de programação</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->

			<!--Desenvolvimento de Software-->


			<!--Backoffice-->
				<aside class="backofficeDev">
					<div class="textoBackofficeDev">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesDev">
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="erro.php" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer');
			<!-- footer -->


        </main>
    </body>
</html>
