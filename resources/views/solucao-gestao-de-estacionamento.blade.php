<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Gestão de Estacionamento - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaGestao">

					<div class="bannerGestao">

					</div>

					<section class="bannerSolucaoGestao">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-gestao-de-estacionamento') }}" class="destaqueAzul">gestão de estacionamento</a>
							</span>
						</div>
							<span class="iconeSegEstacionamento icone10"></span>
							<h1 class="tituloGestao">Solução Gestão de Estacionamento</h1>
							<h2 class="textIntroGestao">Solução certa para todos os tipos de...</h2>
							<h3 class="contTextIntroGestao">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
					</div>

					<div>
						<img class="imgGestao" src="images/solucoes/Gestao-de-Estacionamento/gestao-de-estacionamento-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaGestao">
								<h2 class="h2PropSolucao">ficou interessado na solução?</h2>
								<h2 class="h2Proposta">peça uma proposta sem compromisso.</h2>
							</div>
						</a>
						<p class="solucoesGestao">conheça nossas soluções para gestão de estacionamento</p>
						<hr>
					</aside>

					<div class="divSolucoesGestao">
						<ul>
							<li><a href="#financeiro"><h1>financeiro</h1></a></li>
							<li><a href="#fiscal"><h1>fiscal</h1></a></li>
							<li><a href="#comercial"><h1>comercial</h1></a></li>
							<li><a href="#clientes"><h1>clientes</h1></a></li>
							<li><a href="#dp"><h1>dp</h1></a></li>
							<li><a href="#operacional"><h1>operacional</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosGestao segmentoGestao" id="veiculoComercial">
						<div class="introGestao">
							<h1>financeiro |</h1><h1 class="segGestao">contas a pagar</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						
						<hr class="hrGestao">

						<div class="descricaoGestao">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoGestao">
						<ul>
							<div class="gestaoGestao">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerGestao">
							<div class="beneficiosGestao">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Automatiza os lançamentos dos processos de contas a pagar.</li>
									<li>Fluxo de Aprovações</li>
									<li>Automatiza os processos de liberação financeira de pagamento</li>
									<li>Gere arquivos únicos que são importados diretamente no sistema bancário</li>
								</ul>
							</div>
							<div class="caracteristicasGestao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li class="liNeutra">Lançamento de contas a pagar</li>
									<li>Geração de arquivos de remessa bancária.</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->
					<section class="segmentosGestao segmentoGestaoOposto">
						<div class="introGestaoOposto">
							<h1 class="h1IntroGestao"> financeiro |</h1><h1 class="segGestaoOposto">contas a receber</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrGestao hrOposto">

						<div class="descricaoGestao descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoGestao secaoOposto">
							<ul>
								<div class="gestaoGestao gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-01.png" alt="" title="">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-02.png" alt="" title="">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-03.png" alt="" title="">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-04.png" alt="" title="">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-05.png" alt="" title="">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-06.png" alt="" title="">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-07.png" alt="" title="">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-08.png" alt="" title="">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-09.png" alt="" title="">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-10.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-11.png" alt="" title="">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-12.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-13.png" alt="" title="">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-14.png" alt="" title="">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficiosOposto">
							<div class="beneficiosGestao beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Sincronize automaticamente os dados financeiros de uma filial com os dados dos sistemas administrativos</li>
									<li>Garante a entrega dos boletos para os mensalistas sem gastos de impressão e entrega</li>
									<li>Atualiza de forma automática os dados de pagamentos recebidos na sua conta bancária</li>
									<li>Garante uma base ativa e atualizada dos clientes nas unidades e nos sistemas administrativos</li>
								</ul>
							</div>
							<div class="caracteristicasGestao caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li class="liNeutra">Envio de boletos no e-mail do cliente</li>
									<li>Envio de avisos de vencimento do boleto</li>
									<li>Importação de arquivos de retorno</li>
									<li>Descadastramento automático de inadimplentes</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->
					<section class="segmentosGestao segmentoGestao">
						<div class="introGestao">
							<h1>financeiro |</h1><h1 class="segGestao">tesouraria</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrGestao">
						<img class="imgGestaoGestao" src="images/solucoes/Logistica/yms-a.png" alt="" title="">

						<div class="descricaoGestao">
							<p>...</p>

							<p>...</p>
						</div>


						<div class="secaoGestao">
							<ul>

								<div class="gestaoGestao">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-01.png" alt="" title="">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-02.png" alt="" title="">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-03.png" alt="" title="">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-04.png" alt="" title="">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-05.png" alt="" title="">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-06.png" alt="" title="">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-07.png" alt="" title="">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-08.png" alt="" title="">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-09.png" alt="" title="">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-10.png" alt="" title="">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-11.png" alt="" title="">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-12.png" alt="" title="">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficioGestao">
							<div class="beneficiosGestao beneficioGestao">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Interface para checagem dos fechamentos de caixas recebidos de suas filiais</li>
									<li>Controle a movimentação financeira da caixinha</li>
								</ul>
							</div>


							<div class="caracteristicasGestao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li>Conciliação de caixa</li>
									<li>Controle de caixinha</li>
								</ul>
							</div>
						</aside>
					</section>

				<!--Bloco Oposto-->
					<section class="segmentosGestao segmentoGestaoOposto" id="fiscal">
						<div class="introGestaoOposto">
							<h1 class="h1IntroGestao"> fiscal |</h1><h1 class="segGestaoOposto">RPS</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrGestao hrOposto">

						<div class="descricaoGestao descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoGestao secaoOposto">
							<ul>
								<div class="gestaoGestao gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-01.png" alt="" title="">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-02.png" alt="" title="">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-03.png" alt="" title="">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-04.png" alt="" title="">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-05.png" alt="" title="">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-06.png" alt="" title="">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-07.png" alt="" title="">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-08.png" alt="" title="">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-09.png" alt="" title="">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-10.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-11.png" alt="" title="">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-12.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-13.png" alt="" title="">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-14.png" alt="" title="">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficiosOposto">
							<div class="beneficiosGestao beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Emita RPS para seus clientes</li>
									<li>Envie de forma automática para prefeitura</li>
								</ul>
							</div>
							<div class="caracteristicasGestao caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li class="liNeutra">Emissão e envio de RPS para prefeitura</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->

					<section class="segmentosGestao segmentoGestao">
						<div class="introGestao">
							<h1>fiscal |</h1><h1 class="segGestao">NFE</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrGestao">

						<div class="descricaoGestao">
							<p>...</p>

							<p>...</p>
						</div>


						<div class="secaoGestao">
							<ul>

								<div class="gestaoGestao">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-01.png" alt="" title="">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-02.png" alt="" title="">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-03.png" alt="" title="">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-04.png" alt="" title="">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-05.png" alt="" title="">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-06.png" alt="" title="">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-07.png" alt="" title="">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-08.png" alt="" title="">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-09.png" alt="" title="">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-10.png" alt="" title="">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-11.png" alt="" title="">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-12.png" alt="" title="">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficioGestao">
							<div class="beneficiosGestao beneficioGestao">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Converte os RPS em NFE dentro do prazo</li>
									<li>Envie as NFE diretamente no e-mail dos seus clientes</li>
								</ul>
							</div>


							<div class="caracteristicasGestao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li>Conversão de RPS em NFE</li>
									<li>Disponibilização de NFE para seus clientes</li>
								</ul>
							</div>
						</aside>
					</section>

				<!--Bloco Oposto-->
					<section class="segmentosGestao segmentoGestaoOposto" id="comercial">
						<div class="introGestaoOposto">
							<h1 class="h1IntroGestao"> comercial |</h1><h1 class="segGestaoOposto">prospecção de novos negócios</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrGestao hrOposto">
						
						<div class="descricaoGestao descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoGestao secaoOposto">
							<ul>
								<div class="gestaoGestao gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-01.png" alt="" title="">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-02.png" alt="" title="">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-03.png" alt="" title="">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-04.png" alt="" title="">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-05.png" alt="" title="">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-06.png" alt="" title="">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-07.png" alt="" title="">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-08.png" alt="" title="">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-09.png" alt="" title="">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-10.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-11.png" alt="" title="">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-12.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-13.png" alt="" title="">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-14.png" alt="" title="">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficiosOposto">
							<div class="beneficiosGestao beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Gerencie as propostas que estão em aberto para fechamento de novos negócios</li>
								</ul>
							</div>
							<div class="caracteristicasGestao caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li class="liNeutra">Propspecção de novas unidades</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->

					<section class="segmentosGestao segmentoGestao">
						<div class="introGestao">
							<h1>comercial |</h1><h1 class="segGestao">controle de contratos</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrGestao">
						
						<div class="descricaoGestao">
							<p>...</p>

							<p>...</p>
						</div>


						<div class="secaoGestao">
							<ul>

								<div class="gestaoGestao">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-01.png" alt="" title="">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-02.png" alt="" title="">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-03.png" alt="" title="">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-04.png" alt="" title="">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-05.png" alt="" title="">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-06.png" alt="" title="">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-07.png" alt="" title="">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-08.png" alt="" title="">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-09.png" alt="" title="">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-10.png" alt="" title="">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-11.png" alt="" title="">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-12.png" alt="" title="">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficioGestao">
							<div class="beneficiosGestao beneficioGestao">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Gerencie e salve os contratos ativos e inativos de forma digital</li>
								</ul>
							</div>


							<div class="caracteristicasGestao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li>Lançamentos e gestão de contratos</li>
								</ul>
							</div>
						</aside>
					</section>

					<!--Bloco Oposto-->
					<section class="segmentosGestao segmentoGestaoOposto" id="comercial">
						<div class="introGestaoOposto">
							<h1 class="h1IntroGestao"> comercial |</h1><h1 class="segGestaoOposto">tabelas de preços</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrGestao hrOposto">
						
						<div class="descricaoGestao descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoGestao secaoOposto">
							<ul>
								<div class="gestaoGestao gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-01.png" alt="" title="">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-02.png" alt="" title="">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-03.png" alt="" title="">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-04.png" alt="" title="">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-05.png" alt="" title="">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-06.png" alt="" title="">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-07.png" alt="" title="">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-08.png" alt="" title="">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-09.png" alt="" title="">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-10.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-11.png" alt="" title="">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-12.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-13.png" alt="" title="">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-14.png" alt="" title="">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficiosOposto">
							<div class="beneficiosGestao beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Crie e gerencie as tabelas de preço de acordo com as necessidades do seu negócio</li>
									<li>Receba lembretes periódicos de reajustes das tabelas de preços</li>
									<li>Antecipe da sua concorrência, lançando tabelas promocionais, sabendo o momento ideal para realizar as reajustes</li>
								</ul>
							</div>
							<div class="caracteristicasGestao caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li class="liNeutra">Criação de tabelas de preço</li>
									<li>Controle de reajustes de tabelas de preço</li>
									<li>Gestão de concorrência</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->

					<section class="segmentosGestao segmentoGestao" id="comercial">
						<div class="introGestao">
							<h1>comercial |</h1><h1 class="segGestao">tabela de preço</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrGestao">
						
						<div class="descricaoGestao">
							<p>...</p>

							<p>...</p>
						</div>


						<div class="secaoGestao">
							<ul>

								<div class="gestaoGestao">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-01.png" alt="" title="">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-02.png" alt="" title="">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-03.png" alt="" title="">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-04.png" alt="" title="">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-05.png" alt="" title="">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-06.png" alt="" title="">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-07.png" alt="" title="">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-08.png" alt="" title="">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-09.png" alt="" title="">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-10.png" alt="" title="">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-11.png" alt="" title="">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-12.png" alt="" title="">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficioGestao">
							<div class="beneficiosGestao beneficioGestao">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Venda de vagas mensais de maneira totalmente online</li>
									<li>Permita a troca de vagas mensais por tickets avulsos para que seu cliente não cancele a vaga</li>
									<li>Seus clientes poderão alterar veículos e dados pessoais de forma totalmente independente</li>
									<li>Venda de tickets avulsos com pagamento antecipado</li>
									<li>Se não houver mais vagas, crie uma lista de espera, chame seu cliente quando houver disponibilidade</li>
									<li>Acesso a 2ª via do boleto pelo portal</li>
								</ul>
							</div>


							<div class="caracteristicasGestao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li>Venda de vagas mensalistas</li>
									<li>Permutação de vagas mensalistas</li>
									<li>Interface de atualização de dados cadastrais</li>
									<li>Venda de tickets pré-pago</li>
									<li>Cadastro em lista de espera</li>
									<li>Visualização de 2ª via de boleto</li>
								</ul>
							</div>
						</aside>
					</section>

				<!--Bloco Oposto-->
					<section class="segmentosGestao segmentoGestaoOposto" id="dp">
						<div class="introGestaoOposto">
							<h1 class="h1IntroGestao"> DP |</h1><h1 class="segGestaoOposto">gestão de departamento pessoal</h1>

							<h2>Enllevo TMS - <strong>Transportation Management System</strong></h2>
						</div>
						<hr class="hrGestao hrOposto">
						

						<div class="descricaoGestao descricaoOposto">
							<p>...</p>

							<p>...</p>

						</div>

						<div class="secaoGestao secaoOposto">
							<ul>
								<div class="gestaoGestao gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-01.png" alt="" title="">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-02.png" alt="" title="">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-03.png" alt="" title="">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-04.png" alt="" title="">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-05.png" alt="" title="">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-06.png" alt="" title="">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-07.png" alt="" title="">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-08.png" alt="" title="">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-09.png" alt="" title="">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-10.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-11.png" alt="" title="">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-12.png" alt="" title="">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-13.png" alt="" title="">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/tms-14.png" alt="" title="">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficiosOposto">
							<div class="beneficiosGestao beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Gerencie o cadastro dos colaboradores, mantenha todas as informações em um único lugar</li>
									<li>Emita contratos, cartas, lembretes, advertências em um clique</li>
									<li>Tenha seu próprio sistema de registro de ponto por biometria e visualize online</li>
									<li>Gerencie e disponibilize os espelhos de pontos dos seus colaboradores de forma digital</li>
									<li>Disponibilize todos os holleriths no portal do colaborador</li>
									<li>Emita arquivos com dados variáveis da sua folha de pagamento</li>
									<li>Gerencie os períodos aquisitivos de férias, lance períodos de férias e receba avisos de datas limites para gozo</li>
								</ul>
							</div>
							<div class="caracteristicasGestao caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li class="liNeutra">Cadastro de colaboradores</li>
									<li>Emissão de cartas e contratos personalizados</li>
									<li>Registro de ponto biométrico</li>
									<li>Controle do espelho de ponto</li>
									<li>Geração de holleriths</li>
									<li>Folha de pagamento</li>
									<li>Férias</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->

					<section class="segmentosGestao segmentoGestao" id="operacional">
						<div class="introGestao">
							<h1>operacional |</h1><h1 class="segGestao">sistema operacional</h1>

							<h2>Enllevo YMS - <strong>Yard Management System</strong></h2>
						</div>
						<hr class="hrGestao">

						<div class="descricaoGestao">
							<p>...</p>

							<p>...</p>
						</div>


						<div class="secaoGestao">
							<ul>

								<div class="gestaoGestao">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-01.png" alt="" title="">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-02.png" alt="" title="">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-03.png" alt="" title="">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-04.png" alt="" title="">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-05.png" alt="" title="">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-06.png" alt="" title="">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-07.png" alt="" title="">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-08.png" alt="" title="">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-09.png" alt="" title="">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-10.png" alt="" title="">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-11.png" alt="" title="">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegGestao" src="images/solucoes/Logistica/yms-12.png" alt="" title="">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosGestao conteinerBeneficioGestao">
							<div class="beneficiosGestao beneficioGestao">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Controle de garagens totalmente automatizadas, semiautomatizadas e não automatizadas</li>
									<li>Evite erros, leia um ticket e saiba o valor da cobrança</li>
									<li>Feche uma vaga mensal diretamente na unidade e faça o lançamento desse cadastro</li>
									<li>Venda credenciais e tags para maior conforto na entrada e saída dos clientes</li>
									<li>Emita relatórios de pátio e realize auditorias</li>
									<li>Sistema opera normalmente offline e volta a sincronizar os dados quando a conexão é reestabelecida</li>
								</ul>
							</div>


							<div class="caracteristicasGestao">
								<p>Características do Sistema</p>

								<ul class="caracteristicaGestao">
									<li>Controle de entrada e saída de veículos na unidade</li>
									<li>Calculadora de cobrança de tickets avulsos</li>
									<li>Venda de vagas mensalistas</li>
									<li>Venda de produtos (tags, credenciais, etc)</li>
									<li>Relatórios para auditorias de pátio</li>
									<li>Funcionamento Offline</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeGestao">
					<div class="textoBackofficeGestao">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesGestao">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
