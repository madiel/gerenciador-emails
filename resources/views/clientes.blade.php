
    <head>
        <title>Enllevo - Nossos Clientes - Enllevo - Soluções que Geram Satisfação</title>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- BannerClientes -->
			<div class="conteudoBanner">
				<div class="imagemBannerInstitucional">
					<img class="imgInstitucional" src="images/banner-topo-enllevo.jpg" title="Enllevo - Soluções que Geram Satisfação" alt="Institucional">
				</div>
				<div class="textoBannerContato">
					<h1 class="contatoBanner">Nossos Clientes</h1>
				</div>
			</div>
			<!-- BannerClientes -->

			<!--Clientes-->
				<aside class="backgroundClientes">
					<h3 class="nossosClientes">Junte-se aos nossos clientes.</h3>

					<div class="divNossosClientes">
						<div class="imgNossosClientes">
							<img src="./images/clientes/acos-motta-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/aoki-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/assobens-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/avar-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/buscarioli-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/davoli-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/dilepe-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/hdi-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/inga-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/mhm-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/revimaqi-enllevo.jpg">
						</div>

						<div class="imgNossosClientes">
							<img src="./images/clientes/veisa-enllevo.jpg">
						</div>
					</div>
				</aside>
			<!--Clientes-->


			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
    </body>
</html>
