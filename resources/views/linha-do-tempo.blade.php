<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Modo História - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- BannerInstitucional -->
			<div class="conteudoBanner">
				<div class="imagemBannerInstitucional">
					<img class="imgInstitucional" src="images/banner-topo-enllevo.jpg" title="Enllevo - Soluções que Geram Satisfação" alt="Institucional">
				</div>
				<div class="textoBannerInstitucional">
					<h1 class="sobreBanner">Modo História</h1>
					<span class = "opcoesBannerInstitucional">
						<a href="{{ route('institucional') }}">Sobre</a>   |
						<a class="destaqueVerde" href="{{ route('linhadotempo') }}">Modo História</a>
					</span>
				</div>
			</div>
			<!-- BannerInstitucional -->

			<!--TextoLinhaDoTempo-->
			<div class="backgroundLinhaDoTempo">
			<span class="caminhoSite">
				<a href="{{ route('index') }}">home</a> /
				<a href="{{ route('institucional') }}">institucional</a> /
				<a href="{{ route('linhadotempo') }}" class="destaqueAzul">modo história</a>
			</span>
				<section class="introducaoLinhaDoTempo">
					<h4 class="tituloLinhaDoTempo">Um pouco de nossa história...</h4>
					<p class="textoLinhaDoTempo">A Shapeness evoluiu e se tornou a Enllevo.
					Multiplicou seus negócios, criando soluções inovadoras ao longo de sua trajetória,
					transformando o dia a dia das mais diferentes empresas.</p>
				</section>
			<!--TextoLinhaDoTempo-->

			<!--LinhaDoTempo-->
				<aside class="LinhaDoTempo linhaDoTempo ">
					<span class="LinhaDoTempoCenter"></span>

					<!--Bloco 01-->
					<div class="imgTopoLinhaDoTempo">
						<img src="images/mobile320/shapeness-logo.png" alt="Marca Shapeness" title="Marca Shapeness">
					</div>

					<div class="bolinha"></div>
					<div class="secao1 secaoLinhaDoTempo ">

						<img class="lTempoImg1" src="images/linha-do-tempo/01.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao1LinhadoTempo">06 de Abril de 1986</p>
							<span class="textoSecao1LinhadoTempo">
								<p>Em 06 de Abril de 1986, Edison e Roberto fundaram a Shapeness Informatica, com sua sede no bairro do Paraíso em São Paulo.</p>
							</span>
						</div>
					</div>

					<div class="bolinha"></div>

					<div class="secao2 secaoLinhaDoTempo ">
						<p class="tituloSecao2LinhadoTempo">Experiência no Ramo</p>

						<span class="textoSecao2LinhadoTempo">
							<p>Edison e Roberto que tinha uma vasta experiência na área de informática, tiveram a ideia de criar uma empresa no setor de "computação". E foi no banco BCN que os dois se encontram para fundar a Shapeness.</p><br>

							<p>"Naquela época nos pensavamos em crescer e fazer um serviço diferenciado, criamos ERP e damos um bom atendimento no pós venda."</p>
						</span>
					</div>

					<div class="bolinha"></div>

					<div class="secao3 secaoLinhaDoTempo">
						<img class="lTempoImg3" src="images/linha-do-tempo/02.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao3LinhadoTempo">ERP</p>
							<span class="textoSecao3LinhadoTempo">
								<p>Em 1987 a Shapeness já tinha sua carteira de clientes e atendiam os ramos de doces, bebidas e alimentos com o ERP.</p>
							</span>
						</div>
					</div>
					<!--Bloco 01-->

					<!--Bloco 02-->
					<div class="bolinha"></div>
					<div class="secao4 secaoLinhaDoTempo ">

						<img class="lTempoImg1" src="images/linha-do-tempo/03.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao1LinhadoTempo">Ipiranga</p>
							<span class="textoSecao1LinhadoTempo">
								<p>Em 1988 a Shapeness coloca sua sede no Bairro do Ipiranga em São Paulo. Um lugar de fácil acesso com muitas pequenas e médias empresas.</p>
							</span>
						</div>
					</div>

					<div class="bolinha"></div>

					<div class="secao5 secaoLinhaDoTempo ">
						<p class="tituloSecao2LinhadoTempo">A importância da Informatica</p>

						<span class="textoSecao2LinhadoTempo">
							<p>Em 20 de julho de 1990, Edison e Roberto em uma entrevista para o Jornal Folha do Ipiranga afirmam:  </p><br>

							<p>"Não queremos vender apenas pacotes, que são sistemas prontos (conjunto de programas), mas sim dar assessoria para cada empresa, fornecer informações, equipamentos, vender serviços adequado, treinamentos, suporte e sistemas específicos."</p>
						</span>
					</div>

					<div class="bolinha"></div>

					<div class="secao6 secaoLinhaDoTempo">
						<img class="lTempoImg3" src="images/linha-do-tempo/04.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao3LinhadoTempo">Flexibilidade</p>
							<span class="textoSecao3LinhadoTempo">
								<p>Se o mundo da computação estava procurando por um novo padrão ou não, ele encontrou um em maio de 1990, quando a Microsoft finalmente lançou o Windows 3.0, era executado sobre o DOS e, portanto, oferecia compatibilidade com os programas DOS</p><br>

								<p>"O computador nos da novas flexibilidade Indispensáveis para as empresas se adaptar ás novas realidades. Para isso é preciso de sistemas "ágil" e uma estrutura que se encaixe em suas necessidades.</p><br>

								<p>A Shapeness projetava vários sistemas personalizados como: conta a receber, controle de estoques e contabilidade geral.</p>


							</span>
						</div>
					</div>
					<!--Bloco 02-->


						<!--Bloco 03-->
					<div class="bolinha"></div>
					<div class="secao1 secaoLinhaDoTempo ">

						<img class="lTempoImg1" src="images/linha-do-tempo/05.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao1LinhadoTempo">Uma nova estrutura</p>
							<span class="textoSecao1LinhadoTempo">
								<p>Em 1994 Roberto se desligando da Shapeness e Edison continuou em frente dos negócios. A Shapeness continuou crescendo e se fortalecendo.</p>
							</span>
						</div>
					</div>

					<div class="bolinha"></div>

					<div class="secao2 secaoLinhaDoTempo ">
						<p class="tituloSecao2LinhadoTempo">Novos sócios, novos seguimentos</p>

						<span class="textoSecao2LinhadoTempo">

							<p>Em 1995 outros segmentos começaram a surgir, mais clientes precisavam das soluções em sistemas da Shapeness.</p><br>

							<p>Uma nova etapa se iniciava em 1995 no bairro da Sáude em São Paulo. Com uma nova sede a Shapeness evoluia em seus sistemas e soluções para os clientes.</p><br>

							<p>Em 1999 alguns funcionários se tornaram sócios agregando valor aos negócios da Shapeness.</p><br>

							<p>2010 a Shapeness iniciava algumas mudanças em seus sistemas, o ERP passa a ser ERP Web, funcionando em navegadores e com melhor interações.</p><br>

							<p>Início da parceria com a Associação Brasileira dos Concessionários Mercedes-Benz.</p>

						</span>
					</div>

					<div class="bolinha"></div>

					<div class="secao3 secaoLinhaDoTempo">
						<img class="lTempoImg3" src="images/linha-do-tempo/06.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao3LinhadoTempo">ERP Web</p>
							<span class="textoSecao3LinhadoTempo">
								<p>Em 2014, no mesmo bairro, porém em sua 4º sede com 300m² a Shapeness mais estruturada e com novas metas para evoluir e se manter firme no mercado juntos aos concorrentes em potênciais.</p>
							</span>
						</div>
					</div>
					<!--Bloco 03-->

						<!--Bloco 04-->
					<div class="bolinha"></div>
					<div class="secao4 secaoLinhaDoTempo ">

						<img class="lTempoImg1" src="images/linha-do-tempo/07.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao1LinhadoTempo">Enllevo</p>
							<span class="textoSecao1LinhadoTempo">
								<p>A Shapeness sempre acompanhou os avanços e tecnologia ao longo dos anos, e em 2018 iniciou mais um processo de atualização, a criação de uma nova marca, nova identidade e uma nova cultura.</p>
							</span>
						</div>
					</div>

					<div class="bolinha"></div>

					<div class="secao5 secaoLinhaDoTempo ">
						<p class="tituloSecao2LinhadoTempo">Enllevo by Shapeness</p>

						<span class="textoSecao2LinhadoTempo">
							<p>Vivemos uma transformação digital, a 4º revolução industrial, e a Shapeness passou a se chamar Enllevo que significa "Sensação ou Estado de Espírito" - Êxtase, Encanto, Entusiasmo, Maravilha, Deliciar e Satisfação.</p><br>

							<p>Com a mudança não somente do nome e identidade a Enllevo também constroi uma nova cultura, acompanhando uma transformação digital, melhorando ainda mais suas soluções, processos e serviços, juntos aos seus clientes. </p>

						</span>
					</div>

					<div class="bolinha"></div>

					<div class="secao6 secaoLinhaDoTempo">
						<img class="lTempoImg3" src="images/linha-do-tempo/08.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao3LinhadoTempo">Soluções que Geram Satisfações</p>
							<span class="textoSecao3LinhadoTempo">
								<p>Referência nacional no desenvolvimento de soluções o qual os processos de negócios exijam aprofundamento em requisitos específicos, utilizando práticas em gestão empresarial, logística, gestão de pessoas, relacionamento com clientes, consultorias em tecnologia da informação e segurança, permitindo a total integracão de sua gestão ao seu modelo estratégico.</p>

								<p>Nossas soluções são integradas e atendem tanto o core business quanto o back office dos nossos clientes.</p><br>

								<p>A Enllevo, que possui o DNA da Shapeness, é uma empresa brasileira com mais de 32 anos no mercado e detentora de toda a tecnologia e recursos de gestão necessária ao desenvolvimento e soluções de negócios, oferece recursos de ponta em software para os mais variados segmentos de mercado. Esse é o legado que a Enllevo deseja carregar como herança.</p><br>

									<p>Uma empresa que está preocupada em oferecer, na medida certa, as melhores e mais adequadas soluções para uma gestão profícua e equilibrada.</p>

							</span>
						</div>
					</div>
					<!--Bloco 04-->

					<!--Bloco 05-->
					<div class="bolinha"></div>
					<div class="secao1 secaoLinhaDoTempo ">

						<img class="lTempoImg1" src="images/linha-do-tempo/09.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao1LinhadoTempo">A experiência Enllevo</p>
							<span class="textoSecao1LinhadoTempo">
								<p>Entendemos o seu segmento, conhecemos de tecnologia, entregamos soluções e geramos satisfação!</p>
							</span>
						</div>
					</div>

					<div class="bolinha"></div>

					<div class="secao2 secaoLinhaDoTempo ">
						<p class="tituloSecao2LinhadoTempo">Missão - Visão - Valores </p>

						<span class="textoSecao2LinhadoTempo">

							<p>MISSÃO<br>Ser referência no desenvolvimento de soluções personalizadas, atuando sempre de maneira assertiva juntamente com tecnologia de ponta para os mais variados segmentos do mercado.</p><br>

							<p>VISÃO<br>"Ser a melhor escolha entre as principais empresas do mercado, entregando soluções que geram satisfação."</p><br>

							<p>VALORES<br>
								• Satisfação do cliente;<br>
								• Responsabilidade social;<br>
								• Ética;<br>
								• Confiabilidade;<br>
								• Inovação;<br>
								• Maximização dos recursos;<br>
								• Paixão pelo que faz, sempre aberto às evoluções;<br>
								• Conceito de equipe, sempre pronta a satisfazer o cliente.</p>




						</span>
					</div>

					<div class="bolinha"></div>

					<div class="secao3 secaoLinhaDoTempo">
						<img class="lTempoImg3" src="images/linha-do-tempo/10.jpg" alt="Linha do tempo" title="Enllevo - Soluções que Geram Satisfação">
						<div>
							<p class="tituloSecao3LinhadoTempo">Enllevo 2019 - Parcerias</p>
							<span class="textoSecao3LinhadoTempo">

								<p>A Enllevo desenvolveu um pacote de soluções logísticas para uso no Brasil, chamado de Enllevo Logística, devido à aliança firmada com a Quantum, empresa com atuação exclusiva neste setor, com grande experiência operacional e estratégica.</p><br>

								<p>Para a Enllevo, não se trata de mais uma parceria, mas sim de uma aliança  estratégica que objetiva integrar as forças de ambas empresas e, desta forma,  contribuir para que seus clientes consigam atender aos desafios impostos por um mercado que ainda apresenta muitos entraves. Enquanto a Enllevo entra com sua expertise em TI no mercado brasileiro, a solução da Quantum apoiará a estratégia da Enllevo a partir do seu reconhecimento no segmento logístico europeu, que apresenta uma matriz complexa devido às operações multinacionais.</p><br>

								<p>A Quantum é uma empresa europeia que se destaca pela competência em todas as etapas da cadeia logística, com atuação global e mais de vinte anos de história. O núcleo desta oferta são os sistemas logísticos da classe Supply Chain Execution, para o gerenciamento de estruturas complexas de produção, armazenamento e distribuição. Estes sistemas são, de tal maneira flexíveis, que poderão trabalhar integrados com distintos sistemas ERP. Com investimento constante na evolução tecnológica e funcional de seus aplicativos, a Quantum conseguiu alcançar a posição de liderança deste setor no Leste Europeu.</p>
							</span>
						</div>
					</div>
					<!--Bloco 05-->


					<div class="imgEndLinhaDoTempo">
						<img src="images/mobile320/enllevo-logo.png" alt="Marca Enllevo" title="Marca Enllevo">
					</div>
				</aside>
				</div>
			<!--LinhaDoTempo-->

			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
    </body>
</html>
