<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Manufatura - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaManufatura">

					<div class="bannerManufatura">

					</div>

					<section class="bannerSolucaoManufatura">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-manufatura') }}" class="destaqueAzul">manufatura</a>
							</span>
						</div>
							<span class="iconeSegManufatura icone08"></span>
							<h1 class="tituloManufatura">Solução Manufatura</h1>
							<h2 class="textIntroManufatura">Solução e funcionalidades avançadas para a gestão de sua indústria ou empresa </h2>
							<h3 class="contTextIntroManufatura">Possui funcionalidades exclusivas e essenciais para a gestão de sua empresa e a agilidade tornando-se o ERP de Manufatura mais completo.</h3>
					</section>
					</div>

					<div>
						<img class="imgManufatura" src="images/solucoes/Manufatura/manufatura-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaManufatura">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesManufatura">CONHEÇA NOSSAS SOLUÇÕES PARA MANUFATURA</p>
						<hr>
					</aside>

					<div class="divSolucoesManufatura">
						<ul>
							<li><a href="#veiculoComercial"><h1>MES</h1></a></li>
							<li><a href="#tratores"><h1>APS</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosManufatura segmentoManufatura" id="veiculoComercial">
						<div class="introManufatura">
							<h1>MES |</h1><h1 class="segManufatura">MÓDULO</h1>

							<h2>Enllevo MES - <strong>Manufacturing Execution System</strong></h2>
						</div>
						<hr class="hrManufatura">

						<div class="descricaoManufatura">
							<p>A Solução da Enllevo para Manufatura MES (Manufacturing Execution System), especializada, moderna e eficiente com gestão de tarefas, estoques locais e qualidade da produção.</p>

							<p>Sistema de supevisão do processo de produção onde substitui todas aplicações não-padrão, porporcionando muitas funcionalidades adicionais.  E mais, ele apoia o fluxo de informações no chão-de-fábrica ao integrar com outro sistemas da empresa.</p>

							</div>

						<div class="secaoManufatura">
						<ul>
							<div class="gestaoManufatura">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Comunicação e notificação</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Status de qualidade</p>
								</li>

								<li>
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-03.png" alt="Pickings" title="Pickings">
									<p>Relatorios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Reducao de custo de tempo</p>
								</li>

								<li>
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-05.png" alt="Inventários" title="Inventários">
									<p>Monitoramento de tarefas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Lean Manufacturing</p>
								</li>
							
							<li class="liSegLogistica">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-mes-07.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Rastreamento</p>
								</li>

								
							</ul>
						</div>
						<aside class="conteinerBeneficiosManufatura conteinerManufatura">
							<div class="beneficiosManufatura">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Aumento do rendimento de funcionários e máquinas</li>
									<li>Assistência "Lean Manufacturing</li>
									<li>Redução de custos de produção</li>
									<li>Aumento da flexibilidade de produção</li>
									<li>Comunicação com os equipamentos automáticos da produção</li>
									<li>Rastreamento de processos em tempo real</li>
									<li>Resposta rápida em casos de emergência</li>
									<li>Eliminação da circulação de documentos em papel</li>
									<li>Organização de processos e recursos</li>
									<li>Integração com sistemas externos</li>
									<li>Possibilidade de uma modelagem fácil e rápida de processos de negócios</li>
									<li>Interface do usuário moderna e amigável</li>
								</ul>
							</div>
							<div class="caracteristicasManufatura">
								<p>Características do Sistema</p>

								<ul class="caracteristicaManufatura">
									<li class="liNeutra">Gestão de receitas (BOM)</li>
									<li>Rastreabilidade (genealogia) do produto final</li>
									<li>Gestão de processos de negócios no chão de fábrica</li>
									<li>Gestão de ordens de produção</li>
									<li>Criação e atualização automática do cronograma de produção</li>
									<li>Informação quantitativa sobre a produção em curso</li>
									<li>Registro atual e histórico do trabalho das máquinas e pessoas</li>
									<li>Assistência de implementação "Lean Manufacturing</li>
									<li>Assistência no processo de controle de qualidade</li>
									<li>Gravação de acontecimentos do chão de fábrica</li>
									<li>Gestão de lotes e números de série do produto</li>
									<li>Comunicação com sistemas externos</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Bloco Oposto-->
					<section class="segmentosManufatura segmentoManufaturaOposto" id="tratores">
						<div class="introManufaturaOposto">
							<h1 class="h1IntroManufatura">APS |</h1><h1 class="segManufaturaOposto">MÓDULO</h1>

							<h2>Enllevo APS - <strong>Planejamento e Escalonamento Avançado</strong></h2>
						</div>
						<hr class="hrManufatura hrOposto">

						<div class="descricaoManufatura descricaoOposto">
							<p>A Solução da Enllevo para Manufatura APS (Planejamento e Escalonamento Avançado), que permite compor e otimizar planos de produção complexos.</p>

							<p>Suportando gerenciamento e planejador em programação e otimização de produção sofisticada. Redução de custos e otimização da produção e tempo de processos relacionados.</p>

						</div>

						<div class="secaoManufatura secaoOposto">
							<ul>
								<div class="gestaoManufatura gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-aps-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Minimiza erros na entrega</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-aps-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Status de qualidade</p>
								</li>

								<li>
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-aps-03.png" alt="Pickings" title="Pickings">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-aps-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Reducao de custo de tempo</p>
								</li>

								<li>
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-aps-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de servicos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegManufatura" src="images/solucoes/Manufatura/manufatura-aps-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gráfico gantt</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosManufatura conteinerBeneficiosOposto">
							<div class="beneficiosManufatura beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Reduz o lead time e aumenta a produtividade</li>
									<li>Redução das existências</li>
									<li>Minimiza desvios relativamente às condições de entrega</li>
									<li>Reduz tempos de preparação</li>
									<li>Reduz tempos de espera</li>
									<li>Maximiza a utilização dos recursos</li>
									<li>Melhora a qualidade de serviço ao cliente</li>
									<li>Utilização amigável</li>
									<li>Reduz custos</li>
								</ul>
							</div>
							<div class="caracteristicasManufatura caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaManufatura">
									<li class="liNeutra">Gráfico Gantt de recursos</li>
									<li>Gráfico Gantt de ordens de produção</li>
									<li>Planejamento das operações automáticas</li>
									<li>Planejamento das operações manuais</li>
									<li>Gráfico de cargas de recursos</li>
									<li>Avaliação do plano de produção</li>
									<li>Gestão de recursos e calendários</li>
									<li>Gestão do tempo de paradas</li>
								</ul>
							</div>
						</aside>
					</section>

			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeManufatura">
					<div class="textoBackofficeManufatura">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesManufatura">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
