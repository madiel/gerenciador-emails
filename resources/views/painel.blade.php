@extends('layouts.app')

<style>
    .button {
        background-color: #BFBFFF; /* Green */
        border: none;
        color: #1B5385;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        font-family: 'Roboto-Black';
    }

    .button2 {
        background-color: #FFE599;
    }
    .button3 {background-color: #DFFFBF;}

    a.button:hover{
        background-color: #9393ff;
    }
    a.button2:hover{
        background-color: #ffd966;
    }
    a.button3:hover{
        background-color: #BBFF77;
    }

    .button-painel{
        color: inherit;
        font-family: Roboto-Regular, sans-serif;
        font-size: 1em;
        font-weight: 500;
    }

    .font-contato {
        font-family: Roboto-Regular, sans-serif;
        font-size: 0.9em;
        font-weight: 500;
      }

</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div style="text-align: center; font-size: 40px; margin: 4%;">
                        <img src="images/logo-enllevo-home.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
                    </div>
                    <p style="text-align: center; margin: 4%; color: #1B5385;" class="font-contato">Painel Administrativo Site Enllevo:</p>
                    <div style="text-align: center; margin: 10%;">
                        <a style="text-decoration:none" href="{{ route('contato-interno') }}"  class="button-painel button">Contato</a>
                        <a style="text-decoration:none" href="{{ route('trabalhe-interno') }}" class="button-painel button button2">Trabalhe Conosco</a>
                        <a style="text-decoration:none" href="{{ route('excel') }}" class="button-painel button button3">News Letter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
