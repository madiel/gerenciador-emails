<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções - Motores e Máquinas - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionabannerRotativoMaquina">

					<div class="bannerRotativoMaquina">

					</div>

					<section class="bannerSolucaoMaquina">
					<div class="divCaminhoSolucoes">
						<span class="caminhoSolucoes">
                            <a href="{{ route('index') }}">home</a> /
							<a href="{{ route('index') }}">soluções</a> /
							<a href="{{ route('solucao-motores-e-maquinas') }}" class="destaqueAzul">motores e máquinas</a>
						</span>
					</div>
						<span class="iconeSegMotoresMaquinas icone04"></span>
						<h1 class="tituloSolMaquina">Solução Motores e Máquinas</h1>
						<h2 class="textIntroSolMaquina">Conte com a solução certa paratodos os tipos de máquinas e motores.</h2>
						<h3 class="contTextIntroSolMaquina">Conheça o sistema que gerencia todos os processos e decisões. Monitoramento, rastreabilidade, relatórios, modulo financeiro, apontamento de fases, gestão de contratos orçamentais  e muito mais. </h3>

					</section>
					</div>

					<div>
						<img class="imgSolMaquina" src="images/solucoes/Motores-e-Maquinas/motores-e-maquinas.png" alt="Solução Automotivo" title="Enllevo Solução Automotivo">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoMotoresEMaquinas-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaMaquina">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesParaSolMaquina">CONHEÇA NOSSAS SOLUÇÕES PARA MOTORES E MÁQUINAS</p>
						<hr>
					</aside>

					<div class="divSolucoesMaquina">
						<ul>
							<li><a href="#maquinaEquipamento"><h1>MÁQUINAS E EQUIPAMENTOS</h1></a></li>
							<li><a href="#motores"><h1>MOTORES</h1></a></li>
							<li><a href="#paineis"><h1>PAINÉIS</h1></a></li>
							<li><a href="#pontesRolantes"><h1>PONTES ROLANTES</h1></a></li>
							<li><a href="#bombas"><h1>BOMBAS</h1></a></li>
							<li><a href="#empilhadeiras"><h1>EMPILHADEIRAS</h1></a></li>
							<li><a href="#escavadeiras"><h1>ESCAVADEIRAS</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosSolMaquina segmentoMaquina" id="maquinaEquipamento">
						<div class="introMaquina">
							<h1>MÁQUINAS E EQUIPAMENTO |</h1><h1 class="segMaquina">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Máquinas e Equipamento</strong></h2>
						</div>
						<hr class="hrSolMaquina">

						<div class="descricaoSolMaquina">
							<p>A Solução da Enllevo para Máquinas e Equipamento é um sistema de gestão empresarial completo
							para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria de motores, máquinas, paineis, bombas entre outros.</p>

							</div>

						<div class="secaoSolMaquina">

						<ul>
							<div class="gestaoSolMaquina">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-01.png" alt="Integração com as montadoras" title="Integração com as montadorasss">
									<p class="entregasArmazem">Integração com as montadorasss</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-02.png" alt="Multi empresass" title="Multi empresas">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-03.png" alt="Acesso Transacional" title="Acesso Transacional">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-04.png" alt="Gestão em todas as unidades" title="Gestão em todas as unidades">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-05.png" alt="Gestão de vendas" title="Gestão de vendas">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-06.png" alt="Gestão de estoques" title="Gestão de estoques">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-07.png" alt="Gestão de serviços" title="Gestão de serviçosa">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-08.png" alt="Inventario de peças" title="Inventario de peças">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-09.png" alt="Vendas de Máquinas e Equipamentos" title="Vendas de Máquinas e Equipamentos">
									<p class="embRetornaveis">Vendas de Máquinas e Equipamentos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-10.png" alt="Política Comercial" title="Política Comercial">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-11.png" alt="Personalização às necessidades da empresa" title="Personalização às necessidades da empresa">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-12.png" alt="Segurança dos dados" title="Segurança dos dados">
									<p>Segurança dos dados</p>
								</li>
							</ul>
							
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolMaquina caracteristicasBeneficiosSistemaMaquina">
							<div class="beneficiosSistemaSolMaquina">
								
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>
							<div class="caracteristicasSistemasSolMaquina">
								<p>Características do Sistema</p>

								<ul class="caracteristicasMaquina">
									<li class="liCaracteristicasSistemas">Veículos Comerciais</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
						
					<!--Bloco Oposto-->
					<section class="segmentosSolMaquina segmentoMaquinaOposto" id="motores">
						<div class="introMaquinaOposto">
							<h1 class="h1IntroMaquinaOposto">MOTORES |</h1><h1 class="segMaquinaOposto">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Motores</strong></h2>
						</div>
						<hr class="hrSolMaquina hrSolAutoMaquinaOposto">
						
						<div class="descricaoSolMaquina descricaoSolMaquinaOposto">
							<p>A Solução da Enllevo para Motores é um sistema de Gestão empresarial completo
							para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria de motores.</p>

						</div>

						<div class="secaoSolMaquina secaoSolMaquinaOposto">
							
							<ul>
							<div class="gestaoSolMaquina">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-01.png" alt="Integração com as montadoras" title="Integração com as montadorasss">
									<p class="entregasArmazem">Integração com as montadorasss</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-02.png" alt="Multi empresass" title="Multi empresas">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-03.png" alt="Acesso Transacional" title="Acesso Transacional">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-04.png" alt="Gestão em todas as unidades" title="Gestão em todas as unidades">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-05.png" alt="Gestão de vendas" title="Gestão de vendas">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-06.png" alt="Gestão de estoques" title="Gestão de estoques">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-07.png" alt="Gestão de serviços" title="Gestão de serviçosa">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-08.png" alt="Inventario de peças" title="Inventario de peças">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-09.png" alt="Vendas de Máquinas e Equipamentos" title="Vendas de Máquinas e Equipamentos">
									<p class="embRetornaveis">Vendas de Máquinas e Equipamentos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-10.png" alt="Política Comercial" title="Política Comercial">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-11.png" alt="Personalização às necessidades da empresa" title="Personalização às necessidades da empresa">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-12.png" alt="Segurança dos dados" title="Segurança dos dados">
									<p>Segurança dos dados</p>
								</li>
							</ul>
							
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolMaquina caracteristicasBeneficiosSistemaMaquinaOposto">
							<div class="beneficiosSistemaSolMaquina beneficiosSistemaMaquinaOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>
							<div class="caracteristicasSistemasSolMaquina caracteristicasSistemasMaquinaOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolMaquina">
									<li class="liCaracteristicasSistemas">Veículos Comerciais</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->
					<section class="segmentosSolMaquina segmentoMaquina" id="paineis">
						<div class="introMaquina">
							<h1>PAINÉIS |</h1><h1 class="segMaquina">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Painéis</strong></h2>
						</div>
						<hr class="hrSolMaquina">
						
						<div class="descricaoSolMaquina">
							<p>A Solução da Enllevo para Paineis é um sistema de gestão empresarial completo para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao setor, entregando ao cliente o mais completo serviço de gestão, o qual garante segurança e organização dos processos de todos os segmentos da indústria.</p>
						</div>


						<div class="secaoSolMaquina">
							<ul>
								<div class="gestaoSolMaquina">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Logistica/yms-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaMaquina sistemaMaquina3">
							<div class="beneficiosSistemaSolMaquina beneficiosMaquina3">
								<p>Benefícios do Sistema</p>

								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>


							<div class="caracteristicasSistemasSolMaquina caracMaquina3">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAutoMaquina3">
									<li class="liCaracteristicasSistemas">Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>

					<!--Bloco Oposto-->
					<section class="segmentosSolMaquina segmentoMaquinaOposto" id="pontesRolantes">
						<div class="introMaquinaOposto">
							<h1 class="h1IntroMaquinaOposto">PONTES ROLANTES |</h1><h1 class="segMaquinaOposto">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Pontes Rolantes</strong></h2>
						</div>
						<hr class="hrSolMaquina hrSolAutoMaquinaOposto">
						
						<div class="descricaoSolMaquina descricaoSolMaquinaOposto">
							<p>A Solução da Enllevo para Paineis é um sistema de gestão empresarial completo para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao setor, entregando ao cliente o mais completo serviço de gestão, o qual garante segurança e organização dos processos de todos os segmentos da indústria.</p>

						</div>

						<div class="secaoSolMaquina secaoSolMaquinaOposto">
							<ul>
							<div class="gestaoSolMaquina">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-01.png" alt="Integração com as montadoras" title="Integração com as montadorasss">
									<p class="entregasArmazem">Integração com as montadorasss</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-02.png" alt="Multi empresass" title="Multi empresas">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-03.png" alt="Acesso Transacional" title="Acesso Transacional">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-04.png" alt="Gestão em todas as unidades" title="Gestão em todas as unidades">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-05.png" alt="Gestão de vendas" title="Gestão de vendas">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-06.png" alt="Gestão de estoques" title="Gestão de estoques">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-07.png" alt="Gestão de serviços" title="Gestão de serviçosa">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-08.png" alt="Inventario de peças" title="Inventario de peças">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-09.png" alt="Vendas de Máquinas e Equipamentos" title="Vendas de Máquinas e Equipamentos">
									<p class="embRetornaveis">Vendas de Máquinas e Equipamentos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-10.png" alt="Política Comercial" title="Política Comercial">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-11.png" alt="Personalização às necessidades da empresa" title="Personalização às necessidades da empresa">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-12.png" alt="Segurança dos dados" title="Segurança dos dados">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolMaquina caracteristicasBeneficiosSistemaMaquinaOposto">
							<div class="beneficiosSistemaSolMaquina beneficiosSistemaMaquinaOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>
							<div class="caracteristicasSistemasSolMaquina caracteristicasSistemasMaquinaOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolMaquina">
									<li class="liCaracteristicasSistemas">Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->

					<section class="segmentosSolMaquina segmentoMaquina" id="bombas">
						<div class="introMaquina">
							<h1>BOMBAS |</h1><h1 class="segMaquina">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Bombas</strong></h2>
						</div>
						<hr class="hrSolMaquina">
						
						<div class="descricaoSolMaquina">
							<p>A Solução da Enllevo para Paineis é um sistema de gestão empresarial completo para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao setor, entregando ao cliente o mais completo serviço de gestão, o qual garante segurança e organização dos processos de todos os segmentos da indústria.</p>
						</div>


						<div class="secaoSolMaquina">
							<ul>
							<div class="gestaoSolMaquina">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-01.png" alt="Integração com as montadoras" title="Integração com as montadorasss">
									<p class="entregasArmazem">Integração com as montadorasss</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-02.png" alt="Multi empresass" title="Multi empresas">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-03.png" alt="Acesso Transacional" title="Acesso Transacional">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-04.png" alt="Gestão em todas as unidades" title="Gestão em todas as unidades">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-05.png" alt="Gestão de vendas" title="Gestão de vendas">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-06.png" alt="Gestão de estoques" title="Gestão de estoques">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-07.png" alt="Gestão de serviços" title="Gestão de serviçosa">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-08.png" alt="Inventario de peças" title="Inventario de peças">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-09.png" alt="Vendas de Máquinas e Equipamentos" title="Vendas de Máquinas e Equipamentos">
									<p class="embRetornaveis">Vendas de Máquinas e Equipamentos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-10.png" alt="Política Comercial" title="Política Comercial">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-11.png" alt="Personalização às necessidades da empresa" title="Personalização às necessidades da empresa">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-12.png" alt="Segurança dos dados" title="Segurança dos dados">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaMaquina sistemaMaquina3">
							<div class="beneficiosSistemaSolMaquina beneficiosMaquina3">
								<p>Benefícios do Sistema</p>

								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>


							<div class="caracteristicasSistemasSolMaquina caracMaquina3">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAutoMaquina3">
									<li class="liCaracteristicasSistemas">Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>

					<!--Bloco Oposto-->
					<section class="segmentosSolMaquina segmentoMaquinaOposto" id="empilhadeiras">
						<div class="introMaquinaOposto">
							<h1 class="h1IntroMaquinaOposto">EMPILHADEIRAS |</h1><h1 class="segMaquinaOposto">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Empilhadeiras</strong></h2>
						</div>
						<hr class="hrSolMaquina hrSolAutoMaquinaOposto">

						<div class="descricaoSolMaquina descricaoSolMaquinaOposto">
							<p>A Solução da Enllevo para Paineis é um sistema de gestão empresarial completo para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao setor, entregando ao cliente o mais completo serviço de gestão, o qual garante segurança e organização dos processos de todos os segmentos da indústria.</p>

						</div>

						<div class="secaoSolMaquina secaoSolMaquinaOposto">
							<ul>
							<div class="gestaoSolMaquina">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-01.png" alt="Integração com as montadoras" title="Integração com as montadorasss">
									<p class="entregasArmazem">Integração com as montadorasss</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-02.png" alt="Multi empresass" title="Multi empresas">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-03.png" alt="Acesso Transacional" title="Acesso Transacional">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-04.png" alt="Gestão em todas as unidades" title="Gestão em todas as unidades">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-05.png" alt="Gestão de vendas" title="Gestão de vendas">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-06.png" alt="Gestão de estoques" title="Gestão de estoques">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-07.png" alt="Gestão de serviços" title="Gestão de serviçosa">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-08.png" alt="Inventario de peças" title="Inventario de peças">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-09.png" alt="Vendas de Máquinas e Equipamentos" title="Vendas de Máquinas e Equipamentos">
									<p class="embRetornaveis">Vendas de Máquinas e Equipamentos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-10.png" alt="Política Comercial" title="Política Comercial">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-11.png" alt="Personalização às necessidades da empresa" title="Personalização às necessidades da empresa">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-12.png" alt="Segurança dos dados" title="Segurança dos dados">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaSolMaquina caracteristicasBeneficiosSistemaMaquinaOposto">
							<div class="beneficiosSistemaSolMaquina beneficiosSistemaMaquinaOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>
							<div class="caracteristicasSistemasSolMaquina caracteristicasSistemasMaquinaOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolMaquina">
									<li class="liCaracteristicasSistemas">Empilhadeiras</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>
					<!--Final Bloco Oposto-->

					<section class="segmentosSolMaquina segmentoMaquina" id="escavadeiras">
						<div class="introMaquina">
							<h1>ESCAVADEIRAS |</h1><h1 class="segMaquina">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de Gestão Escavadeiras</strong></h2>
						</div>
						<hr class="hrSolMaquina">
						
						<div class="descricaoSolMaquina">
							<p>A Solução da Enllevo para Paineis é um sistema de gestão empresarial completo para o controle ideal de todas as fases.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao setor, entregando ao cliente o mais completo serviço de gestão, o qual garante segurança e organização dos processos de todos os segmentos da indústria.</p>
						</div>


						<div class="secaoSolMaquina">
							<ul>
							<div class="gestaoSolMaquina">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-01.png" alt="Integração com as montadoras" title="Integração com as montadorasss">
									<p class="entregasArmazem">Integração com as montadorasss</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-02.png" alt="Multi empresass" title="Multi empresas">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-03.png" alt="Acesso Transacional" title="Acesso Transacional">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-04.png" alt="Gestão em todas as unidades" title="Gestão em todas as unidades">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-05.png" alt="Gestão de vendas" title="Gestão de vendas">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-06.png" alt="Gestão de estoques" title="Gestão de estoques">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-07.png" alt="Gestão de serviços" title="Gestão de serviçosa">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-08.png" alt="Inventario de peças" title="Inventario de peças">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-09.png" alt="Vendas de Máquinas e Equipamentos" title="Vendas de Máquinas e Equipamentos">
									<p class="embRetornaveis">Vendas de Máquinas e Equipamentos</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-10.png" alt="Política Comercial" title="Política Comercial">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-11.png" alt="Personalização às necessidades da empresa" title="Personalização às necessidades da empresa">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegMaquina" src="images/solucoes/Motores-e-Maquinas/motores-maquina-12.png" alt="Segurança dos dados" title="Segurança dos dados">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="caracteristicasBeneficiosSistemaMaquina sistemaMaquina3">
							<div class="beneficiosSistemaSolMaquina beneficiosMaquina3">
								<p>Benefícios do Sistema</p>

								<ul>
									<li class="liBeneficiosSistema">Integração total com as montadoras</li>
									<li>Gestão em todas as unidades</li>
									<li>Controle de visitas externas (Sistematica de Vendas)</li>
									<li>Controle orçamentário de todas as áreas</li>
									<li>Gestão de vendas, estoques, serviços e clientes</li>
									<li>Funcionalidades da concessionária</li>
									<li>Mapas gerenciais</li>
									<li>Soluções customizadas</li>
									<li>Gestão completa dos processos produtivos</li>
									<li>Agendamento de serviços</li>
									<li>Inventário de peças</li>
									<li>Gestão completa de creditos dos clientes</li>
									<li>Controle de  descontos pela condição de pagmento/tipo de produto</li>
									<li>Vendas de Máquinas e Equipamentos</li>
									<li>Adaptação do software às necessidades da empresa</li>
								</ul>
							</div>


							<div class="caracteristicasSistemasSolMaquina caracMaquina3">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolAutoMaquina3">
									<li class="liCaracteristicasSistemas">Escavadeiras</li>
									<li>Peças e Oficinas</li>
									<li>Visitas Externas</li>
									<li>Política Comercial</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Integração contabil e fiscal</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Retifica</li>
									<li>Relatório parametrizado de contratos</li>
									<li>Módulos de gerenciamento de RH, Folha de Pagamento e E-social.</li>
									<li>Planejamento eficaz dos setores de produção e montagem</li>
									<li>Maior segurança dos dados da empresa</li>
								</ul>
							</div>
						</aside>
					</section>

			<!--SoluçãoMotoresEMaquinas-->


			<!--Backoffice-->
				<aside class="backofficeMaquina">
					<div class="textoBackofficeMaquina">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesMaquina">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
