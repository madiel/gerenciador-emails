<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Consultoria Empresarial - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaConsult">

					<div class="bannerConsult">

					</div>

					<section class="bannerSolucaoConsult">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">serviços</a> /
								<a href="{{ route('consultoria-empresarial') }}" class="destaqueAzul">consultoria empresarial</a>
							</span>
						</div>
							<span class="iconeSevConsultoria icone16"></span>
							<h1 class="tituloConsult">Consultoria Empresarial</h1>
							<h2 class="textIntroConsult">Entrega incremental em todas as versões: os módulos do sistema que são testados e entregues ao cliente a medida que ficarem prontos, para evitar que alguma
							falha contamine o projeto inteiro e ele precise ser refeito por completo. </h2>
							<h3 class="contTextIntroConsult">...</h3>
					</section>
					</div>

					<div>
						<img class="imgConsult" src="images/solucoes/Consultoria-Empresarial/Consultoria-Empresarial.png" alt="Consultoria Empresarial" title="Enllevo Consultoria Empresarial">
					</div>
			<!--Cabeçalho-->

			<!--ConsultoriaEmpresarial-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaConsult" >
								<h2 class="h2PropSolucao">ficou interessado no serviço?</h2>
								<h2 class="h2Proposta">peça uma proposta sem compromisso.</h2>
							</div>
						</a>
						<p class="solucoesConsult">conheça nosso serviço de consultoria empresarial</p>
						<hr>
					</aside>

					<div class="divSolucoesConsult">
						<ul>
							<li><a href="#governanca"><h1>governança de TI</h1></a></li>
							<li><a href="#consultoria"><h1>consultoria tributaria e fiscal</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosConsult segmentoConsult" id="governanca">
						<div class="introConsult">
							<h1>Governança de TI |</h1><h1 class="segConsult">Governança de TI</h1>

							<h2>Enllevo MES - <strong>Manufacturing Execution System</strong></h2>
						</div>
						<hr class="hrConsult">
						<img class="imgGestaoConsult" src="images/solucoes/Automotivo/veiculos-de-passeio-01.png" alt="" title="">

						<div class="descricaoConsult">
							<p>A Solução Enllevo para Governança de TI oferece processos otimizados para o planejamento estratégico de TI de sua empresa, 
							com foco nas necessidades do seu negócio e alcance de metas e objetivos.</p>

							<p>A Enllevo utiliza a metodologia CobiT (Control Objectives for Information and related Technology).
							 O CobiT é um guia com boas práticas para controle e otimização dos investimentos de TI.</p>

							</div>

						<div class="secaoConsult">
						<ul>
							<div class="gestaoConsult">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="entregasArmazem">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="identificacaoAuto">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p class="embRetornaveis">...</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="" title="">
									<p>...</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosConsult conteinerConsult">
							<div class="beneficiosConsult">
								<p>Benefícios do Serviço</p>
								<ul>
									<li class="liNeutra">Alinhamento estratégico</li>
									<li>Entrega de valor</li>
									<li>Gestão de recursos</li>
									<li>Gestão de riscos</li>
									<li>Mensuração de desempenho</li>
								</ul>
							</div>
							<div class="caracteristicasConsult">
								<p>Características do Serviço</p>
								<ul class="caracteristicaConsult">
									<li class="liNeutra">Estabelece conexão entre os objetivos do negócio e de TI a partir do desenho 
									e validação da proposta de valor.</li>

									<li>Otimização de custos e entrega os benefícios previstos no planejamento estratégico.</li>

									<li>Avaliação e aplicação das melhores opções de investimento e gerenciamento dos recursos, 
									no que abrange conhecimento e infraestrutura.</li>

									<li>Entendimento dos níveis de disposição de risco da empresa e transparência, requerimentos de 
									conformidade e gestão dos riscos nas atividades da corporação.</li>

									<li>Supervisão e monitoramento de projetos, da implantação até a conclusão, e serviços, 
									controlando a performance com base em indicadores que traduzem as estratégias em planos de ação.</li>
								</ul>
							</div>
						</aside>
					</section>

					<!--Bloco Oposto-->
					<section class="segmentosConsult segmentoConsultOposto" id="consultoria">
						<div class="introConsultOposto">
							<h1 class="h1IntroConsult">consultoria tributária e fiscal |</h1><h1 class="segConsultOposto">módulo</h1>

							<h2>Enllevo APS - <strong>Planejamento e Escalonamento Avançado</strong></h2>
						</div>
						<hr class="hrConsult hrOposto">

						<div class="descricaoConsult descricaoOposto">
							<p>Cumprir com as obrigações tributarias e fiscais das operações da sua empresa pode se tornar confuso e complicado, 
								representando riscos, demandando mais recursos e diminuindo a eficiência de seus negócios.</p>

							<p>A Enllevo pode ajudar a sua empresa a otimizar seus processos de apuração e recolhimento de tributos diretos e indiretos 
								(IRPJ, CSLL, PIS, COFINS, ICMS, IPI e ISSQN). A Consultoria Tributária e Fiscal da Enllevo oferece uma analise completa 
								e aprofundada das características e necessidades tributárias e fiscais do seu negócio, visando sempre identificar oportunidades, 
								organizar o planejamento tributário e reduzir riscos e custos.</p>

						</div>

						<div class="secaoConsult secaoOposto">
							<ul>
								<div class="gestaoConsult gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegConsult" src="images/solucoes/provisorio.png" alt="Enllevo - Gestão de custos de transportadoras" title="Gestão de custos de transportadoras">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosConsult conteinerBeneficiosOposto">
							<div class="beneficiosConsult beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Redução de riscos de autuação fiscal</li>
									<li>Redução de pagamentos indevidos</li>
								</ul>
							</div>
							<div class="caracteristicasConsult caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicaConsult">
									<li class="liNeutra">Planejamento tributário</li>
									<li>Diagnóstico de procedimentos fiscais</li>
									<li>Diagnóstico de carga tributária</li>
									<li>Revisões fiscais periódicas</li>
									<li>Consultoria sobre preços de transferências</li>
									<li>Consultoria fiscal contínua</li>
									<li>Consultoria a expatriados</li>
									<li>Recuperação de créditos fiscais</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--ConsultoriaEmpresarial-->


			<!--Backoffice-->
				<aside class="backofficeConsult">
					<div class="textoBackofficeConsult">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesConsult">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
