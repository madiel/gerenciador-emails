<!DOCTYPE html>
<html lang="pt-br">
<meta charset="utf-8">
    <head>
        <title>Enllevo - Soluções que Geram Satisfação</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		

		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS Slick Carrosel -->
		<link rel="stylesheet" type="text/css" href="./plugins/slick/slick.css">
		<link rel="stylesheet" type="text/css" href="./plugins/slick/slick-theme.css">
		<!-- CSS Slick Carrosel -->

		<!-- CSS owl.carousel js -->
		<link rel="stylesheet" href="./plugins/OwlCarousel2-2.3.4/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="./plugins/OwlCarousel2-2.3.4/assets/owl.theme.default.min.css">
		<!-- CSS owl.carousel js -->

		<!-- CSS Swiper's CSS -->
		<link rel="stylesheet" href="./plugins/swiper/css/swiper.min.css"><!-- novo Carrossel -->
		<!-- CSS Swiper's CSS -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

		<!-- sweetalert -->
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<!-- sweetalert -->

		
    </head>
    <body>
		<div class="geralIndex">
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- Banner Imagem/Vídeo >

				<div class="conteudoVideoImagem">
					<div class="slickVideoEnllevo slider">
						<div>
							<video id="videoEnllevo" class="midia" autoplay loop poster="images/mobile320/further-rio-800x800.jpg">
								<source src="musicaRio.mp4" type="video/mp4">
							</video>
						</div>
						<div>
							<img src="images/mobile320/further-rio-800x800.jpg" alt="Marca Enllevo" title="Marca Enllevo">
							<div class="labelVideoImagem">
								<h2 class="tituloLabelConfianca">Tudo que fazemos envolve confiança.</h2>
								<span class="tituloLabelSatisfacao">Soluções que geram satisfação</span>
							</div>
						</div>
					</div>
				</div>
				<!-- Banner Imagem/Vídeo -->

				<div class="col-md-offset-2 col-md-8">
						@include('sweet::alert')
				</div>

				<!-- Banner Imagem/Video -->

				<div class="conteudoVideoImagem">
					<div class="owl-carousel-videoEnllevo owl-carousel owl-theme slickVideoEnllevo">
						<!--div>
                            <div class="labelVideoImagem">
                                <h2 class="tituloLabelConfianca">Tudo que fazemos envolve confiança.</h2>
                                <span class="tituloLabelSatisfacao">Soluções que geram satisfação</span>
                            </div>
							<video id="videoEnllevo" class="midia" autoplay loop poster="musica.gif">
								<source src="musicaRio.mp4" type="video/mp4">
							</video>
						</div-->
						<div class="slider-banner">
							<img src="images/banners/enllevo-banner-04.jpg" alt="Marca Enllevo" title="Marca Enllevo">
							<div class="labelVideoImagem">
								<h2 class="tituloLabelConfianca">Tudo que fazemos envolve confiança.</h2>
								<span class="tituloLabelSatisfacao">Soluções que<br/>geram satisfação</span>
							</div>
						</div>

						<div class="slider-banner">
							<img class="imgGifBanner" src="images/banners/enllevo02.gif" alt="Marca Enllevo" title="Marca Enllevo">
							<div class="labelVideoImagem">
								<h2 class="tituloLabelConfianca">Entregamos mais do que linha de código.</h2>
								<span class="tituloLabelSatisfacao">Soluções que<br/>geram satisfação</span>
							</div>
						</div>

						<!--div class="slider-banner">
							<img class="imgGifBanner" src="images/banners/Video Enllevo-00.gif" alt="Marca Enllevo" title="Marca Enllevo">
						</div-->

						<div class="slider-banner">
							<img src="images/banners/Banner-03-enllevo.jpg" alt="Marca Enllevo" title="Marca Enllevo">
							<div class="labelVideoImagem">
								<h2 class="tituloLabelBanner03">CONHEÇA A SOLUÇÃO DE LOGÍSTICA E GARANTA MAIS EFICIÊNCIA E TECNOLOGIA NO PROCESSO LOGÍSTICO.</h2>
								<span class="textoLabelBanner03">SOLUÇÕES SOB MEDIDA COM A QUALIDADE DA ENLLEVO</span>
								<div class="botaoBanner03">

									<a href="{{ route('solucao-logistica') }}">CONHEÇA A SOLUÇÃO</a>
								</div>
							</div>
						</div>
					</div>
						<div class="carregando">
							<div></div><div></div><div></div><div></div>
						</div>
				</div>
				<!-- Banner Imagem/Video -->

				<!-- Informacoes empresa -->
			<div class="backgroundInfoERP">
				<div class="infoEmpresa">
					<div class="caixaInfoNegocios">
						<div class="caixaIconeInfo">
							<img class="imgIconeInfo" src="images/destaque-01.png" title="Consultoria de Negócios - Enllevo" alt="icone">
						</div>

						<div class="caixaTextoInfo">
							<h2 class="tituloTextoInfo">Solução de Logística</h2>
							<p class="paragrafoTextoInfo">
								Conte com soluções de ponta da Enllevo e garanta mais eficiência e tecnologia no processo logístico para entregar resultados.
							</p>
						</div>
					</div>
					<div class="caixaImagensInfoNegocios invisivel">
						<div class="slickNegocios slider">

							<!-- Destaque 01 -->
							<div>
								<img src="images/mobile320/img-tela01.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								<span class="botaoPopupInfoEmpresa iconeBalaozinho" onclick="abrirPopupInfoEmpresa(event)"></span>
								<div class="backgroundPopupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)"></div>
								<section class="popupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)">
									<h4 class="tituloPopup">Solução de Logística</h4>
									<p class="paragrafoPopupInfoEmpresa">
										Conte com soluções de ponta da Enllevo e garanta mais eficiência e tecnologia no processo logístico para entregar resultados.
									</p>
									<div class="rodapePopup">
										<a href="#" class="botao botaoConhecaSolucao">Conheça nossa solução</a>
										<span class="iconeBalaozinho"></span>
									</div>
								</section>
							</div>
							<div>
								<img src="images/mobile320/img-tela02.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								<span class="botaoPopupInfoEmpresa iconeBalaozinho" onclick="abrirPopupInfoEmpresa(event)"></span>
								<div class="backgroundPopupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)"></div>
								<section class="popupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)">
									<h4 class="tituloPopup">Solução de Logística</h4>
									<p class="paragrafoPopupInfoEmpresa">
										Conte com soluções de ponta da Enllevo e garanta mais eficiência e tecnologia no processo logístico para entregar resultados.
									</p>
									<div class="rodapePopup">
										<a href="#" class="botao botaoConhecaSolucao">Conheça nossa solução</a>
										<span class="iconeBalaozinho"></span>
									</div>
								</section>
							</div>
						</div>
					</div>

					<!-- Destaque 02 -->
					<div class="caixaInfoEmpresarial">
						<div class="caixaIconeInfo">
							<img class="imgIconeInfo" src="images/destaque-02.png" title="ERP Gestao Empresarial - Enllevo" alt="icone">
						</div>
						<div class="caixaTextoInfo">
							<h2 class="tituloTextoInfo">Solução Automotiva</h2>
							<p class="paragrafoTextoInfo">
								Solução certa que atende às particularidades do segmento, desde o controle de mapas de produção até indicadores automatizados de qualidade.
							</p>
						</div>
					</div>
					<div class="caixaImagensEmpresarial invisivel">
						<div class="slickEmpresarial slider">
							<div>
								<img src="images/mobile320/img-tela01.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								<span class="botaoPopupInfoEmpresa iconeBalaozinho" onclick="abrirPopupInfoEmpresa(event)"></span>
								<div class="backgroundPopupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)"></div>
								<section class="popupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)">
									<h4 class="tituloPopup">Solução Automotiva</h4>
									<p class="paragrafoPopupInfoEmpresa">
										Solução certa que atende às particularidades do segmento, desde o controle de mapas de produção até indicadores automatizados de qualidade.
									</p>
									<div class="rodapePopup">
										<a href="#" class="botao botaoConhecaSolucao">Conheça nossa solução</a>
										<span class="iconeBalaozinho"></span>
									</div>
								</section>
							</div>
							<div>
								<img src="images/mobile320/img-tela02.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								<span class="botaoPopupInfoEmpresa iconeBalaozinho" onclick="abrirPopupInfoEmpresa(event)"></span>
								<div class="backgroundPopupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)"></div>
								<section class="popupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)">
									<h4 class="tituloPopup">Solução Automotiva</h4>
									<p class="paragrafoPopupInfoEmpresa">
										Solução certa que atende às particularidades do segmento, desde o controle de mapas de produção até indicadores automatizados de qualidade.
									</p>
									<div class="rodapePopup">
										<a href="#" class="botao botaoConhecaSolucao">Conheça nossa solução</a>
										<span class="iconeBalaozinho"></span>
									</div>
								</section>
							</div>
						</div>
					</div>

					<!-- Destaque 03 -->
					<div class="caixaInfoGestao">
						<div class="caixaIconeInfo">
							<img class="imgIconeInfo" src="images/destaque-03.png" title="HCM Enllevo" alt="icone">
						</div>
						<div class="caixaTextoInfo">
							<h2 class="tituloTextoInfo">Desenvolvimento de Software</h2>
							<p class="paragrafoTextoInfo">
								Entregamos mais do que linhas de código. Desenvolvido sob medida, personalizado, usabilidade, experiencia do usuario e muito mais.
							</p>
						</div>
					</div>
					<div class="caixaImagensGestao invisivel">
						<div class="slickGestao slider">
							<div>
								<img src="images/mobile320/img-tela01.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								<span class="botaoPopupInfoEmpresa iconeBalaozinho" onclick="abrirPopupInfoEmpresa(event)"></span>
								<div class="backgroundPopupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)"></div>
								<section class="popupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)">
									<h4 class="tituloPopup">Desenvolvimento de Software</h4>
									<p class="paragrafoPopupInfoEmpresa">
										Entregamos mais do que linhas de código. Desenvolvido sob medida, personalizado, usabilidade, experiencia do usuario e muito mais.
									</p>
									<div class="rodapePopup">
										<a href="#" class="botao botaoConhecaSolucao">Conheça nossa solução</a>
										<span class="iconeBalaozinho"></span>
									</div>
								</section>
							</div>
							<div>
								<img src="images/mobile320/img-tela02.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								<span class="botaoPopupInfoEmpresa iconeBalaozinho" onclick="abrirPopupInfoEmpresa(event)"></span>
								<div class="backgroundPopupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)"></div>
								<section class="popupInfoEmpresa invisivel" onclick="fecharPopupInfoEmpresa(event)">
									<h4 class="tituloPopup">Desenvolvimento de Software</h4>
									<p class="paragrafoPopupInfoEmpresa">
										Entregamos mais do que linhas de código. Desenvolvido sob medida, personalizado, usabilidade, experiencia do usuario e muito mais.
									</p>
									<div class="rodapePopup">
										<a href="#" class="botao botaoConhecaSolucao">Conheça nossa solução</a>
										<span class="iconeBalaozinho"></span>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
				<!-- Informacoes empresa -->

				<!--Carrocel 01 "Descktop"-->
				<div class="sistemaERP">
					<div class="conteudoConsultoriaNegocios conteudo">
						<h1 class="tituloSistemaERP">Soluções de Logística para WMS, TMS e YMS</h1>
						 <div class="container" id="container">
							<div class="caixaZIndexCarrossel">
								<ul>
									<li>
										<img src="images/carrossel/enllevo-logistica-tms.jpg" alt="enllevo-software" />
									</li>
									<li>
										<img src="images/carrossel/enllevo-logistica-wms.jpg" alt="enllevo-software" />
									</li>
									<li>
										<img src="images/carrossel/enllevo-logistica-wms-02.jpg" alt="enllevo-software" />
									</li>
									<span class="setaBannerEsquerda">
										<img src="images/seta-banner-esquerda.png" class="left">
									</span>
									<span class="setaBannerDireita">
										<img src="images/seta-banner-direita.png" class="right">
									</span>
								</ul>
							</div>
							<div class="caixaZIndexBotao">
								<hr class="HRSistemaERP"/>
								<a href="{{ route('solucao-logistica') }}" class="botao botaoConhecaSolucao">CONHEÇA NOSSA SOLUÇÃO</a>
							</div>
						</div>
						<h3 class="h3SistemaERP">Conte com soluções de ponta da Enllevo e garanta mais eficiência e tecnologia no processo logístico para entregar resultados.Conheça o sistema que oferece controle total sobre cada etapa da operação
						logística, da entrada de matérias-primas até a expedição dos produtos.</h3>
					</div>

				<!--Carrocel 02 "Descktop"-->
				<div class="conteudoERPGestaoEmpresarial conteudo invisivel">
						<h1 class="tituloSistemaERP">Solução Automotiva Completa e Eficiente</h1>
						 <div class="container" id="container2">
							 <div class="caixaZIndexCarrossel">
								<ul>
									<li>
										<img src="images/carrossel/enllevo-software01.png" alt="enllevo-software" />
									</li>
									<li>
										<img src="images/carrossel/enllevo-software02.png" alt="enllevo-software" />
									</li>
									<li>
										<img src="images/carrossel/enllevo-software03.png" alt="enllevo-software" />
									</li>
									<span class="setaBannerEsquerda">
										<img src="images/seta-banner-esquerda.png" class="left">
									</span>
									<span class="setaBannerDireita">
										<img src="images/seta-banner-direita.png" class="right">
									</span>
								</ul>
							</div>
							<div class="caixaZIndexBotao">
								<hr class="HRSistemaERP"/>
								<a href="{{ route('solucao-automotivo') }}" class="botao botaoConhecaSolucao">CONHEÇA NOSSA SOLUÇÃO</a>
							</div>
						</div>
						<h3 class="h3SistemaERP">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores, envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</div>

					<!--Carrocel 03 "Descktop"-->
					<div class="conteudoGestaoPessoas conteudo invisivel">
						<h1 class="tituloSistemaERP">Desenvolvimento de Software</h1>
						 <div class="container" id="container3">
							 <div class="caixaZIndexCarrossel">
								<ul>
									<li>
										<img src="images/carrossel/enllevo-software01.png" alt="enllevo-software" />
									</li>
									<li>
										<img src="images/carrossel/enllevo-software02.png" alt="enllevo-software" />
									</li>
									<li>
										<img src="images/carrossel/enllevo-software03.png" alt="enllevo-software" />
									</li>
									<span class="setaBannerEsquerda">
										<img src="images/seta-banner-esquerda.png" class="left">
									</span>
									<span class="setaBannerDireita">
										<img src="images/seta-banner-direita.png" class="right">
									</span>
								</ul>
							</div>
							<div class="caixaZIndexBotao">
								<hr class="HRSistemaERP"/>
								<a href="{{ route('desevolvimento-de-software') }}" class="botao botaoConhecaSolucao">CONHEÇA NOSSA SOLUÇÃO</a>
							</div>
						</div>
						<h3 class="h3SistemaERP">Entregamos mais do que linhas de código. Desenvolvido sob medida, personalizado, usabilidade, experiencia do usuario e muito mais.</h3>
					</div>

					<div class="modalCarrossel">
						<img class="imgModal" src="" alt="" />
					</div>
				</div>
			</div>
				<!--Slider Sistema ERP-->

				<!-- Historia Enllevo -->
				<!--div class="historiaEnllevo1024"-->
					<div class="informacoesEnllevo">

						<div>
							<h1 class="tituloEnllevo">enllevo</h1>
						</div>
						
							<div class="caixa-carrossel-home"><!-- nova div -->
							<div class="slickEnllevo slider imgSliderEnllevo">
								<div>
									<img src="images/enllevo-empresa-software01.png" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								</div>
								<div>
									<img src="images/enllevo-empresa-software02.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								</div>
								<div>
									<img src="images/enllevo-empresa-software03.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
								</div>
							</div>
							<div class="caixaSwiperCarrossel"><!-- novo Carrossel -->
								<div class="swiper-enllevo swiper-container">
									<div class="swiper-wrapper">
									  <div class="swiper-slide">
										<img src="images/enllevo-empresa-software01.png" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
									  </div>
									  <div class="swiper-slide">
										<img src="images/enllevo-empresa-software02.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
									  </div>
									  <div class="swiper-slide">
										<img src="images/enllevo-empresa-software03.jpg" alt="Enllevo" title="Enllevo - Soluções que Geram Satisfação">
									  </div>
									</div>
								</div>
								<!-- Add Pagination -->
								<div class="swiper-pagination"></div>
							</div>
							<div class="historiaEmpresa">
								<h2 class="subTituloEnllevo">A enllevo hoje...</h2>
								<p class="paragrafo">
									No mercado há mais de 28 anos, a Shapeness é uma empresa caracterizada por estratégias de forte investimento
									em pesquisa e desenvolvimento de soluções de gestão empresarial.
								</p>
								<p class="paragrafo ultimoParagrafo">
									Desenvolvedora de soluções de ERP - Gestor Empresarial, fornecemos um conjunto completo de gestão que se adapta
									de forma rápida e segura aos segmentos Industrial, Distribuição, Serviços e Automotivo, proporcionando a estes uma gestão eficiente.
								</p>
								<!--a href="#" class="conhecaMais botaoConhecaHistoria1024" title="Conheça um pouco mais">Conheça um pouco mais</a-->
								<a href="{{ route('institucional') }}" class="botaoHistoriaEnllevo" title="Conheça um pouco mais"> Conheça um pouco mais</a>
							</div>
						</div>
					</div>



						<div class="nuvemTags noneNuvemTags">
							<div class="caixaTags">
								<h3 class="tagEnllevoConfianca tagNuvem" onClick="trocaCor()">Confiança</h3>
								<h4 class="tagEnllevoContentamento tagNuvem" onClick="trocaCor()">Contentamento</h4>
								<h4 class="tagEnllevoExperiencia tagNuvem" onClick="trocaCor()">Experiência</h4>
								<h3 class="tagEnllevoAgilidade tagNuvem" onClick="trocaCor()">Agilidade</h3>
								<h4 class="tagEnllevoSolucao tagNuvem" onClick="trocaCor()">Solução</h4>
								<h3 class="tagEnllevoExtase tagNuvem" onClick="trocaCor()">Êxtase</h3>
							</div>
							<a href="#" class="botao conhecaMais botaoConhecaMais1024" title="Conheça um pouco mais">Conheça um pouco mais</a>

							<div class="EnllevoByShapeness">
								<a href="{{ route('erro') }}" class="imgEnllevoByShapeness">
									<img src="images/enllevo-by-shapeness-marca.png" alt="Enllevo By Shapeness" title="Enllevo By Shapeness">
								</a>
							</div>
						</div>

				<!--/div-->
				<!-- Historia Enllevo -->
				<!--Nossos Clientes-->
				<section class="secaoClientes">
					<h5>Alguns de nossos clientes</h5>
					<p class="textoCliente">
						Conheça alguns casos de sucesso de diversos setores que estão com a Enllevo.
					</p>
					<div class="clientes slider">
						<div>
							<img src="images/clientes/acos-motta-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/aoki-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/assobens-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/avar-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/buscarioli-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/davoli-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div>
						<div>
							<img src="images/clientes/dilepe-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/hdi-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/inga-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/mhm-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/revimaqi-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/veisa-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div>
						<!--div>
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes01.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes01.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div>
						<div>
							<img src="images/mobile320/clientes01.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes01.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes01.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/mobile320/clientes.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div-->
					</div>

					<div class="owl-carousel-clientes owl-carousel owl-theme">
						<div class="divClientes1024">
							<img src="images/clientes/acos-motta-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/aoki-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/assobens-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/avar-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div>
						<div class="divClientes1024">
							<img src="images/clientes/buscarioli-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/davoli-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/dilepe-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/hdi-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div>
						<div class="divClientes1024">
							<img src="images/clientes/inga-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/mhm-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/revimaqi-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
							<img src="images/clientes/veisa-enllevo.jpg" alt="Nossos Clientes Enllevo" title="Enllevo - Soluções que Geram Satisfação">
						</div>
					</div>
					<a  class="botao" href="{{ route('clientes') }}">CONHEÇA TODOS NOSSOS CLIENTES </a>
				</section>
				<!-- > /Nossos Clientes -->
				<!--Depoimentos-->
				<aside class="secaoDepoimentos">
					<h5>Depoimentos</h5>
					<div class="depoimentos slider">
						<div class="slider-personalizado">
							<img class="imgDepoimentos" src="images/depoimentos/Jorge-Serrano-Pinto.png" alt="Depoimento" title="Enllevo - Soluções que Geram Satisfação">
							<p class="nomeDepoimentos">Jorge Serrano Pinto</p>
							<mark>Quando a Quantum iniciou o processo de expansão da sua presença para outros países, a identificação e escolha da empresa parceira é crucial
							e por se tratar de um elemento chave para o sucesso da operação. Quantum Brasil em aliança com a Enllevo no desenvolvimento de soluções como um todo,
							incluindo hosting, hardware específico, softwares complementares, dimensionamento de projetos e atendimento local, à medida das necessidades dos clientes,
							no modelo financeiro que for mais confortável, queremos garantir valências como a implementação dos sistemas Qguar em parceria com a Enllevo."</mark>
						</div>
						<div class="slider-personalizado">
							<img class="imgDepoimentos" src="images/depoimentos/Daniel-Gregorio-Picolotto.png" alt="Depoimento" title="Enllevo - Soluções que Geram Satisfação">
							<p class="nomeDepoimentos">Daniel Gregorio Picolotto</p>
							<mark>"O sistema veio para facilitar a vida do administrador e de todos os colaboradores, pois é um sistema completo e extremamente fácil
							de utiliza-lo. Com ele é possível  obter informações imediatas a respeito da empresa, referente a todos os departamentos, fazendo com que o controle
							se torne preciso e eficiente. Alem disso a empresa conta  com suporte extremamente rápido, com atendentes dispostos a ajudar no que for preciso e
							esclarecer quaisquer duvidas."</mark>
						</div>
						<div class="slider-personalizado">
							<img class="imgDepoimentos" src="images/depoimentos/padrao.png" alt="Depoimento" title="Enllevo - Soluções que Geram Satisfação">
							<p class="nomeDepoimentos">Edson Milton Cabezaolias</p>
							<mark>"Nos últimos anos estivemos trabalhando juntos em projetos de interesse dos nossos clientes e posso atestar as excelentes qualidades de da Enllevo. Tenho admiração pelos trabalho profissional da empresa, de total competência e que nunca falhou com um prazo estabelecido. Tenho a certeza que a contratação da empresa Enllevo será uma ótima escolha para qualquer empresa."</mark>
						</div>
						
						<div class="slider-personalizado">
							<img class="imgDepoimentos" src="images/depoimentos/raul-lullo.png" alt="Depoimento" title="Enllevo - Soluções que Geram Satisfação">
							<p class="nomeDepoimentos">Raul Lullo Junior</p>
							<mark>"A Enllevo, empresa de referência no mercado, que transmite confiança e preza pela qualidade e eficiência na prestação dos serviços, trabalhando em parceria há mais de 10 anos com o Grupo Buscarioli"</mark>
						</div>
					</div>
				</aside>

			<!-- > /Depoimentos -->
            <!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
		</div>
		<!-- JS Slick -->
		<script src="./plugins/slick/slick.min.js" type="text/javascript"></script>
		<!-- JS Slick -->

		<!-- owl.carousel js -->
		<script src="./plugins/OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
		<!-- owl.carousel js -->

		<!-- Swiper JS -->
		<script src="./plugins/swiper/js/swiper.min.js"></script><!-- novo Carrossel -->
		<!-- Swiper JS -->
    </body>
</html>
