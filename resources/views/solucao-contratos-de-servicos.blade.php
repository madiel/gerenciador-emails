<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Contratos de Serviços - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaContratos">

					<div class="bannerContratos">

					</div>

					<section class="bannerSolucaoContratos">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-contratos-de-servicos') }}" class="destaqueAzul">contratos de serviços</a>
							</span>
						</div>
							<span class="iconeSegContratos icone13"></span>
							<h1 class="tituloContratos">Solução Contratos de Serviços</h1>
							<h2 class="textIntroContratos">Solução Contratos e Serviços controle desde a criação de um contrato até seu encerramento, gerenciando
							com eficácia todas as fases de vida do contrato.</h2>
							<h3 class="contTextIntroContratos">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
					</div>

					<div>
						<img class="imgContratos" src="images/solucoes/Contratos-e-Servicos/contratos-e-servicos-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaContratos">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesContratos">CONHEÇA NOSSAS SOLUÇÕES PARA CONTRATOS DE SERVIÇOS</p>
						<hr>
					</aside>

					<div class="divSolucoesContratos">
						<ul>
							<li><a href="#CONTRATOS DE SERVIÇOS"><h1>CONTRATOS DE SERVIÇOS</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosContratos segmentoContratos" id="CONTRATOS DE SERVIÇOS">
						<div class="introContratos">
							<h1>CONTRATOS DE SERVIÇOS |</h1><h1 class="segContratos">MÓDULO</h1>

							<h2>Enllevo Modulo de Gestão - <strong>Sistema de gestão Automotiva</strong></h2>
						</div>
						<hr class="hrContratos">
						
						<div class="descricaoContratos">
							<p>A Solução da Enllevo para Contratos de Serviços (sistema ERP de Gestão Obras) trata do processo de gerenciamento e
							controle de todos os contratos de uma empresa com seus clientes e fornecedores, proporcionando um efetivo acompanhamento
							das fases dos eventos contratuais, gerando automaticamente as obrigações quando ao pagamento ou recebimento de valores acordados.</p>

							<p>Dê vida aos contratos de sua empresa com o sistema da Enllevo Contratos de Serviços e gerencie de forma centralizada seus
							inúmeros contratos simultaneamente.</p>

							</div>

						<div class="secaoContratos">
						<ul>
							<div class="gestaoContratos">
								<span>Segurança e confiabilidade no controle dos contratos de sua empresa</span>
							</div>
								<li>
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Armazenamento da imagem do contrato</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Rascunho do contrato</p>
								</li>

								<li>
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Negociação</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Ajustes de administração</p>
								</li>

								<li>
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Renegociação</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Melhoria e otimização</p>
								</li>

								<li>
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">######</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>######</p>
								</li>

								<li>
									<img class="imgSegContratos" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">######</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegContratos" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>######</p>
								</li>

								<li>
									<img class="imgSegContratos" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>######</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegContratos" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>######</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosContratos conteinerBeneficioContratos">
							<div class="beneficiosContratos">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Automatização da gestão de cobranças contratuais</li>
									<li>Fácil gestão, alteração e manutenção de faturamento parcelado</li>
									<li>Ampla visualização e controle do faturamento dos contratos </li>
									<li>Planejamento e controle de longo prazo </li>
									<li>Simplificação de processos</li>
									<li>Acesso Web para maior flexibilidade e mobilidade de uso</li>
									<li>Gerencia contratos (clientes e fornecedores)</li>
									<li>Controla vigências, reajustes e eventos</li>
									<li>Criar novo aditivo</li>
									<li>Renovação de um contrato ou lote</li>
									<li>Gera parcelas dos valores</li>
									<li>Armazena as imagens dos contratos</li>
									<li>Histórico dos eventos</li>
									<li>Redução dos passivos, riscos e custos</li>
									<li>Indicadores gráficos</li>
								</ul>
							</div>
							<div class="caracteristicasContratos">
								<p>Características do Sistema</p>

								<ul class="caracteristicaContratos">
									<li class="liNeutra">Todos os dados e características de cada contrato centralizados e organizados;</li>
									<li>Facilita a emissão de notas fiscais</li>
									<li>Emissão de boletos diretamente pelo sistema</li>
									<li>Automatização de cálculos para retenção de impostos</li>
									<li>Faturamento parcelado de contratos: gestão, organização, manutenção e controle</li>
									<li>Planejamento financeiro com ampla visão e controle</li>
									<li>Balanço financeiro organizado, claro e automatizado </li>
									<li>Divisão por unidades de negócios para maior controle sobre receitas e despesas </li>
									<li>Ferramenta fundamental para controle de resultados e suporte na tomada de decisões estratégicas </li>
									<li>Backoffice consolidado para ampla gestão financeira</li>
									<li>Possibilita visualização geral de receitas e despesas de sua empresa, 
										bem como analise e gestão individual de cada unidade de negócios internamente.</li>
									<li>Contratos jurídicos e contratos não jurídicos</li>
									<li>Registra mais de um cliente/fornecedor no mesmo contrato</li>
									<li>Informa rateio de custos/lucros por centro de custo</li>
									<li>Trabalha com diversos tipos de produtos ou serviços</li>
									<li>Disponibiliza o contrato para consultas de forma prática e ágil</li>
									<li>Trata a data de validade dos eventos</li>
									<li>Anexa a imagem do documento associada ao próprio</li>
									<li>Registra cancelamento (resilição) e suspensões do contrato</li>
									<li>Proporciona segurança de que os pagamentos e cobranças</li>
									<li>Gera relatório parametrizado de contratos</li>
								</ul>
							</div>
						</aside>
					</section>
					
			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeContratos">
					<div class="textoBackofficeContratos">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesContratos">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
