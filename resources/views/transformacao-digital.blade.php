<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Transformação Digital - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

			<!-- BannerTransformacaoDigital -->
			<div class="conteudoBanner">
				<div class="imagemBannerInstitucional">
					<img class="imgInstitucional" src="images/banner-topo-enllevo.jpg" title="Enllevo - Soluções que Geram Satisfação" alt="Institucional">
				</div>
				<div class="textoBannerTransformacao">
					<h1 class="transformacaoBanner">Transformação Digital</h1>
				</div>
			</div>
			<!-- BannerTransformacaoDigital -->

			<!--IntroducaoTransformacaoDigital-->
			<div class="backgroundTransformacaoDigital">
			<span class="caminhoSite">
                <a href="{{ route('index') }}">home</a> /
				<a href="{{ route('transformacao-digital') }}" class="destaqueAzul">Transformação Digital</a>
			</span>
					<section class="introducaoTranformacao">
						<h4>Transformação digital começa pelas pessoas e não pela tecnologia</h4>
						<p>Parecer um conceito futurista sem previsão para acontecer...</p>
					</section>
			<!--IntroduçãoTransformacaoDigital-->

			<!--TextoTransformacaoDigital-->
				<section class="sigTransformacaoDigital">
					<div class="imgTransformacaoDigital">
						<img>
					</div>
					<p>Transformação digital é um processo no qual as empresas fazem uso da tecnologia para melhorar o desempenho,
					aumentar o alcance e garantir resultados melhores.
					É uma mudança estrutural e cultural nas organizações dando um papel essencial para a tecnologia.</p>
				</section>

					<section class="descricaoTransformacaoDigital">
						<p>Processo de integrar tecnologia digital a todos os aspectos da empresa,
						o que exige mudanças fundamentais de tecnologia, cultura, operações, estrutural e entrega de valor.
						Porém, a transformação digital não é um conceito para o futuro, e sim algo pelo qual as empresas
						de hoje têm de dar o máximo de atenção para continuar vivas.
						Para aproveitar melhor as tecnologias emergentes e sua rápida expansão nas atividades humanas,
						uma empresa precisa se atualizar, transformando todos os seus processos e modelos.
						A transformação digital requer uma mudança significativa e se livrar da tecnologia legada,
						além de alterar a cultura da companhia para apoiar a aceleração que vem com a transformação digital.</p>

						<p class="paragrafoTransformacao">As empresas estão rapidamente trocando suas interações tradicionais por experiências digitais alimentadas pelos avanços na tecnologia.
						Frequentemente, isso ocorre não porque as empresas querem se transformar, mas porque elas precisam se transformar se quiserem sobreviver.
						O mercado passou a esperar que as empresas criem experiências digitais eficientes, e uma empresa que não se adaptar a esse novo modelo de cliente digital será abandonada.</p>

						<p class="paragrafoTransformacao">Por outro lado, uma empresa que aceitar as mudanças e se adaptar a um modelo mais ágil tem o potencial para ter mais sucesso do que nunca.
						Isso porque a transformação digital, em sua essência, significa examinar todos os aspectos de uma empresa e descobrir como modernizá-los,
						para que possam continuar a evoluir conforme a tecnologia evolui.</p>
					</section>
			<!--TextoTransformacaoDigital-->

			<!--RevolucaoIndustrial-->
					<aside class="revolucoesIndustriais">
						<h4>A 4ª REVOLUÇÃO INDUSTRIAL</h4>
						<span>Todas as revoluções industriais que a humanidade viveu em sua história aconteceram em momentos-chave,
						em que determinadas invenções impulsionaram grandes mudanças na sociedade. Enquanto as anteriores redefiniram os conceitos de transportar,
						produzir e modernizaram a indústria, esta que estamos prestes a viver está provando ser a mais desafiadora de todas.</span>
						<p>As revoluções industriais e seu impacto em todo o mundo:</p>
					</aside>

					<div class="divRevoluçãoIndustrial">

						<div class="detalhamentoRevolucao">
							<div class="linhaRevolucaoIndustrial"></div>
							<div class="numtituloRevolucao">
								<span class="numRevolucao">1º</span>
								<span class="tituloRevolucao">Revolução "1784"</span>
							</div>
								<div>
									<p class="descricaoRevolucao">Teve início na Inglaterra por volta da metade do século 18 e um dos seus acontecimentos mais marcantes foi
									a invenção da máquina a vapor. A novidade impulsionou a produção têxtil, o que gerou muitas novas profissões,
									as cidades cresceram e se expandiram não apenas economicamente, mas também geograficamente falando.
									No mesmo período, foram construídas as ferrovias, o transporte de bens de mercadorias se ampliou,
									assim como a busca por mas insumos para a indústria.</p>
								</div>
						</div>

							<div class="caracteristicasRevolucao">
								<img class="imgPrimeiraRevolucao">
								<div class="periodoIndustrial">
									<span class="tituloPeriodoIndustrial">Industria 1.0</span>
									<ul class="listagemPeriodoIndustrial">
										<li>Mecanização</li>
										<li>Tear</li>
										<li>Força à vapor</li>
									</ul>
									<img class="ilustracaoDemonstrativa">
								</div>
							</div>


							<div class="detalhamentoRevolucao">
							<div class="linhaRevolucaoIndustrial"></div>
							<div class="numtituloRevolucao">
								<span class="numRevolucao">2º</span>
								<span class="tituloRevolucao">Revolução "1870"</span>
							</div>
								<div>
									<p class="descricaoRevolucao">Aconteceu em meados do século 19, período em que surgiram várias invenções que até hoje fazem parte do nosso dia a dia:
									automóvel, telefone, tv, rádio, avião, entre outros. Como pode-se perceber, a grande revolução aconteceu na área tecnológica,
									o que desencadeou uma forte aceleração, da economia e do estilo de vida da sociedade como um todo.</p>
								</div>
							</div>

							<div class="caracteristicasRevolucao">
								<img class="imgPrimeiraRevolucao">
								<div class="periodoIndustrial">
									<span class="tituloPeriodoIndustrial">Industria 2.0</span>
									<ul class="listagemPeriodoIndustrial">
										<li>Produção em escala</li>
										<li>Linha de montagem</li>
										<li>Eletricidade e Combustão</li>
									</ul>
									<img class="ilustracaoDemonstrativa">
								</div>
							</div>


							<div class="detalhamentoRevolucao">
							<div class="linhaRevolucaoIndustrial"></div>
							<div class="numtituloRevolucao">
								<span class="numRevolucao">3º</span>
								<span class="tituloRevolucao">Revolução "1969"</span>
							</div>
								<div class="conjuntoTextoRevolucao">
									<p class="paragrafoRevolucao">Apesar de haver bastante divergência no sentido de quando realmente aconteceu,
									é consenso que aconteceu entre o fim da 2ª Guerra Mundial até a metade da década de noventa,
									com a popularização do computador pessoal e a internet.</p>

									<p class="paragrafoRevolucao">De qualquer maneira, o que sabe-se com certeza é que a 3ª revolução foi marcada por grandes avanços
									tecnológicos e científicos que atingiram todas as esferas:
									industrial, agropecuária, comercial e prestação de serviços.</p>

									<p class="paragrafoRevolucao">Outra evolução que merece menção é a globalização, que com sua chegada permitiu que o mundo se conectasse,
									trocando produtos, tecnologias e informações que antes levavam muito mais tempo para atravessar os oceanos de
									distância entre os continentes. Mas isso já seria o início da 4ª Revolução Industrial: a Transformação Digital.</p>
								</div>
							</div>

							<div class="caracteristicasRevolucao Revolucao3">
								<img class="imgPrimeiraRevolucao">
								<div class="periodoIndustrial">
									<span class="tituloPeriodoIndustrial">Industria 3.0</span>
									<ul class="listagemPeriodoIndustrial">
										<li>Automação Robótica</li>
										<li>Computadores e Internet</li>
										<li>Eletrônicos</li>
									</ul>
									<img class="ilustracaoDemonstrativa">
								</div>
							</div>


							<div class="detalhamentoRevolucao">
							<div class="linhaRevolucaoIndustrial"></div>
							<div class="numtituloRevolucao">
								<span class="numRevolucao">4º</span>
								<span class="tituloRevolucao">Revolução "dias atuais"</span>
							</div>

								<div class="conjuntoDescricaoRevolucao">
									<p class="descricaoRevolucao">A Quarta Revolução Industrial é diferente de tudo o que a humanidade já experimentou.
									Novas tecnologias estão fundindo os mundos físico, digital e biológico de forma a criar
									grandes promessas e possíveis perigos.
									A velocidade, a amplitude e a profundidade desta revolução estão nos forçando a repensar
									como os países se desenvolvem, como as organizações criam valor e o que significa ser humano.

									<p class="paragrafoQuartaRevolucao">O que caracteriza essa revolução?</p>

									<div class="caracQuartaRevolucao">
										<p><strong class="especificacaoQuartaRevolucao">Dados são o novo ouro:</strong>
										Cada era teve seu recurso primordial, como o carvão e a eletricidade.
										Porém, hoje em dia esse objeto é um pouco menos tangível.
										Conhecimento é a mercadoria de maior valor para a quarta revolução industrial,
										pois ele permite aproveitar melhor os recursos disponíveis e produzir com mais qualidade em menos tempo.</p>

										<p><strong class="especificacaoQuartaRevolucao">Máquinas que aprendem:</strong>
										Junto com a informação, também vem o aprendizado das inteligências artificiais.
										Graças ao grande acervo de ferramentas de dados, como o Big Data,
										a Nuvem e a Internet das Coisas, as máquinas já podem encontrar novos padrões de atuação,
										fazer testes e se tornarem mais inteligentes.</p>
									</div>
								</div>
							</div>

							<div class="caracteristicasRevolucao Revolucao4">
								<img class="imgPrimeiraRevolucao">
								<div class="periodoIndustrial">
									<span class="tituloPeriodoIndustrial">Industria 4.0</span>
									<ul class="listagemPeriodoIndustrial">
										<li>Sistemas Cibernéticos</li>
										<li>Internet das Coisas</li>
										<li>Conexões super rápidas</li>
										<li>Inteligência Artificial</li>
									</ul>
									<img class="ilustracaoDemonstrativa">
								</div>
							</div>
						</div>

					<aside class="beneficiosTransformacao">
						<h4>QUAIS OS BENEFÍCIOS DA TRANSFORMAÇÃO DIGITAL</h4>

						<p>De acordo com o livro "Transformação Digital: repensando o seu negócio para a Era Digital", de David L. Rogers,
						especialista em estratégia digital, existem 5 domínios estratégicos em mutação nessa era que fazem com que possamos
						observar esse novo mundo mais de perto. Eles são os <span class="textoDestaque">clientes, competição, dados, inovação e valor.</span>

						<p>Ao longo desses 5 domínios, as tecnologias digitais estão redefinindo muito dos princípios básicos da estratégia
						e mudando as regras de como as empresas devem operar no mercado para serem bem-sucedidas.</p>
					</aside>
			<!--RevolucaoIndustrial-->

			<!--ClienteDadosInovacaoValor-->
			<div class="clienteDadosInovacaoValor">
					<div class="clienteTransformacao">
						<div class="barraCliente"></div>
						<h4 class="tituloCliente">Cliente</h4>
					</div>

					<div class="conteudoCliente invisivel">
						<div class="barraClienteTransformacao"></div>
						<h4>Competição</h4>
						<p>O segundo domínio da transformação digital é a competição.</p>

						<p>Está acontecendo algo engraçado em relação a esse aspecto.
						Tradicionalmente competição e cooperação eram vistas como opostos.</p>

						<p>As empresas competiam com empresas rivais muito parecidas com elas mesmas
						e cooperavam com parceiros da cadeia de fornecimento que distribuíram seus
						bens ou forneciam os inputs necessários para a sua produção.</p>

						<p>Hoje estamos caminhando para um mundo de fronteiras mais fluidas em relação aos setores.
						Os nossos maiores desafiadores podem ser concorrentes fora do nosso segmento que oferecem valores parecidos.</p>

						<p>Essa "desintermediação digital" está virando de ponta cabeça parcerias e cadeias
						de fornecimento - os nossos parceiros de negócios de longa data podem tornar-se
						nossos maiores concorrentes se os nossos aliados tradicionais começarem a servir diretamente aos
						nossos clientes.</p>

						<p>O resultado final dessas mudanças é um grande deslocamento do que chamamos de competição.
						A batalha é sobre uma disputa de influência entre empresas,
						com modelos de negócio muito diferentes, cada uma buscando a alavancagem diante ao consumidor final.</p>
					</div>



				<div class="dadosTransformacao">
					<div class="barraDados"></div>
					<h4 class="tituloDados">Dados</h4>
				</div>
					<div class="conteudoDados invisivel">
						<span class="barraDadosTransformacao"></span>
						<h4>Competição</h4>
						<p>O segundo domínio da transformação digital é a competição.</p>

						<p>Está acontecendo algo engraçado em relação a esse aspecto.
						Tradicionalmente competição e cooperação eram vistas como opostos.</p>

						<p>As empresas competiam com empresas rivais muito parecidas com elas mesmas
						e cooperavam com parceiros da cadeia de fornecimento que distribuíram seus
						bens ou forneciam os inputs necessários para a sua produção.</p>

						<p>Hoje estamos caminhando para um mundo de fronteiras mais fluidas em relação aos setores.
						Os nossos maiores desafiadores podem ser concorrentes fora do nosso segmento que oferecem valores parecidos.</p>

						<p>Essa "desintermediação digital" está virando de ponta cabeça parcerias e cadeias
						de fornecimento - os nossos parceiros de negócios de longa data podem tornar-se
						nossos maiores concorrentes se os nossos aliados tradicionais começarem a servir diretamente aos
						nossos clientes.</p>

						<p>O resultado final dessas mudanças é um grande deslocamento do que chamamos de competição.
						A batalha é sobre uma disputa de influência entre empresas,
						com modelos de negócio muito diferentes, cada uma buscando a alavancagem diante ao consumidor final.</p>
					</div>

				<div class="inovacaoTransformacao">
					<div class="barraInovacao"></div>
					<h4 class="tituloInovacao">Inovação</h4>
				</div>
					<div class="conteudoInovacao invisivel">
						<span class="barraInovacaoTransformacao"></span>
						<h4>Competição</h4>
						<p>O segundo domínio da transformação digital é a competição.</p>

						<p>Está acontecendo algo engraçado em relação a esse aspecto.
						Tradicionalmente competição e cooperação eram vistas como opostos.</p>

						<p>As empresas competiam com empresas rivais muito parecidas com elas mesmas
						e cooperavam com parceiros da cadeia de fornecimento que distribuíram seus
						bens ou forneciam os inputs necessários para a sua produção.</p>

						<p>Hoje estamos caminhando para um mundo de fronteiras mais fluidas em relação aos setores.
						Os nossos maiores desafiadores podem ser concorrentes fora do nosso segmento que oferecem valores parecidos.</p>

						<p>Essa "desintermediação digital" está virando de ponta cabeça parcerias e cadeias
						de fornecimento - os nossos parceiros de negócios de longa data podem tornar-se
						nossos maiores concorrentes se os nossos aliados tradicionais começarem a servir diretamente aos
						nossos clientes.</p>

						<p>O resultado final dessas mudanças é um grande deslocamento do que chamamos de competição.
						A batalha é sobre uma disputa de influência entre empresas,
						com modelos de negócio muito diferentes, cada uma buscando a alavancagem diante ao consumidor final.</p>
					</div>



				<div class="valorTransformacao">
					<div class="barraValor"></div>
					<h4 class="tituloValor">Valor</h4>
				</div>
					<div class="conteudoValor invisivel">
						<span class="barraValorTransformacao"></span>
						<h4>Competição</h4>
						<p>O segundo domínio da transformação digital é a competição.</p>

						<p>Está acontecendo algo engraçado em relação a esse aspecto.
						Tradicionalmente competição e cooperação eram vistas como opostos.</p>

						<p>As empresas competiam com empresas rivais muito parecidas com elas mesmas
						e cooperavam com parceiros da cadeia de fornecimento que distribuíram seus
						bens ou forneciam os inputs necessários para a sua produção.</p>

						<p>Hoje estamos caminhando para um mundo de fronteiras mais fluidas em relação aos setores.
						Os nossos maiores desafiadores podem ser concorrentes fora do nosso segmento que oferecem valores parecidos.</p>

						<p>Essa "desintermediação digital" está virando de ponta cabeça parcerias e cadeias
						de fornecimento - os nossos parceiros de negócios de longa data podem tornar-se
						nossos maiores concorrentes se os nossos aliados tradicionais começarem a servir diretamente aos
						nossos clientes.</p>

						<p>O resultado final dessas mudanças é um grande deslocamento do que chamamos de competição.
						A batalha é sobre uma disputa de influência entre empresas,
						com modelos de negócio muito diferentes, cada uma buscando a alavancagem diante ao consumidor final.</p>
					</div>
			</div>
			<!--ClienteDadosInovacaoValor-->

			<!--JornadaDoSucesso-->
				<aside class="jornadaSucesso">
					<h4>A jornada para o sucesso</h4>
					<p>Se você chegou até aqui, é porque leva a sério a ideia de transformar a sua empresa para tirar pleno proveito dos recursos digitais.
					Então, nada melhor que aprender de forma prática o que será necessário para chegar ao fim dessa jornada com o objetivo realizado.</p>
				</aside>

				<section class="passosSucesso">
					<h4>São 3 passos que você precisa dar. Veja quais são:</h4>

					<div class="divJornadaSucesso">
						<img class="imgJornadaSucesso">
						<span class="tituloJornada">Visualize o futuro digital do negócio</span>

						<p class="paragrafoJornada">O primeiro passo é enxergar as possibilidades que se abrirão para o seu negócio assim que ele
						estiver realmente preparado para tirar proveito do ambiente digital.</p>

						<p class="paragrafoJornada">Com isso, você será capaz de montar um plano de ação,
						com cada etapa da mudança até chegar no ponto ideal.</p>

						<p class="paragrafoJornada">É claro que isso não deve ser feito sozinho,
						mas com a ajuda de outras figuras de liderança na empresa.</p>
					</div>

					<div class="divJornadaSucesso iniciativasDigitais">
						<img class="imgJornadaSucesso imgIniciativasDigitais">
						<span class="tituloJornada tituloIniciativasDigitais">Invista em iniciativas digitais e novas habilidades</span>

						<p class="paragrafoJornada paragrafoIniciativasDigitais">Algumas das iniciativas digitais que fazem parte da transformação digital, como o marketing de conteúdo,
						mas há muitas outras que vale a pena explorar.</p>

						<p class="paragrafoJornada paragrafoIniciativasDigitais">Além disso, não deixe de ajudar seu time a desenvolver novas habilidades,
						que serão importantes para a transformação digital, como a persuasão.</p>
					</div>


					<div class="divJornadaSucesso transformContinua">
						<img class="imgJornadaSucesso">
						<span class="tituloJornada">Transformação digital é uma jornada contínua</span>

						<p class="paragrafoJornada">O comportamento do cliente muda frequentemente, portanto a transformação digital é uma jornada contínua.
						É um projeto que nunca vai parar.

						<p class="paragrafoJornada">Se você chegar na conclusão que o seu negócio ficou muito mais eficiente depois do seu projeto terminado,
						saiba que há sempre mais para fazer!</p>

						<p class="paragrafoJornada">Procure se atualizar todos os dias e para isso leia as novidades no mundo do Marketing Digital.
						Olhe as tendências tecnológicas globais e saiba que investir na tecnologia será o maior benefício
						para os seus clientes agora e futuramente.</p>
					</div>
				</section>
				<article class="blocoCitacaoTransformacao">
					<hr class="linhaTransformacao">
					<span class="citacaoTransformacao">"A transformação digital não se diz respeito, basicamente, a tecnologia, mas sim a estratégia."</span>
					<p class="paragrafoCitacao">Podemos ajudar você a transformar seus negócios mais rápida e facilmente do que você jamais considerou possível.</p>
				</article>
			</div>
			<!--JornadaDoSucesso-->

			<!--ConversaConosco-->
				<aside class="faleConosco">
					<h4>Vamos conversar</h4>
					<p>Fale com um especialista da ENLLEVO sobre como você pode começar ou acelerar a sua transformação digital.</p>
					<span>
						<a href="contato.php">Fale Conosco</a>
					</span>
				</aside>
			<!--ConversaConosco-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->
        </main>
    </body>
</html>
