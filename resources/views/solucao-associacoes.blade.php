<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções de Associações - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->

		<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaAssociacoes">

					<div class="bannerAssociacoes">

					</div>

					<section class="bannerSolucaoAssociacoes">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
								<a href="{{ route('index') }}">home</a> /
								<a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-associacoes') }}" class="destaqueAzul">associações</a>
							</span>
						</div>
							<span class="iconeSegAssociacoes icone14"></span>
							<h1 class="tituloAssociacoes">Solução Associações</h1>
							<h2 class="textIntroAssociacoes">Solução certa para todos os tipos de...</h2>
							<h3 class="contTextIntroAssociacoes">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
					</div>

					<div>
						<img class="imgAssociacoes" src="images/solucoes/Associacoes/associacoes-enllevo.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoContabilFiscal-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaAssociacoes">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesAssociacoes">CONHEÇA NOSSAS SOLUÇÕES PARA ASSOCIAÇÕES</p>
						<hr>
					</aside>

					<div class="divSolucoesAssociacoes">
						<ul>
							<li><a href="#veiculoComercial"><h1>ERP</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosAssociacoes segmentoAssociacoes" id="veiculoComercial">
						<div class="introAssociacoes">
							<h1>ASSOCIAÇÕES |</h1><h1 class="segAssociacoes">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrAssociacoes">
						
						<div class="descricaoAssociacoes">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoAssociacoes">
						<ul>
							<div class="gestaoAssociacoes">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegAssociacoes" src="images/solucoes/provisorio.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosAssociacoes conteinerBeneficioAssociacoes">
							<div class="beneficiosAssociacoes">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Potencialização da estratégia de vendas e comercial</li>
									<li>Análise de mercado por segmento e concorrência</li>
									<li>Sistema conversa com o Back-office</li>
									<li>Cobrança automática entre os associados</li>
									<li>Agilidade no acompanhamento de emplacamentos </li>
									<li>Dashboards (layouts e gráficos)</li>
									<li>Mapas gerenciais</li>
									<li>Acompanhamento ágil e detalhado das ações e vendas da concorrência</li>
									<li>Análise de mercado </li>
									<li>Inserção automatica de produtos disponíveis no estoque </li>
									<li>Consultas disponíveis na rede para venda</li>

								</ul>
							</div>

							<div class="caracteristicasAssociacoes">
								<p>Características do Sistema</p>

								<ul class="caracteristicaAssociacoes">
									<li class="liNeutra">Acesso à base de dados de emplacamentos da empresa</li>
									<li>Gestão do banco de dados de emplacamentos</li>
									<li>Emissão de relatórios a partir da base de dados</li>
									<li>Monitoramento em tempo real de todos os emplacamentos do Brasil</li>
									<li>Análises por meio de relatórios convencionais ou Dashboards</li>
									<li>Ranking nacional dos emplacamentos </li>
									<li>Filtragem de segmentos de mercado, modelos de veículos </li>
									<li>Controle de cobranças de associações</li>
									<li>Emissão de boletos automático</li>
									<li>Bolsa de consulta de peças (banco de dados com todas os produtos da rede)</li>
									<li>Integração contabil e fiscal</li>
								</ul>
							</div>
						</aside>
					</section>


			<!--SoluçãoContabilFiscal-->


			<!--Backoffice-->
				<aside class="backofficeAssociacoes">
					<div class="textoBackofficeAssociacoes">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesAssociacoes">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
