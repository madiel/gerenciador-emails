<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Enllevo - Soluções Gestão de RH - Enllevo - Soluções que Geram Satisfação</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137221678-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137221678-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		
		<!-- CSS Reset -->
        <link href="./css/reset.css" rel="stylesheet">
        <!-- CSS Reset -->

		<!-- CSS -->
		<link href="./css/style.css" rel="stylesheet">
		<!-- CSS -->

		<!-- Biblioteca jQuery -->
		<script src="./scripts/jquery-3.3.1.min.js"></script>
		<!-- Biblioteca jQuery -->

		<!-- Scripts JS -->
		<script src="./scripts/scripts.js"></script>
		<!-- Scripts JS -->

    </head>
    <body>
        <main class="conteudoPrincipal">
            <!-- Include header -->
            @include('layouts.header')
			<!-- header -->


			<!--Cabeçalho-->
			<div class="backSecaoLogistica">
				<div class="redimencionaBannerRH">

					<div class="bannerRH">

					</div>

					<section class="bannerSolucaoRH">
						<div class="divCaminhoSolucoes">
							<span class="caminhoSolucoes">
                                <a href="{{ route('index') }}">home</a> /
                                <a href="{{ route('index') }}">soluções</a> /
								<a href="{{ route('solucao-gestao-rh') }}" class="destaqueAzul">gestão RH</a>
							</span>
						</div>
							<span class="iconeSegGestaoRh icone06"></span>
							<h1 class="tituloRH">Solução Gestão de RH</h1>
							<h2 class="textIntroRH">Solução certa para todos os tipos de Gestão de RH...</h2>
							<h3 class="contTextIntroRH">Conheça o ERP desenvolvido pela da Enllevo, gerencie os contratos com clientes e fornecedores,
							envolvendo contratos de compras, vendas, aluguéis, manutenções, serviços e todos os documentos relacionados.</h3>
					</section>
				</div>

					<div>
						<img class="imgRH" src="images/solucoes/Gestao-de-RH/gestao-de-rh.png" alt="Solução Fornecimento de Peças" title="Enllevo Solução Fornecimento de Peças">
					</div>
			<!--Cabeçalho-->

			<!--SoluçãoFornecimentoPeca-->
					<aside class="intermedioSolucaoProposta">
						<a href="{{ route('contato') }}">
							<div class="buttonPropostaRH">
								<h2 class="h2PropSolucao">FICOU INTERESSADO NA SOLUÇÃO?</h2>
								<h2 class="h2Proposta">PEÇA UMA PROPOSTA SEM COMPROMISSO.</h2>
							</div>
						</a>
						<p class="solucoesRH">CONHEÇA NOSSAS SOLUÇÕES PARA CONTRATOS DE SERVIÇOS</p>
						<hr>
					</aside>

					<div class="divSolucoesRH">
						<ul>
							<li class="pesquisa"><a href="#pesquisaClima"><h1>PESQUISA DE CLIMA ORGANIZACIONAL</h1></a></li>
							<li class="diario"><a href="#diarioBordo"><h1>DIÁRIO DE BORDO</h1></a></li>
							<li class="Competencias"><a href="#competencias"><h1>COMPETÊNCIAS</h1></a></li>
							<li class="Desempenho"><a href="#desempenho"><h1>DESEMPENHO</h1></a></li>
							<li class="matriz"><a href="#matrizAvaliacao"><h1>MATRIZ DE AVALIAÇÃO</h1></a></li>
							<li class="Sucessao"><a href="#sucessao"><h1>SUCESSÃO</h1></a></li>
						</ul>
					</div>
				</div>
					<section class="segmentosRH segmentoRH" id="pesquisaClima">
						<div class="introRH">
							<h1>PESQUISA DE CLIMA ORGANIZACIONAL |</h1><h1 class="segRH">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrRH">
						
						<div class="descricaoRH">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoRH">
						<ul>
							<div class="gestaoRH">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosRH conteinerBeneficiosRH1">
							<div class="beneficiosRH">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Definição de papéis e responsabilidades de forma clara e objetiva;</li>
									<li>Suporte e agiliza o processo de contratação e de avaliação;</li>
									<li>Garante uniformidade das informações, agilizando o processo de análise de decisão;</li>
									<li>Facilita a leitura dos diferentes níveis de responsabilidade, comparando com os mesmos;</li>
									<li>Adapta-se ao dinamismo do mercado;</li>
									<li>Visualização da estrutura organizacional de forma completa</li>
								</ul>
							</div>
							<div class="caracteristicasRH">
								<p>Características do Sistema</p>

								<ul class="caracteristicasRH1">
									<li class="liNeutra">Descrição conforme o modelo já utilizado pela empresa, garantindo a aderência à cultura da mesma;</li>
									<li>Otimiza o tempo estabelecendo previamente os parâmetro que padronizam as suas descrições de cargo;</li>
									<li>Mantém a base de colaboradores integrada ao sistema de folha de pagamento;</li>
									<li>Possibilita a descrição de cargos individuais ou em famílias;</li>
									<li>Integra as descrições ao módulo de avaçiação de competências;</li>
									<li>Gera o orgonograma a partir das descrições.</li>
								</ul>
							</div>
						</aside>
					</section>
			
					<!--Bloco Oposto-->
					<section class="segmentosRH segmentoRHOposto" id="diarioBordo">
						<div class="introRHOposto">
							<h1 class="h1IntroRH">DIÁRIO DE BORDO |</h1><h1 class="segRHOposto">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrRH hrOposto">

						<div class="descricaoRH descricaoOposto">
							<p>A Solução da Enllevo TMS (Sistema de Gestão de Transportes) é um software responsável pelo planejamento,
							monitoramento e contabilidade do transporte, em qualquer tipo de estrutura de distribuição.</p>

							<p>Aperfeiçoamento do planejamento de rotas, combinação de transportes,
							transbordos e muitas outras funções fazem deste módulo uma ferramenta perfeita para todas
							as empresas com operações de transporte complexas.</p>

						</div>

						<div class="secaoRH secaoOposto">
							<ul>
								<div class="gestaoRH gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-01.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-02.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-03.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-04.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-05.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-06.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-07.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-08.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-09.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-10.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-11.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-12.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-13.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-14.png" alt="Enllevo - Gestão de custos de transportadoras" title="Gestão de custos de transportadoras">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosRH conteinerBeneficiosOposto">
							<div class="beneficiosRH beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Prontidão na disponibilização de informações e a comunicação aos colaboradores traz celebridade ao processo;</li>
									<li>O gestor terá elementos necessários para cumprir a missão de avaliar e dar feedback;</li>
									<li>O feedback permite alinhar o gestor com os colaboradores de forma dinâmica e produtiva;</li>
									<li>A visão histórica do feedback permite perceber avanços ou ajustes necessários, aumentando a assertividade e ações
									direcionadas aos objetivos.</li>
								</ul>
							</div>
							<div class="caracteristicasRH caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasRH2">
									<li class="liNeutra">Permite a atribuição de feedbacks 360º com possibilidade de recuperação dos mesmos no momento da avaliação de
									competências, tornando o processo mais transparente;</li>
									<li>Proporciona a notificação dos colaboradores sobre os feedbacks recebidos, conforme decisão da empresa ou do avaliador;</li>
									<li>Promove a cultura de feedbacks com a possibilidade de acompanhamento pelo RH;</li>
									<li>Possibilita o envio e réplicas e tréplicas dos feedbacks;</li>
									<li>Oferece a personalização do formulário de feedback de acordo com a necessidade da empresa.</li>
								</ul>
							</div>
						</aside>
					</section>
			
					<!--Final Bloco Oposto-->
					<section class="segmentosRH segmentoRH" id="competencias">
						<div class="introRH">
							<h1>COMPETÊNCIAS |</h1><h1 class="segRH">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrRH">

						<div class="descricaoRH">
							<p>A Solução da Enllevo YMS (Sistema de Gerenciamento de Pátios) é um software especializado que permite controlar
							totalmente o tráfego de veículos e pedestres nos pátios e instalações da fábrica.</p>

							<p>Além disso, melhora o planejamento das operações de carga e descarga, otimizando o trabalho
							e minimizando o tempo de inatividade.</p>
						</div>


						<div class="secaoRH">

							<ul>
								<div class="gestaoRH">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegFornecimento" src="images/solucoes/Logistica/yms-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosRH conteinerBeneficiosRH3">
							<div class="beneficiosRH beneficiosRH3">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Disponibiliza o pronto acesso às informações de avaliações de colaboradores;</li>
									<li>Forma de medir resultados únicos;</li>
									<li>Comunicação e alinhamento durante todo o processo de avalização;</li>
									<li>Visualização simples e atualizada, apoia o gestor na análise e tomada de decisão;</li>
									<li>Adequação das informações conforme necessidades de cada nível de cargo;</li>
									<li>Várias visões sobre o colaborador garantindo transparência;</li>
									<li>Avaliação conforme o interesse e a necessidade da organização e ajustes de critérios por complexidade da área e cargo;</li>
									<li>Para o gestor mais detalhista um universo de informações, para os objetivos, agilidade.</li>
								</ul>
							</div>


							<div class="caracteristicasRH caracRH3">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolRH3">
									<li>Envolve todos os participantes no processo de validação dos colaboradores em período de experiência,
									melhorando a efetividade da contratação de pessoas;</li>
									<li>Viabiliza a avaliação conforme a régua definida pela cultura da empresa;</li>
									<li>Crie e-mails personalizados de notificações em todas as fases;</li>
									<li>Acompanhe as avaliações, em tempo real, por meio de dashboards;</li>
									<li>Possibilita avaliações nos modelos 90º, 180º e 360º;</li>
									<li>Aprove as avaliações por meio de comitê de calibragem;</li>
									<li>Customize o questionário de avaliações através da associação irrestrita de itens para cada uma das competências,
									advindas ou não do módulo de descrição de cargos;</li>
									<li>Apresenta os resultados individuais para os envolvidos de maneira estruturada.</li>
								</ul>
							</div>
						</aside>
					</section>

					<section class="segmentosRH segmentoRHOposto" id="desempenho">
						<div class="introRHOposto">
							<h1 class="h1IntroRH">DESEMPENHO |</h1><h1 class="segRHOposto">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrRH hrOposto">
						
						<div class="descricaoRH descricaoOposto">
							<p>A Solução da Enllevo para Automotivo é um sistema de gestão empresarial completo
							para o controle ideal do seu centro automotivo.</p>

							<p>Atuando de forma integrada, reúne as áreas de negócio mais relevantes ao
							setor, entregando ao cliente o mais completo serviço de gestão, o qual
							garante segurança e organização dos processos de todos os segmentos da indústria automotiva.</p>

							</div>

						<div class="secaoRH secaoOposto">
						<ul>
							<div class="gestaoRH gestaoOposto">
								<span>Gestão completa, flexível e sem limites para a evolução dos seus negócios</span>
							</div>
								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-01.png" alt="Entregas ao Armazém" title="Entregas ao Armazém">
									<p class="entregasArmazem">Integração com as montadoras</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-02.png" alt="Gestão de locais" title="Gestão de locais">
									<p>Multi empresas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-03.png" alt="Pickings" title="Pickings">
									<p>Acesso Transacional</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-04.png" alt="Expedição do armazém" title="Expedição do armazém">
									<p>Gestão em todas as unidades</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-05.png" alt="Inventários" title="Inventários">
									<p>Gestão de vendas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-06.png" alt="RF, RFID e Voz" title="RF, RFID e Voz">
									<p>Gestão de estoques</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-07.png" alt="Identificação Automática" title="Identificação Automática">
									<p class="identificacaoAuto">Gestão de serviços</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-08.png" alt="Status de qualidade e datas de validade" title="Status de qualidade e datas de validade">
									<p>Inventario de peças</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/provisorio.png" alt="Embalagens Retornáveis" title="Embalagens Retornáveis">
									<p class="embRetornaveis">Vendas Automotivas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/provisorio.png" alt="GS1" title="GS1">
									<p>Política Comercial</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/provisorio.png" alt="Integração com equipamentos externos" title="Integração com equipamentos externos">
									<p>Personalização às necessidades da empresa</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Automotivo/v-passeio-12.png" alt="Gestão de custos" title="Gestão de custos">
									<p>Segurança dos dados</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosRH conteinerBeneficiosOposto">
							<div class="beneficiosRH beneficiosOposto">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Definição de papéis e responsabilidades de forma clara e objetiva;</li>
									<li>Suporte e agiliza o processo de contratação e de avaliação;</li>
									<li>Garante uniformidade das informações, agilizando o processo de análise de decisão;</li>
									<li>Facilita a leitura dos diferentes níveis de responsabilidade, comparando com os mesmos;</li>
									<li>Adapta-se ao dinamismo do mercado;</li>
									<li>Visualização da estrutura organizacional de forma completa</li>
								</ul>
							</div>
							<div class="caracteristicasRH caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasRH2">
									<li class="liNeutra">Descrição conforme o modelo já utilizado pela empresa, garantindo a aderência à cultura da mesma;</li>
									<li>Otimiza o tempo estabelecendo previamente os parâmetro que padronizam as suas descrições de cargo;</li>
									<li>Mantém a base de colaboradores integrada ao sistema de folha de pagamento;</li>
									<li>Possibilita a descrição de cargos individuais ou em famílias;</li>
									<li>Integra as descrições ao módulo de avaçiação de competências;</li>
									<li>Gera o orgonograma a partir das descrições.</li>
								</ul>
							</div>
						</aside>
					</section>
			
					<!--Bloco Oposto-->
					<section class="segmentosRH segmentoRH" id="matrizAvaliacao">
						<div class="introRH">
							<h1>MATRIZ DE AVALIAÇÃO |</h1><h1 class="segRH">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrRH">

						<div class="descricaoRH">
							<p>A Solução da Enllevo TMS (Sistema de Gestão de Transportes) é um software responsável pelo planejamento,
							monitoramento e contabilidade do transporte, em qualquer tipo de estrutura de distribuição.</p>

							<p>Aperfeiçoamento do planejamento de rotas, combinação de transportes,
							transbordos e muitas outras funções fazem deste módulo uma ferramenta perfeita para todas
							as empresas com operações de transporte complexas.</p>

						</div>

						<div class="secaoRH">
							<ul>
								<div class="gestaoRH">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-01.png" alt="Enllevo - Ordens de transporte" title="Ordens de transporte">
									<p>Ordens de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-02.png" alt="Enllevo - Centros de distribuições" title="Centros de distribuições">
									<p>Centros de distribuições</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-03.png" alt="Enllevo - Meios de transporte" title="Meios de transporte">
									<p>Meios de transporte</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-04.png" alt="Enllevo - Planejamento manual e automático de rotas" title="Planejamento manual e automático de rotas">
									<p>Planejamento manual e automático de rotas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-05.png" alt="Enllevo - Teste de correção de rotas" title="Teste de correção de rotas">
									<p>Teste de correção de rotas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-06.png" alt="Enllevo - Prognóstico de custos de rotas" title="Prognóstico de custos de rotas">
									<p>Prognóstico de custos de rotas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-07.png" alt="Enllevo - Monitoramento de tarefas" title="Monitoramento de tarefas">
									<p class="identificacaoAuto">Monitoramento de tarefas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-08.png" alt="Enllevo - Gestão dos custos da operação" title="Gestão dos custos da operação">
									<p>Gestão dos custos da operação</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-09.png" alt="Enllevo - Monitoramento de rotas" title="Monitoramento de rotas">
									<p class="embRetornaveis">Monitoramento de rotas</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-10.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-11.png" alt="Enllevo - Mapas" title="Mapas">
									<p>Mapas</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-12.png" alt="Enllevo - Gestão de custos dos serviços" title="Gestão de custos dos serviços">
									<p>Gestão de custos dos serviços</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-13.png" alt="Enllevo - Gestão ADR" title="Gestão ADR">
									<p>Gestão ADR</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/tms-14.png" alt="Enllevo - Gestão de custos de transportadoras" title="Gestão de custos de transportadoras">
									<p>Gestão de custos de transportadoras</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosRH conteinerBeneficiosRH3">
							<div class="beneficiosRH beneficiosRH3">
								<p>Benefícios do Sistema</p>
								<ul>
									<li class="liNeutra">Prontidão na disponibilização de informações e a comunicação aos colaboradores traz celebridade ao processo;</li>
									<li>O gestor terá elementos necessários para cumprir a missão de avaliar e dar feedback;</li>
									<li>O feedback permite alinhar o gestor com os colaboradores de forma dinâmica e produtiva;</li>
									<li>A visão histórica do feedback permite perceber avanços ou ajustes necessários, aumentando a assertividade e ações
									direcionadas aos objetivos.</li>
								</ul>
							</div>
							<div class="caracteristicasRH caracRH3">
								<p>Características do Sistema</p>

								<ul class="caracteristicasRH3">
									<li class="liNeutra">Permite a atribuição de feedbacks 360º com possibilidade de recuperação dos mesmos no momento da avaliação de
									competências, tornando o processo mais transparente;</li>
									<li>Proporciona a notificação dos colaboradores sobre os feedbacks recebidos, conforme decisão da empresa ou do avaliador;</li>
									<li>Promove a cultura de feedbacks com a possibilidade de acompanhamento pelo RH;</li>
									<li>Possibilita o envio e réplicas e tréplicas dos feedbacks;</li>
									<li>Oferece a personalização do formulário de feedback de acordo com a necessidade da empresa.</li>
								</ul>
							</div>
						</aside>
					</section>
			
					<!--Final Bloco Oposto-->
					<section class="segmentosRH segmentoRHOposto" id="sucessao">
						<div class="introRHOposto">
							<h1 class="h1IntroRH">SUCESSÃO |</h1><h1 class="segRHOposto">ERP</h1>

							<h2>Enllevo ERP - <strong>Enterprise Resource Planning</strong></h2>
						</div>
						<hr class="hrRH hrOposto">
						
						<div class="descricaoRH descricaoOposto">
							<p>A Solução da Enllevo YMS (Sistema de Gerenciamento de Pátios) é um software especializado que permite controlar
							totalmente o tráfego de veículos e pedestres nos pátios e instalações da fábrica.</p>

							<p>Além disso, melhora o planejamento das operações de carga e descarga, otimizando o trabalho
							e minimizando o tempo de inatividade.</p>
						</div>


						<div class="secaoRH secaoOposto">

							<ul>

								<div class="gestaoRH gestaoOposto">
									<span>Gestão automatizada, redução de custos e maior eficiência nas operações.</span>
								</div>
								<li>
									<img class="imgSegFornecimento" src="images/solucoes/Logistica/yms-01.png" alt="Enllevo - Agenda de operações" title="Agenda de operações">
									<p>Agenda de operações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-02.png" alt="Enllevo - Medição dos tempos das operações" title="Medição dos tempos das operações">
									<p>Medição dos tempos das operações</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-03.png" alt="Enllevo - Minimização do acúmulo de recebimentos e envios" title="Minimização do acúmulo de recebimentos e envios">
									<p>Minimização do acúmulo de recebimentos e envios</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-04.png" alt="Enllevo - Planejamento de cargas e descargas" title="Planejamento de cargas e descargas">
									<p>Planejamento de cargas e descargas</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-05.png" alt="Enllevo - Gestão de cargas e descarga" title="Gestão de cargas e descarga">
									<p>Gestão de cargas e descarga</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-06.png" alt="Enllevo - Minimização de eventuais furtos" title="Minimização de eventuais furtos">
									<p>Minimização de eventuais furtos</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-07.png" alt="Enllevo - Gestão de estacionamento e pedestres" title="Gestão de estacionamento e pedestres">
									<p class="identificacaoAuto">Gestão de estacionamento e pedestres</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-08.png" alt="Enllevo - Registro de peso" title="Registro de peso">
									<p>Registro de peso</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-09.png" alt="Enllevo - Comunicação e notificações" title="Comunicação e notificações">
									<p class="embRetornaveis">Comunicação e notificações</p>
								</li>

								<li class="liSegLog">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-10.png" alt="Enllevo - Planejamento de tráfego de veículos" title="Planejamento de tráfego de veículos">
									<p>Planejamento de tráfego de veículos</p>
								</li>

								<li>
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-11.png" alt="Enllevo - Otimização da utilização dos veículos" title="Otimização da utilização dos veículos">
									<p>Otimização da utilização dos veículos</p>
								</li>

								<li class="liSegLogistica">
									<img class="imgSegRH" src="images/solucoes/Logistica/yms-12.png" alt="Enllevo - Interação com sistemas de controle de acesso" title="Interação com sistemas de controle de acesso">
									<p>Interação com sistemas de controle de acesso</p>
								</li>
							</ul>
						</div>
						<aside class="conteinerBeneficiosRH conteinerBeneficiosOposto">
							<div class="beneficiosRH  beneficiosOposto">
								<p>Benefícios do Sistema</p>

								<ul>
									<li>Disponibiliza o pronto acesso às informações de avaliações de colaboradores;</li>
									<li>Forma de medir resultados únicos;</li>
									<li>Comunicação e alinhamento durante todo o processo de avalização;</li>
									<li>Visualização simples e atualizada, apoia o gestor na análise e tomada de decisão;</li>
									<li>Adequação das informações conforme necessidades de cada nível de cargo;</li>
									<li>Várias visões sobre o colaborador garantindo transparência;</li>
									<li>Avaliação conforme o interesse e a necessidade da organização e ajustes de critérios por complexidade da área e cargo;</li>
									<li>Para o gestor mais detalhista um universo de informações, para os objetivos, agilidade.</li>
								</ul>
							</div>


							<div class="caracteristicasRH  caracteristicasOposto">
								<p>Características do Sistema</p>

								<ul class="caracteristicasSolRH2">
									<li>Envolve todos os participantes no processo de validação dos colaboradores em período de experiência,
									melhorando a efetividade da contratação de pessoas;</li>
									<li>Viabiliza a avaliação conforme a régua definida pela cultura da empresa;</li>
									<li>Crie e-mails personalizados de notificações em todas as fases;</li>
									<li>Acompanhe as avaliações, em tempo real, por meio de dashboards;</li>
									<li>Possibilita avaliações nos modelos 90º, 180º e 360º;</li>
									<li>Aprove as avaliações por meio de comitê de calibragem;</li>
									<li>Customize o questionário de avaliações através da associação irrestrita de itens para cada uma das competências,
									advindas ou não do módulo de descrição de cargos;</li>
									<li>Apresenta os resultados individuais para os envolvidos de maneira estruturada.</li>
								</ul>
							</div>
						</aside>
					</section>
			<!--SoluçãoGestaoRh-->


			<!--Backoffice-->
				<aside class="backofficeRH">
					<div class="textoBackofficeRH">
						<h3>BackOffice</h3>
						<h4>Também conhecido como atividades de apoio,
						nosso backoffice contempla módulos que automatizam os processos administrativos de uma empresa,
						permitindo mais eficiência e produtividade.</h4>
					</div>

					<ul class="backofficeOpcoesRH">
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FINANCEIRO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">CONTABILIDADE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FISCAL</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">FATURAMENTO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">JURÍDICO</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">ESTOQUE</a></li>
						<li><a href="{{ route('erro') }}" title="Enllevo - Soluções que Geram Satisfação">RH</a></li>
					</ul>
				</aside>

			<!--Backoffice-->



			<!-- Include footer -->
            @include('layouts.footer')
			<!-- footer -->


        </main>
    </body>
</html>
