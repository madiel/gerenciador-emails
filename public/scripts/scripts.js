$(document).ready(function(){

	
	// if(window.width < 1024) {
		window.addEventListener('resize', redimensionaJanela)
		window.onload=redimensionaJanela()
	// }

	
	// Abre o menu Mobile
	$(".menuMobile").on("click", function() {
		$(".menuNavegacao").addClass("aberto");
		$(".menuAberto").removeClass("invisivel");
		$(this).addClass("invisivel");
	});
	
	// Fechar o menu Mobile
	$(".fecharMenu").on("click", function() {
		$(".menuNavegacao").removeClass("aberto");
		$(".menuAberto").addClass("invisivel");
		$(".menuInicial").removeClass("invisivel");
		$(".listaIdiomas").addClass("invisivel");
		$(".menuSolucoes").addClass("invisivel");
		$(".menuServicos").addClass("invisivel");
		$(".menuMobile").removeClass("invisivel");
	});
	
	// Abre o menu Soluções
	$(".solucoes").on("click", function() {
		$(".menuInicial").addClass("invisivel");
		$(".menuSolucoes").removeClass("invisivel");
	});
	
	// Fechar o menu Soluções
	$(".fecharMenuSolucoes").on("click", function() {
		$(".menuInicial").removeClass("invisivel");
		$(".menuSolucoes").addClass("invisivel");
	});
	
	// Abre o menu Serviços
	$(".servicos").on("click", function() {
		$(".menuInicial").addClass("invisivel");
		$(".menuServicos").removeClass("invisivel");
	});
	
	// Fechar o menu Serviços
	$(".fecharMenuServicos").on("click", function() {
		$(".menuInicial").removeClass("invisivel");
		$(".menuServicos").addClass("invisivel");
		$(".menuNavegacao").addClass("backgroundAzul");
		$(".menuNavegacao").removeClass("backgroundVerde");
	});
	
	//Abre Soluções Desktop
	$(".menuDesktopSolucoes").on("click", function(){
		if($(".abreMenuDesktop").hasClass("invisivel")){
			$(".abreMenuDesktop").removeClass("invisivel");
			$(".abreMenuServicos").addClass("invisivel");
		}else{
			$(".abreMenuDesktop").addClass("invisivel");	
		}
	});
	
	$("body").on("click", function(){
		$(".abreMenuDesktop").addClass("invisivel");	
	});
	
	//Abre Serviços Desktop
	$(".menuDesktopServicos").on("click", function(){
		if($(".abreMenuServicos").hasClass("invisivel")){
			$(".abreMenuServicos").removeClass("invisivel");
			$(".abreMenuDesktop").addClass("invisivel");
		}else{
			$(".abreMenuServicos").addClass("invisivel");	
		}
	});
	
	$("body").on("click", function(){
		$(".abreMenuServicos").addClass("invisivel");	
	});
	
	//Abrir informações Transformação Digital
	$(".clienteTransformacao").on("click", function(){
		if($(".conteudoCliente").hasClass("invisivel")){
			$(".conteudoCliente").removeClass("invisivel");
		}else {
			$(".conteudoCliente").addClass("invisivel");
		}
		
	});
	
	$(".dadosTransformacao").on("click", function(){
		if($(".conteudoDados").hasClass("invisivel")){
			$(".conteudoDados").removeClass("invisivel");
		}else {
			$(".conteudoDados").addClass("invisivel");
		}
		
	});
	
	$(".inovacaoTransformacao").on("click", function(){
		if($(".conteudoInovacao").hasClass("invisivel")){
			$(".conteudoInovacao").removeClass("invisivel");
		}else {
			$(".conteudoInovacao").addClass("invisivel");
		}
		
	});
	
	$(".valorTransformacao").on("click", function(){
		if($(".conteudoValor").hasClass("invisivel")){
			$(".conteudoValor").removeClass("invisivel");
		}else {
			$(".conteudoValor").addClass("invisivel");
		}
		
	});
	
	//Expande barra de pesquisa 
	$(".iconeAbrirInput").on("click", function(event){
		event.stopPropagation();
		if(!$(".inputBarraPesquisa").hasClass("inputBarraPesquisaExpandido")){
			$(".inputBarraPesquisa").addClass("inputBarraPesquisaExpandido");
			$(".inputBarraPesquisa").focus();
		}else {
			$(".inputBarraPesquisa").removeClass("inputBarraPesquisaExpandido");
		}
	});
	
	$(".iconePesquisar").on("click", function(event){
		event.stopPropagation();
		alert('pesquisar');
	});
	// Caso o input receba o click o event não é propagado
	$(".inputBarraPesquisa").on("click", function(event){
		event.stopPropagation();
	});
	
	//Retrai barra de pesquisa
	$(".headerDesk").on("click", function(event){
		event.stopPropagation();
		$(".inputBarraPesquisa").removeClass("inputBarraPesquisaExpandido");
	});	
	
	//Troca de background
	
	$(".servicos").on("click", function() {
		
	if($(".menuNavegacao").hasClass("backgroundAzul")){
	$(".menuNavegacao").addClass("backgroundVerde");
	$(".menuNavegacao").removeClass("backgroundAzul");
		
		
	}else{
		$(".menuNavegacao").removeClass("backgroundVerde");
		$(".menuNavegacao").addClass("backgroundAzul");	
	}
	});
	
	// Abre a Lista de Idiomas
	$(".idiomaSelecionado").on("click", function() {
		$(".listaIdiomas").removeClass("invisivel");
	});
	
	// Altera idioma selecionado
	$(".listaIdiomas img").on("click", function(){
		$(".idiomaSelecionado").attr("src", $(this).attr("src"));
		$(".idiomaSelecionado").attr("title", $(this).attr("alt"));
		$(".idiomaSelecionado").attr("alt", $(this).attr("alt"));
		$(".listaIdiomas").addClass("invisivel");
	});
	
	// Exibir informações da Enllevo
	$(".caixaInfoNegocios").on("click", function() {
		if(!$(".caixaImagensInfoNegocios").hasClass("invisivel")){
			$(".caixaImagensInfoNegocios").addClass("invisivel");
			$(".slickNegocios").slick('unslick');
			fecharPopupInfoEmpresa();
		}else{
			$(".caixaImagensInfoNegocios").removeClass("invisivel");
			// Carousel slickNegocios
			$(".slickNegocios").slick({
				lazyLoad: 'ondemand', // ondemand progressive anticipated
				arrows: false,
				dots: false,
				infinite: true,
				fade: true,
				mobileFirst: true
			});
		}
	});
	
	$(".caixaInfoEmpresarial").on("click", function() {
		if(!$(".caixaImagensEmpresarial").hasClass("invisivel")){
			$(".caixaImagensEmpresarial").addClass("invisivel");
			$(".slickEmpresarial").slick('unslick');
			fecharPopupInfoEmpresa();
		}else{
			$(".caixaImagensEmpresarial").removeClass("invisivel");
			// Carousel slickEmpresarial
			$(".slickEmpresarial").slick({
				lazyLoad: 'ondemand', // ondemand progressive anticipated
				arrows: false,
				dots: false,
				infinite: true,
				fade: true,
				mobileFirst: true
			});
		}
	});
	
	$(".caixaInfoGestao").on("click", function() {
		if(!$(".caixaImagensGestao").hasClass("invisivel")){
			$(".caixaImagensGestao").addClass("invisivel");
			$(".slickGestao").slick('unslick');
			fecharPopupInfoEmpresa();
		}else{
			$(".caixaImagensGestao").removeClass("invisivel");
			// Carousel slickGestao
			$(".slickGestao").slick({
				lazyLoad: 'ondemand', // ondemand progressive anticipated
				arrows: false,
				dots: false,
				infinite: true,
				fade: true,
				mobileFirst: true
			});
		}
	});
	
	//if($(".slickVideoEnllevo").length > 0){
		// Carousel slickEnllevo
		//$(".slickVideoEnllevo").slick({
			//arrows: false,
			//dots: false,
			//fade: true,
			//infinite: true,
		//});
		
		//videoEnllevo.play();
		
		//$('.slickVideoEnllevo').on('afterChange', function(event, slick, slide) {
		  //if (slide == 1) {
			//$('.slickVideoEnllevo').slick('slickPause');
			//videoEnllevo.play();
		  //}
		//});

		//$('#videoEnllevo').on('ended', function() {
			//$('.slickVideoEnllevo').slick('slickPlay');
		//});
	//}
	if($(".owl-carousel-videoEnllevo").length > 0){
		$('.owl-carousel-videoEnllevo').owlCarousel({
			loop:true,
			nav:true,
			dots:false,
			items:1,
			lazyLoad:true,
			autoplay:true,
			autoplayTimeout:5000,
			autoplayHoverPause:true
		})
	}
	// if($(".slickVideoEnllevo").length > 0){
		// Carousel slickEnllevo
		// $(".slickVideoEnllevo").slick({
		// 	arrows: false,
		// 	dots: false,
		// 	fade: true,
		// 	infinite: true,
		// 	slidesToScroll: 1,
		// 	autoplay: true,
		// 	autoplaySpeed: 7000
		// });
		
		

		//
		//$('.slickVideoEnllevo').on('afterChange', function(event, slick, slide) {
		  //if (slide >= 1) {
			//$('.slickVideoEnllevo').slick('slickPause');
		
			//videoEnllevo.play();
		  //}
		//});

		//$('#videoEnllevo').on('ended', function() {
			//$('.slickVideoEnllevo').slick('slickPlay');
		//});
	// }
	
	if($(".swiper-enllevo").length > 0){
		// Carousel swiper-enllevo

		var swiperEnllevo = new Swiper('.swiper-enllevo', {
			slidesPerView: 'auto',
			spaceBetween: 24,
			pagination: {
			el: '.swiper-pagination',
				clickable: true,
			},
		});
	}
	
	
	if($(".slickEnllevo").length > 0){
		// Carousel slickEnllevo
		$(".slickEnllevo").slick({
			lazyLoad: 'ondemand', // ondemand progressive anticipated
			arrows: false,
			dots: true,
			infinite: true,
			fade: true,
			mobileFirst: true
		});
	}
	
	if($(".depoimentos").length > 0){
		// Carousel depoimentos
		$(".depoimentos").slick({
			lazyLoad: 'ondemand', // ondemand progressive anticipated
			arrows: false,
			dots: true,
			infinite: true,
			slidesToShow: 1,
			mobileFirst: true
		});
	}
	
	if($(".clientes").length > 0){
		// Carousel clientes
		$(".clientes").slick({
			lazyLoad: 'ondemand', // ondemand progressive anticipated
			arrows: false,
			dots: false,
			infinite: true,
			fade: true,
			mobileFirst: true
		});
	}
	
	if($(".owl-carousel-clientes").length > 0){
		$('.owl-carousel-clientes').owlCarousel({
			loop:true,
			nav:false,
			items:1,
			lazyLoad:true,
		})
	}
	
	
	// Mostrar o restante dos textos
	$(".exibirTexto").on('click', function() {
		$(".caixaConteudo .paragrafoHistoriaEnllevo.invisivel").removeClass("invisivel");
		$(this).addClass("invisivel");
		$(".recolherTexto").removeClass("invisivel");
	});
	
	// Voltar a exibir somento o primeiro paragrafo
	$(".recolherTexto").on('click', function() {
		$(".caixaConteudo .paragrafoHistoriaEnllevo").each(function(index) {
			if(index > 0) {
				$(this).addClass("invisivel");
			}
		});
		$(this).addClass("invisivel");
		$(".exibirTexto").removeClass("invisivel");
	});
	
	// Equipe Enllevo
	// $(".figureColaborador").on('click', mostrarInfoColaborador1024);

	// Equipe Enllevo
	$(".figureColaborador1024").on('click', mostrarInfoColaborador1024);

	$(".adicaoTexto").on('click', modalInfo);
	
	// Mostrar todos os colaboradores
	$(".exibirEquipe").on('click', function() {
		$(".conteinerPerfilProfissional1024 figure.invisivel").removeClass("invisivel");
		$(this).addClass("invisivel");
		$(".recolherEquipe").removeClass("invisivel");
	});
	
	// Voltar a exibir somento os primeiros colaboradores
	$(".recolherEquipe").on('click', function() {
		$(".conteinerPerfilProfissional1024 figure.figureColaborador1024").each(function(index) {
			if(index > 1) {
				$(this).addClass("invisivel");
			}
		});
		$(this).addClass("invisivel");
		$(".exibirEquipe").removeClass("invisivel");
	});

	//Contagem dos caracteres da caixa dos colaboradores
	// window.onload = divXpto = $('.paragrafoInfoColaborador1024');
	// divXpto.each(function(index, divXptov){
	// 	var texto = $(divXptov).text();		
	// 	$(divXptov).text(texto.substring(0,150));	
	// 	divXpto[index] = divXptov;		
	// });



	//Abre Modal Texto Colaboradores
	// $(".adicaoTexto").on('click', 

	// 	$(".modalInfo").removeClass("invisivel");
		
	// );

	//Fechar Modal Texto Colaboradores
	$(".cabecalhoModal").on('click', modalInfo);

	
	// Mostrar todas as areas de atuação
	$(".exibirAreaAtuacao").on('click', function() {
		$(".caixaAreaAtuacao .areaAtuacao.invisivel").removeClass("invisivel");
		$(this).addClass("invisivel");
		$(".recolherAreaAtuacao").removeClass("invisivel");
	});
	
	// Voltar a exibir somento as areas de atuação
	$(".recolherAreaAtuacao").on('click', function() {
		$(".caixaAreaAtuacao .areaAtuacao").each(function(index) {
			if(index > 1) {
				$(this).addClass("invisivel");
			}
		});
		$(this).addClass("invisivel");
		$(".exibirAreaAtuacao").removeClass("invisivel");
	});
	
	// radioButton formulário contato
	$(".radioButtonQuadrado").on('click', selecionaRadioButton);
	
	// Alterar o texto do label input file no evento change
	$('.upload').change(function() {
		//$('#selected_filename').text($('#fileinput]')[0].files[0].name);
		$(".labelInputFile").text($(".upload")[0].files[0].name);
		console.log($(".upload")[0].files[0].name);
	});
	// animação linha do tempo
	var $target = $('.secaoLinhaDoTempo');
	animacaoLinhadoTempo($target);
	

	
	// Novo
	//Exibir conteudo InfoNegocios
	$(".caixaInfoNegocios").on('click', conteudoCaixaInfoNegocios);
	//Exibir conteudo InfoEmpresarial
	$(".caixaInfoEmpresarial").on('click', conteudoCaixaInfoEmpresarial);
	//Exibir conteudo 
	$(".caixaInfoGestao").on('click', conteudoCaixaInfoGestao);
	
	// Abrir a modal para exibir a imagem do carrossel 
	$('#container img').on('click', abrirModalCarrossel);
	
	// Abrir a modal para exibir a imagem do carrossel 
	$('#container2 img').on('click', abrirModalCarrossel);
	
	// Abrir a modal para exibir a imagem do carrossel 
	$('#container3 img').on('click', abrirModalCarrossel);
	
	// Fechar a modal
	$('.modalCarrossel').on('click', function() {
		$(this).css('display', 'none');
	});
	
	//Rolagem Solução Logistica	
	var $doc = $('html, body');
	$('a').click(function() {
		$doc.animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 500);
		return false;
	});
	
	//Scroll volta ao topo
	$('.voltaTopo').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 700);
		return false;
	});
	
});

function selecionaRadioButton() {
	// Guarda na variável o elemento que sofreu o evento
	var containerDiv 	= $(event.target).parent();
	// Remove a classe selecionado na labelRadioButtonQuadrado
	$(".caixaRadioButtonQuadrado .labelRadioButtonQuadrado").removeClass("selecionado");
	// Pega o label do elemento que sofreu o evento
	var labelRadioButton = $(containerDiv).find('.labelRadioButtonQuadrado');
	// Adiciona a classe selecionado na label
	$(labelRadioButton).addClass("selecionado");
}

function mostrarInfoColaborador1024(){
	var figCaption = $(this).find('figcaption.infoColaborador');
	if($(this).hasClass('figureInfoColaborador')){
		$(this).removeClass('figureInfoColaborador');
		figCaption.addClass('invisivel');
	}else{
		$(this).addClass('figureInfoColaborador');
		figCaption.removeClass('invisivel');
	}
}


function mostrarInfoColaborador1024(){
	
	var figCaption = $(this).find('figcaption.infoColaborador1024');
	if($(this).hasClass('figureInfoColaborador1024')){
		$(this).removeClass('figureInfoColaborador1024');
		figCaption.addClass('invisivel');
	}else{
		$(this).addClass('figureInfoColaborador1024');
		figCaption.removeClass('invisivel');
	}
}

// function textoCaixaColab() {
// 	var tamColab = $('.paragrafoInfoColaborador1024');
// }


function modalInfo(){

	var figure = $(this).parent().parent();
	var figureModal = $(figure).find('.modalInfoColab');
	if($(figureModal).hasClass('modalInfo')){
		$(figureModal).removeClass('modalInfo');
		$(figureModal).addClass('invisivel');
	}else{
		$(figureModal).addClass('modalInfo');
		$(figureModal).removeClass('invisivel');
	}
}
function AbrirFecharIOT(secaoIOTDados,secaoIOT){
	
	if($(secaoIOTDados).hasClass("invisivel")){
		$(secaoIOTDados).removeClass("invisivel");
		$(secaoIOT).addClass("fundoSelecionado");
	}else{
		$(secaoIOTDados).addClass("invisivel");
		$(secaoIOT).removeClass("fundoSelecionado");	
	}
}

debounce = function(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function animacaoLinhadoTempo($target) {
	function animeScroll() {
		var documentTop 	= $(document).scrollTop();
		var animationClass 	= 'animationSecao';
		var	offset 			= $(window).height() * 4/5;

		$target.each(function(){
			var itemTop = $(this).offset().top;

			if (documentTop > itemTop - offset) {

				$(this).addClass(animationClass);
			} else {
				$(this).removeClass(animationClass);
		}
		});
	}

	animeScroll();

	$(document).scroll(debounce(function(){
		animeScroll();
	}, 200));
}

// Abrir Popup Informações da empresa
function abrirPopupInfoEmpresa(event) {
	var containerDiv 	= $(event.target).parent();
	var backgroundPopup = $(containerDiv).find('.backgroundPopupInfoEmpresa');
	var popup 			= $(containerDiv).find('.popupInfoEmpresa');

	$(this).addClass("invisivel");
	$(backgroundPopup).removeClass("invisivel");
	$(popup).removeClass("invisivel");
}

// Fechar Popup Informações da empresa
function fecharPopupInfoEmpresa() {
	$('.backgroundPopupInfoEmpresa').addClass("invisivel");
	$('.popupInfoEmpresa').addClass("invisivel");
	$(".botaoPopupInfoEmpresa").removeClass("invisivel");
}

//Troca de cor Nuvem de Tags Index

/*function trocaCor() {
	var corAzul = document.getElementsByClassName("fontAzul");
	var corVerde = document.getElementsByClassName("fontVerde");
	
	if (corAzul.getClickCount() == 1 {
		corAzul = document.getElementsByClassName("tagEnllevoConfianca");
		corAzul = document.getElementsByClassName("tagEnllevoContentamento");
		corAzul = document.getElementsByClassName("tagEnllevoExperiencia");
		corAzul = document.getElementsByClassName("tagEnllevoAgilidade");
		corAzul = document.getElementsByClassName("tagEnllevoSolucao");
		corAzul = document.getElementsByClassName("tagEnllevoExtase");
	}
	else {
		corVerde = document.getElementsByClassName("tagEnllevoConfianca");
		corVerde = document.getElementsByClassName("tagEnllevoContentamento");
		corVerde = document.getElementsByClassName("tagEnllevoExperiencia");
		corVerde = document.getElementsByClassName("tagEnllevoAgilidade");
		corVerde = document.getElementsByClassName("tagEnllevoSolucao");
		corVerde = document.getElementsByClassName("tagEnllevoExtase");
	}
}*/

function trocaCor() {
	var cont = 0;
	
	document.getElementsByClassName("tagNuvem").onclick=function(){
		if(cont==0){
			corVerde();
			cont=+1;
		}
		else{
			corAzul();
			cont=0;
		}
	}
	
	function corVerde() {
		var corVerde = document.getElementsByClassName("fontVerde");
		
		corVerde = document.getElementsByClassName("tagEnllevoConfianca");
		corVerde = document.getElementsByClassName("tagEnllevoContentamento");
		corVerde = document.getElementsByClassName("tagEnllevoExperiencia");
		corVerde = document.getElementsByClassName("tagEnllevoAgilidade");
		corVerde = document.getElementsByClassName("tagEnllevoSolucao");
		corVerde = document.getElementsByClassName("tagEnllevoExtase");
	}
	
	function corAzul() {
		var corAzul = document.getElementsByClassName("fontAzul");
		
		corAzul = document.getElementsByClassName("tagEnllevoConfianca");
		corAzul = document.getElementsByClassName("tagEnllevoContentamento");
		corAzul = document.getElementsByClassName("tagEnllevoExperiencia");
		corAzul = document.getElementsByClassName("tagEnllevoAgilidade");
		corAzul = document.getElementsByClassName("tagEnllevoSolucao");
		corAzul = document.getElementsByClassName("tagEnllevoExtase");
	}
}

/*function resolucao() {
	if (screen.width <= 992) {
	document.write(<?php require_once('_header.php')?>);
	}
	else if (screen.width > 992) {
	document.write(<?php require_once('_header1024.php')?>);
	} 
}*/

// carrocel

/**
 * Created by tingyuan on 15/12/29.
 */
;
(function ($) {
    if ($ === undefined) {
        return;
    }

    var defaultConfig = {
        num: 3, //要显示的数量，应该是个奇数
        maxWidth: 250, //代表中央图片的宽度
        maxHeight: 150, //中央图片的高度
        autoPlay: true, //是否自动滚动播放
        showTime: 1000, //autoPlay为true时这个属性才有用
        animationTime: 300, //
        scale: 0.8,
        distance: 50
    };


    function getzIndexValue(num, direction) {
        var zIndexs = [];
        for (var i = 0; i < num; i++) {
            if (i <= (num - 1) / 2) {
                zIndexs.push(i);
            } else {
                zIndexs.push((num - 1) / 2 - i);
            }
        }
        if (direction === 'left') {
            zIndexs.reverse();
            return zIndexs;
        }
        if (direction === 'right') {
            return zIndexs;
        }

    }

    function scroll($container, direction) {
        if ($container.data('isanimating')) {
            return;
        }
        var config = $container.data('config');
        var halfShowNum = (config.num - 1) / 2;
        var scales, i, newIndex;
        var totalNum = $container.data('totalNum');
        var targetCss;
        var firstIndexBeforeScroll, lastIndexBeforeScroll;
        if (direction === 'left') {
            newIndex = ($container.data('index') - 1 + totalNum) % totalNum;
        } else if (direction === 'right') {
            newIndex = ($container.data('index') + 1) % $container.data('totalNum');
        } else {
            return;
        }
        // $container.find('ul li').stop(true, true);
        var tempIndexsInfo = getShowIndexs($container);
        firstIndexBeforeScroll = tempIndexsInfo.indexs[0];
        lastIndexBeforeScroll = tempIndexsInfo.indexs[config.num - 1];
        $container.data('index', newIndex);
        var showIndexsInfo = getShowIndexs($container);
        var zIndexs = getzIndexValue(config.num, direction);
        if (totalNum === config.num) {
            animationTimeForEdge = 0
        } else if (totalNum - config.num === 2) {
            animationTimeForEdge = config.animationTime / 2;
        } else {
            animationTimeForEdge = config.animationTime;
        }

        /*
         showIndexsInfo = {
         indexs: [5, 6, 0, 1, 2]
         hashIndexs: {
         '5': 0,
         '6': 1,
         '0': 2,
         '1': 3,
         '2': 4
         }
         }
         */
        $container.find('ul li').each(function (index, element) {

            i = showIndexsInfo.hashIndexs[index];

            if (i !== undefined) {
                scales = Math.pow(config.scale, Math.abs(i - halfShowNum));
                $container.data('isanimating', true);
                $(element).css({
                    display: 'block',
                    'z-index': zIndexs[i] + 9999
                }).animate({
                    width: scales * config.maxWidth,
                    height: scales * config.maxHeight,
                    left: i * config.distance + (1 - scales) * config.maxWidth * Number(i > halfShowNum),
                    top: (1 - scales) * config.maxHeight / 2
                }, config.animationTime, function () {
                    $container.data('isanimating', false);
                });

            } else {
                scales = Math.pow(config.scale, halfShowNum);
                //if(direction === 'right' && index === firstIndexBeforeScroll){
                //    console.log('right' + index);
                //} else if(direction === 'left' && index === lastIndexBeforeScroll) {
                //    console.log('left' + index);
                //}

                targetCss = {
                    display: 'none',
                    left: halfShowNum * config.distance + (1 - scales) * config.maxWidth / 2,
                    top: 0
                };
                if (direction === 'left' && index === lastIndexBeforeScroll) {

                    $(element).css('z-index', -1).animate({
                        left: "-=" + config.distance + "px"
                    }, config.animationTime, function () {
                        $(element).css(targetCss);
                    });
                } else if (direction === 'right' && index === firstIndexBeforeScroll) {

                    $(element).css('z-index', -1).animate({
                        left: "+=" + config.distance + "px"
                    }, config.animationTime, function () {
                        $(element).css(targetCss);
                    });
                } else {
                    $(element).css({
                        display: 'none',
                        width: scales * config.maxWidth,
                        height: scales * config.maxHeight,
                        left: halfShowNum * config.distance + (1 - scales) * config.maxWidth / 2,
                        top: 0
                    });
                }
            }

        });
    }

    function getConfig(newConfig) {
        var config = null;
        if (typeof newConfig === 'object' && newConfig !== null) {
            config = {};
            for (var prop in defaultConfig) {
                if (defaultConfig.hasOwnProperty(prop)) {
                    config[prop] = defaultConfig[prop];
                }
            }
            for (prop in newConfig) {
                if (newConfig.hasOwnProperty(prop) && config.hasOwnProperty(prop)) {
                    config[prop] = newConfig[prop];
                }
            }
        }
        return config;
    }

    function getShowIndexs($container) {
        var showIndexs = [];
        var temp;
        var halfShowNum = ($container.data('config').num - 1) / 2;
        var currentIndex = $container.data('index') || 0;
        var totalNum = $container.data('totalNum') || 0;
        for (var i = -halfShowNum; i <= halfShowNum; i++) {
            temp = currentIndex + i;
            showIndexs.push((temp < 0 ? (temp + totalNum) : temp) % totalNum);
        }
        var hashIndexs = {};
        for (i = 0; i < showIndexs.length; i++) {
            hashIndexs[showIndexs[i]] = i;
        }
        return {
            indexs: showIndexs,
            hashIndexs: hashIndexs
        };
    }

    function initStyle($container) {
        var showIndexsInfo = getShowIndexs($container);

        var zIndex = 9999;
        var scales;
        var config = $container.data('config');
        var halfShowNum = (config.num - 1) / 2;
        var listWidth = halfShowNum * config.distance * 2 + config.maxWidth;
        var containerWidth = $container.width();
        var containerHeight = $container.height();
        if (containerWidth < listWidth) {
            $container.width(listWidth);
        }
        if (containerHeight < config.maxHeight) {
            $container.height(config.maxHeight);
        }
        $container.find('ul li img').css({
            width: "100%",
            height: "100%"
        });
        $container.find('ul').css({
            position: 'relative',
            width: listWidth,
            height: config.maxHeight,
            'list-style': 'none',
            padding: 0,
            margin: 0,
            marginLeft: '50%',
            left: -listWidth / 2,
            top: 70,
        });

        $container.find('.left').css({
            position: 'absolute',
            left: 10,
            top: '50%',
            'z-index': 9999 + $container.data('totalNum') + 1
        });

        $container.find('.right').css({
            position: 'absolute',
            right: 10,
            top: '50%',
            'z-index': 9999 + $container.data('totalNum') + 1
        });

        $container.find('ul li').each(function (index, element) {
            var i = showIndexsInfo.hashIndexs[index];
            if (i !== undefined) {
                scales = Math.pow(config.scale, Math.abs(i - halfShowNum));
                zIndex = 9999 + (i > halfShowNum ? (config.num - 1 - i) : i);
                $(element).css({
                    display: 'block',
                    position: 'absolute',
                    'z-index': zIndex,
                    overflow: 'hidden',
                    width: scales * config.maxWidth,
                    height: scales * config.maxHeight,
                    left: i * config.distance + (1 - scales) * config.maxWidth * Number(i > halfShowNum),
                    top: (1 - scales) * config.maxHeight / 2
                });
            } else {
                scales = Math.pow(config.scale, halfShowNum);
                $(element).css({
                    display: 'none',
                    position: 'absolute',
                    overflow: 'hidden',
                    width: scales * config.maxWidth,
                    height: scales * config.maxHeight,
                    left: halfShowNum * config.distance + (1 - scales) * config.maxWidth / 2,
                    top: 0
                });
            }

        });
    }

   // Modificado
    $(document).ready(function() {
        $('.container').carousel({
            num: 5,
            maxWidth: 620,
            maxHeight: 300,
            distance: 50,
            scale: 0.6,
            animationTime: 1000,
            showTime: 4000
        });
    });


    $.fn.carousel = function (param) {
        var config;
        var totalNum;
        var $target;
        $(this).each(function(index, target) {
            $target = $(target);
            if (typeof param === 'object' && param !== null) {
                config = getConfig(param);
                totalNum = $target.find('ul li').length;
                if (totalNum <= 0 || totalNum % 2 === 0) {
                    return;
                }
                if (config.num <= 0 || config.num > totalNum) {
                    config.num = totalNum;
                }
                $target.data('config', config);
                $target.data('index', 0);
                $target.data('totalNum', totalNum);
                initStyle($target);

                $target.find('.left').off('click').on('click', (function($target) {
                    return function() {
                        scroll($target, 'left');
                    }
                })($target));
                $target.find('.right').off('click').on('click',(function($target) {
                    return function() {
                        scroll($target, 'right');
                    }
                })($target));

                (function($target) {
                    var autoPlay;
                    clearInterval($target.data('auto'));
                    if($target.data('config').autoPlay) {
                        autoPlay = setInterval(function() {
                            scroll($target, 'right');
                        }, $target.data('config').showTime);
                        $target.data('auto', autoPlay);
                        $target.find('ul').off('mouseenter').on('mouseenter', function() {
                            clearInterval($target.data('auto'));
                        }).off('mouseleave').on('mouseleave', function() {
                            autoPlay = setInterval(function() {
                                scroll($target, 'right');
                            }, $target.data('config').showTime);
                            $target.data('auto', autoPlay);
                        });
                    } else {
                        $target.find('ul').off('mouseenter').off('mouseleave');
                    }
                })($target);
            }

        });

    };

})(jQuery);

// Novo
// Mostrar conteudo CaixaInfoNegocios
function conteudoCaixaInfoNegocios() {
	$(this).css('background', '#1C5485');
	$('.caixaInfoEmpresarial').css('background', '#1B1E21');
	$('.caixaInfoGestao').css('background', '#1B1E21');
	$(".sistemaERP .conteudo").addClass('invisivel');
	$(".sistemaERP .conteudoConsultoriaNegocios").removeClass('invisivel');
	
	$('#container').carousel({
		num: 5,
		maxWidth: 620,
		maxHeight: 300,
		distance: 50,
		scale: 0.6,
		animationTime: 1000,
		showTime: 4000
	});
}
// Novo
// Mostrar conteudo CaixaInfoNegocios
function conteudoCaixaInfoEmpresarial() {
	$(this).css('background', '#1C5485');
	$('.caixaInfoNegocios').css('background', '#1B1E21');
	$('.caixaInfoGestao').css('background', '#1B1E21');
	$(".sistemaERP .conteudo").addClass('invisivel');
	$(".sistemaERP .conteudoERPGestaoEmpresarial").removeClass('invisivel');
	
	$('#container2').carousel({
		num: 5,
		maxWidth: 620,
		maxHeight: 300,
		distance: 50,
		scale: 0.6,
		animationTime: 1000,
		showTime: 4000
	});
}
// Novo
// Mostrar conteudo CaixaInfoNegocios
function conteudoCaixaInfoGestao() {
	$(this).css('background', '#1C5485');
	$('.caixaInfoNegocios').css('background', '#1B1E21');
	$('.caixaInfoEmpresarial').css('background', '#1B1E21');
	$(".sistemaERP .conteudo").addClass('invisivel');
	$(".sistemaERP .conteudoGestaoPessoas").removeClass('invisivel');
	
	$('#container3').carousel({
		num: 5,
		maxWidth: 620,
		maxHeight: 300,
		distance: 50,
		scale: 0.6,
		animationTime: 1000,
		showTime: 4000
	});
}
// Novo
function abrirModalCarrossel() {
	$('.modalCarrossel').css('display', 'block');
	$('.imgModal').attr('src', $(this).attr('src'));
	$('.imgModal').attr('alt', $(this).attr('alt'));
}

function redimensionaJanela() {
	$(".figureColaborador1024::after").addClass("invisivel");
	if(window.innerWidth < 1024) {
		
		$(".exibirEquipe").removeClass("invisivel");
		$(".figureColaborador1024").each((index, figureColaborador)=>{

			if(index > 1){
					console.log('conteinerPerfilProfissional ', index);
				$(figureColaborador).addClass("invisivel");
			}								
		});	
		
	}else {
			console.log('else ', window.innerWidth)
		// ScrollReveal().reveal('.figureColaborador1024', {
		// 	delay: 500,
		// 	useDelay: 'onload',
		// 	reset: true,
		// });
		$(".figureColaborador1024").removeClass("invisivel");
				console.log($(".exibirEquipe"))
				$(".exibirEquipe").addClass("invisivel");
		
	}
}

		// console.log('T')
		
	// var conteinerPerfilProfissional = ;
	// function figureColaborador {
	// 		$(figureColaborador1024).addClass("invisivel");
	// 		$(exibirEquipe).removeClass("invisivel");
	// 		$(imgColaborador).addClass("imgColaborador1024");
	// }

				// console.log('conteinerPerfilProfissional ', figureColaborador);
						// console.log($(".figureColaborador"))
									// console.log('Ta')