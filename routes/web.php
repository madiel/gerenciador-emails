<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index')->name('index');
Route::get('/sendEmail', 'ControllerDados@email');

Route::post('dados/email', 'ControllerDados@email');
Route::post('dados/contatos', 'ControllerDados@contatos');
Route::post('dados/trabalhe-conosco', 'ControllerDados@trabalhe');
Route::get('/pesquisar-trabalhe', 'Controller@pesquisarTrabalhe')->name('pesquisarTrabalhe');
Route::get('/pesquisar-contato', 'Controller@pesquisarContato')->name('pesquisarContato');

Route::get('/trabalhe-conosco', 'Controller@trabalheConosco')->name('trabalhe-conosco');
Route::get('/listarCurriculos', 'ControllerDados@listarCurriculos')->name('listarCurriculos');
Route::get('/excel', 'ExcelController@emails')->name('excel');;
Route::get('/institucional', 'Controller@institucional')->name('institucional');
Route::get('/contato', 'Controller@contato')->name('contato');
Route::get('/erro', 'Controller@erro')->name('erro');
Route::get('/clientes', 'Controller@clientes')->name('clientes');
Route::get('/linhadotempo', 'Controller@linhadotempo')->name('linhadotempo');
Route::get('/parceiros', 'Controller@parceiros')->name('parceiros');
Route::get('/associacoes', 'Controller@associacoes')->name('solucao-associacoes');
Route::get('/solucao-automotivo', 'Controller@automotivo')->name('solucao-automotivo');
Route::get('/solucao-contabil-fiscal', 'Controller@contabilFiscal')->name('solucao-contabil-fiscal');
Route::get('/solucao-contratos-de-servicos', 'Controller@controleServisos')->name('solucao-contratos-de-servicos');
Route::get('/solucao-controle-de-cobranca', 'Controller@controleCobranca')->name('solucao-controle-de-cobranca');
Route::get('/solucao-controle-de-obras', 'Controller@controleObras')->name('solucao-controle-de-obras');
Route::get('/solucao-distribuicao', 'Controller@controleDistribuicao')->name('solucao-distribuicao');
Route::get('/solucao-fornecimento-de-pecas', 'Controller@controlePecas')->name('solucao-fornecimento-de-pecas');
Route::get('/solucao-gestao-de-estacionamento', 'Controller@controleEstacionamento')->name('solucao-gestao-de-estacionamento');
Route::get('/solucao-gestao-rh', 'Controller@solucaoRh')->name('solucao-gestao-rh');
Route::get('/solucao-logistica', 'Controller@solucaoLogistica')->name('solucao-logistica');
Route::get('/solucao-manufatura', 'Controller@solucaoManufatura')->name('solucao-manufatura');
Route::get('/solucao-motores-e-maquinas', 'Controller@solucaoMaquinas')->name('solucao-motores-e-maquinas');
Route::get('/solucao-nota-fiscal-eletronica', 'Controller@solucaoEletronica')->name('solucao-nota-fiscal-eletronica');
Route::get('/transformacao-digital', 'Controller@transformacaoDigital')->name('transformacao-digital');
Route::get('/termos-e-condicoes', 'Controller@transformacaoDigital')->name('termos-e-condicoes');
Route::get('/contato-interno', 'ControllerDados@contatoInterno')->name('contato-interno');
Route::get('/trabalhe-interno', 'ControllerDados@trabalheInterno')->name('trabalhe-interno');
Route::get('/trabalhe-total/{id}', 'ControllerDados@trabalheTotal')->name('trabalhe-total');
Route::get('/contato-dados/{id}', 'ControllerDados@contatoTotal')->name('contato-dados');
Route::get('/desevolvimento-de-software', 'Controller@devSotware')->name('desevolvimento-de-software');
Route::get('/consultoria-empresarial', 'Controller@consultEmpresarial')->name('consultoria-empresarial');


Auth::routes();

Route::get('/painel', 'HomeController@index')->name('painel');
