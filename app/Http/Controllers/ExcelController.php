<?php

namespace App\Http\Controllers;
use App\Email;
use App\ScholarShip;
use App\EmailStudent;
use App\emails;
use Excel;
use Carbon\Carbon;

class ExcelController extends Controller
{
    public function emails($id = 0)
    {
        $emailEnllevo = Email::where('id', '>=', $id)->orderBy('id', 'DESC')->get();

        return \Excel::create('newsletter', function($excel) use ($emailEnllevo) {
            $excel->sheet('Inscrições', function($sheet) use ($emailEnllevo) {

                foreach ($emailEnllevo as $item) {
                    $rows[] = [
                        '#' => $item->id,
                        'Data' => \PHPExcel_Shared_Date::PHPToExcel(Carbon::parse($item->created_at)),
                        'Email' => $item->email,
                    ];
                }
                $sheet->setColumnFormat([
                    'B' => 'dd/mm/yyyy h:mm'
                ]);

                $sheet->fromArray($rows, null, 'A1', false);

                $sheet->cells('A1:M1', function($cells) {
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(14);
                    $cells->setFontColor('#FFFFFF');
                    $cells->setBackground('#1A2134');
                });
            });
        })->download('xlsx');
    }
}