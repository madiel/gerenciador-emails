<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\TrabalheConosco;
use App\Contato;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        return view('index');
    }

    public function trabalheConosco(){
        return view('trabalhe-conosco');
    }

    public function institucional(){
        return view('institucional');
    }

    public function contato(){
        return view('contato');
    }

    public function erro(){
        return view('erro');
    }

    public function clientes(){
        return view('clientes');
    }

    public function linhadotempo(){
        return view('linha-do-tempo');
    }

    public function parceiros(){
        return view('parceiros');
    }

    public function associacoes(){
        return view('solucao-associacoes');
    }

    public function automotivo(){
        return view('solucao-automotivo');
    }

    public function contabilFiscal(){
        return view('solucao-contabil-fiscal');
    }

    public function controleServisos(){
        return view('solucao-contratos-de-servicos');
    }

    public function controleCobranca(){
        return view('solucao-controle-de-cobranca');
     }

    public function controleObras(){
        return view('solucao-controle-de-obras');
     }

    public function controleDistribuicao(){
        return view('solucao-distribuicao');
     }

    public function controlePecas(){
        return view('solucao-cotacao-eletronica');
     }

    public function controleEstacionamento(){
        return view('solucao-gestao-de-estacionamento');
     }

    public function solucaoRh(){
        return view('solucao-gestao-rh');
     }

    public function solucaoLogistica(){
        return view('solucao-logistica');
     }

    public function solucaoManufatura(){
        return view('solucao-manufatura');
     }

    public function solucaoMaquinas(){
        return view('solucao-motores-e-maquinas');
     }

    public function solucaoEletronica(){
        return view('solucao-nota-fiscal-eletronica');
     }

    public function transformacaoDigital(){
        return view('transformacao-digital');
     }

    public function devSotware(){
        return view('servicos-desenvolvimento-de-software');
     }

    public function consultEmpresarial(){
        return view('servicos-consultoria-empresarial');
     }



     public function pesquisarTrabalhe(Request $request)
     {

        $q = $request->input('pesquisar');

        $data['trabalhe'] = trabalheConosco::where(function($query) use ($q) {
            $query->where('nome', 'LIKE', '%'.$q.'%')
                  ->orWhere('sobrenome', 'LIKE', '%'.$q.'%')
                  ->orWhere('created_at', 'LIKE', '%'.$q.'%');
        })->paginate(5);


        return view('mail.trabalhe-interno', $data);

     }

     public function pesquisarContato(Request $request)
     {

        $q = $request->input('pesquisar');

        $data['contato'] = contato::where(function($query) use ($q) {
            $query->where('nome', 'LIKE', '%'.$q.'%')
                  ->orWhere('sobrenome', 'LIKE', '%'.$q.'%')
                  ->orWhere('created_at', 'LIKE', '%'.$q.'%');
        })->paginate(5);

        return view('mail.contato-interno', $data);

     }

 



}


