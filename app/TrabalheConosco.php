<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrabalheConosco extends Model
{

    protected $fillable = [
        'g-recaptcha-response',
        'nome',
        'sobrenome',
        'email',
        'linkedin',
        'area',
        'cargo',
        'mensagem',
        'curriculo',
        'celular',
    ];

    public $rules = [ 
        'g-recaptcha-response' => 'required|recaptcha',
    ];
    
}
